all: Thesis.pdf titlePage.pdf titlePic.pdf

titlePage.pdf:
	latexmk -pdflatex="pdflatex --shell-escape -halt-on-error %O %S" -pdf titlePage.tex

titlePic.pdf:
	latexmk -pdflatex="pdflatex --shell-escape -halt-on-error %O %S" -pdf titlePic.tex

Thesis.pdf:
	latexmk -pdflatex="pdflatex --shell-escape -halt-on-error %O %S" -pdf Thesis.tex

clean:
	rm -f figures/* *~ parts/*~ Thesis.pdf *.aux *.log *.blg *.bbl *.toc *.old *.out

.PHONY: Thesis.pdf titlePage.pdf titlePic.pdf
