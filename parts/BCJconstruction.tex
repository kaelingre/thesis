\chapter{Construction of Color-Kinematics Dual Amplitudes}\label{ch:BCJconstruction}

The construction of a color-kinematics dual representation, see chapter~\ref{ch:bcj}, is often done via an Ansatz construction.
A first step is to identify a minimal set of numerators independent under Jacobi identities/commutation relations.
We call this set \emph{master} numerators or simply \emph{masters}.
After having identified the basic Lorentz-invariant kinematic objects, we can write down an Ansatz for each master.
The requirements from the color-kinematics duality and physical requirements constrain the parameters in the Ansatz.
For us, the latter information is coming from unitarity cuts.
In general, any object fulfilling these requirements is a valid color-kinematics dual representation and can be used for a double copy construction.
Since the resulting expressions may still have additional freedom, one can choose to implement further constraints.
For example, we can manifest certain useful properties to improve the presentation for further processing (e.g. integration or double copy).

The main content of the first two parts of this section is a presentation of various useful constraints that have been identified in~\cite{Johansson:2014zca} and publications~\ref{lbl:paper1} and \ref{lbl:paper3} attached to this thesis.
The third part concerns the idea of rendering the Ansatz approach obsolete by trying to lift expressions for unitarity cuts off-shell and directly construct numerators for trivalent graphs.
This discussion is based on the ideas of paper~\ref{lbl:paper3}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Master Numerators and the Ansatz}\label{sec:masters}
The system of equations built out of the Jacobi identities and commutation relations for color-kinematics dual numerators
\begin{equation}
  n_i + n_j + n_k = 0
\end{equation}
is in general hard to solve (i.e. finding a set of master numerators).
It grows significantly for each additional external leg or additional loop, see for example~\cite{Carrasco:2015iwa}.
However, one possible algorithm is the following:
\begin{enumerate}
\item pick a numerator that has not yet been solved for and add it to the set of masters;
\item identify the set of numerators that can be expressed in terms of the masters and those that cannot;
\item if all numerators are expressible in terms of the masters, the algorithm terminates, otherwise continue from step 1.
\end{enumerate}
Depending on the choice in step~1, the set of master numerators may notably change.
Possible purely heuristic criteria are given in the following list.
One can choose master numerators such that
\begin{itemize}
\item its associated graph has a large amount of symmetries (to simplify the symmetry constraints of the system),
\item its associated graph corresponds to a maximal cut; i.e. it is the only numerator contributing to the cut,
\item the associated graph is planar,
\item the number of master numerators is minimal,
\item the average number of masters appearing in expressions for derived numerators is minimized.
\end{itemize}
Depending on the situation, some of the criteria can be more useful than others.
It can, for example, be useful to increase the number of master numerators if in turn other constraint equations simplify.

The idea of master numerators can be extended to not only include constraints from the color-kinematics duality but also any other kind of functional relations among numerators.
This in general reduces the number of masters and as such the size of the Ansatz (number of free parameters).
It comes with the risk that several constraints are mutually incompatible and no representation can be found.
This problem might either be overcome by dropping the constraints or by increasing the size of the Ansatz.
We will discuss some functional constraints in the following section.
This system of equations is not necessarily trivially solved in terms of the master numerators as there might be different ``paths'' how a derived numerator is reached from the masters.
The equations that are not trivially solved need to be implemented as consistency constraints on the Ansatz.

Once the master numerators are identified, we make a uniform Ansatz for each of them.
From the discussion of unitarity cuts in sec.~\ref{ch:supersums} and explicit computations in~\cite{Johansson:2014zca} and papers~\ref{lbl:paper1} and \ref{lbl:paper3}, we see that the state configuration object~$\hat{\kappa}$ captures the correct helicity weights and non-localities.
Thus, we expect the numerators to be of the form $(\text{poly})\times\hat{\kappa}$, where $(\text{poly})$ is a \emph{Lorentz-invariant} polynomial in the kinematics.
Most importantly, it is local.

For a dimensionally regulated theory living in $D=4-2\epsilon$ dimensions, we build the polynomial from a collection of objects:
\begin{itemize}
\item Lorentz products between internal and external momenta such as
  \begin{equation}
    p_i\cdot p_j,\, p_i\cdot\ell_j,\, \ell_i\cdot\ell_j\,
  \end{equation}
  where $p_i$ denotes four-dimensional external momenta and $\ell_i$ are $D$-dimensional internal loop momenta.
  Since the $p_i$ can be embedded in a four-dimensional subspace of the full $D$-dimensional space, the product between an external and a loop momentum is equvalently written as $p_i\cdot\ell_j = p_i\cdot\bar{\ell}_j$, where $\bar{\ell}_i$ denotes the four-dimensional part of the loop momentum.
\item The contraction of the Levi-Civita tensor with external momenta and the four-dimensional part of loop momenta
  \begin{equation}
    \epsilon(k_1,k_2,k_3,k_4)\equiv\det(k_{i\mu}),\quad \text{where } \mu = 1,\dots,4\,.
  \end{equation}
\item We can decompose $\ell_i=\bar{\ell}_i+\mu_i$. Objects denoted by $\mu_{ij}=-\mu_i\cdot\mu_j$ need to be included in the Ansatz as well. The minus sign is coming from our use of the mostly-minus metric.
\item We use the six-dimensional spinor-helicity formalism to obtain information about the amplitude in $D$ dimensions.
  To encode this information, we need additional extra-dimensional antisymmetric objects, e.g. for two loops there is a single object:
  \begin{equation}
    \epsilon(\mu_1,\mu_2)\equiv\frac{\epsilon^{(6)}(p_1,p_2,p_3,p_4,\ell_1,\ell_2)}{\epsilon(p_1,p_2,p_3,p_4)}=\det(\mu_1,\mu_2)\,.
  \end{equation}
\end{itemize}
The Lorentz products, $\mu_{ij}$, and $\epsilon(\mu_1,\mu_2)$ have dimension $(\text{mass})^2$, the four-dimensional antisymmetric objects have twice this dimension $(\text{mass})^4$.
We define $M^{(N)}$ as the set of (linearly independent) monomials built out of the above objects with dimension $(\text{mass})^{2N}$.
Note that independence of these objects is not a strict requirement; redundancy may even help to simplify the algebraic representation of the final numerators.

For a numerator of a four-point $L$-loop amplitude for $\cN$ supersymmetries, a possible Ansatz reads
\begin{equation}\label{eq:ansatz}
  n\left(1,2,3,4,\{\ell_i\}_{i=1}^L\right) = \sum_{(\ubar{i}\ubar{j})} \hat{\kappa}_{(\ubar{i}\ubar{j})}\sum_k a_{(\ubar{i}\ubar{j});k}M_k^{(3-\cN-L)}\,,
\end{equation}
where $a_{(\ubar{i}\ubar{j});k}$ are constant rational coefficients to be determined.
Depending on possible external state configurations of the corresponding graph, one includes a certain set of $(\ubar{i}\ubar{j})$ in this sum.

This Ansatz has been successfully used for the calculation of a color-kinematics dual form of all one-loop amplitudes with four external gluons in $\cN=0,1,2$ (S)QCD~\cite{Johansson:2014zca} and of all one- and two-loop amplitudes in $\cN=2$ SDCQ with four arbitrary external states in publications~\ref{lbl:paper1} and \ref{lbl:paper3}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{A Plethora of Constraints}\label{sec:constraints}
The necessary constraints for a color-kinematics dual representation come from Jacobi identities and commutation relations for the numerator factors and the physical unitarity cuts.
Generically, the Ansatz~\eqref{eq:ansatz} still has a large number of free parameters after fixing these constraints.
In principle it is possible to solve the constraints for an arbitrary subset of parameters~$\{a_{(\ubar{i}\ubar{j});k}\}$ and set the remaining free parameters to zero (or to any other arbitrary value).
Expressions obtained this way are in general algebraically large and badly suited for integration or a double copy.
Furthermore, for a better understanding of the mathematical structure of integrands, it can be advantageous to manifest symmetries or other properties.

We present here a collection of such constraints that have been useful for the computations in the referenced papers.

\paragraph{Crossing symmetry}
A powerful set of constraints comes from the symmetry of diagrams.
The idea is to enforce a relabeling symmetry of a diagram on the corresponding numerator expression.
An instructive example is:
\begin{equation}
  %\tikzset{external/force remake}
  \begin{aligned}
   \gBoxBox[all=gluon,iA=quark,iB=quark,iC=quark,iD=quark,iE=quark,iF=quark,eLA=$1^-$,eLB=$2^-$,eLC=$3^+$,eLD=$4^+$,iLA=$\ell_1\!\uparrow$,iLD=$\uparrow\!\ell_2$]{}
   &\leftrightarrow \gBoxBox[all=gluon,iA=quark,iB=quark,iC=quark,iD=quark,iE=quark,iF=quark,eLA=$3^+$,eLB=$4^+$,eLC=$1^-$,eLD=$2^-$,iLA=$\ell_2\!\downarrow$,iLD=$\downarrow\!\ell_1$]{}\\
   \Leftrightarrow n(1^-,2^-,3^+,4^+;\ell_1,\ell_2) &=n\left(3^+,4^+,1^-,2^-;-\ell_2,-\ell_1\right)\,.
  \end{aligned}
\end{equation}
This identity relates different state configurations and can directly be used to decrease the number of master numerators.
In general, these constraints have to be implemented on the Ansatz by constraining the coefficients.
Computationally, this can be implemented rather efficiently for symmetries of master numerators but unfortunately, it becomes significantly harder the further away a numerator is from the masters.

\paragraph{CPT conjugation}
CPT invariance of the theory can be manifested in the numerator expressions.
In terms of the basic building blocks, CPT conjugation acts by exchanging $\ket{i}\leftrightarrow\sqket{i}$ and $\eta_i^A\leftrightarrow\bar{\eta}_{i,A}$.
For each diagram, this is achieved by swapping the helicity of external vector states and reversing the arrow for hypermultiplets.
In terms of the kinematical objects in the Ansatz, this transformation corresponds to replacing the state configuration $(\ubar{i}\ubar{j})$ with its complement $(\overline{\ubar{i}\ubar{j}})$ and flipping the sign of parity-odd terms.
Schematically,
\begin{equation}
  n(1,2,3,4,\{\ell_i\}) = \bar{n}(1,2,3,4,\{\ell_i\})|_{\hat{\kappa}_{(\ubar{i}\ubar{j})}\rightarrow\hat{\kappa}_{(\overline{\ubar{i}\ubar{j}})},\ket{i}\leftrightarrow\sqket{i}}\,,
\end{equation}
where $\bar{n}$ denotes the numerator corresponding to the graph with reversed matter lines.
From experience, the CPT constraints are not very strong and often automatically fulfilled through unitarity cuts.

\paragraph{Power counting}
Heuristically, numerators with $m_i$ propagators carrying the loop momentum~$\ell_i$ in the corresponding diagram permit a representation with maximally $m_i-\cN$ powers of $\ell_i$.
This constraint is straightforward to implement, since terms with too high power-counting are simply excluded from the Ansatz.
Power counting constraints can be extremely powerful but have a tendency to clash with other constraints discussed below.

\paragraph{Tadpoles and external bubbles}
In principle, a color-kinematics dual representation is allowed to include diagrams with massless tadpole subdiagrams or bubbles on external legs, diagrammatically
\begin{equation}
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize]
    \draw[line] (0,0) arc (0:360:0.2);
    \draw[line] (0,0) -- (0.3,0);
    \draw[dotted] (0.35,0) -- (0.6,0);
  \end{tikzpicture}\,,
  \quad
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize]
    \draw[line] (0,0) node[left] {$i$} -- (0.3,0);
    \draw[line] (0.3,0) arc (180:540:0.2);
    \draw[line] (0.7,0) -- (1,0);
    \draw[dotted] (1.05,0) -- (1.3,0);
  \end{tikzpicture}\,.
\end{equation}
Since these diagrams never appear in physical unitarity cuts the corresponding numerators can be chosen to vanish as long as it is consistent with the color-kinematics duality (or other constraints).
It is of course also possible to remove any other type of diagram as long as it is consistent with all the other constraints.
Since these numerators tend to be far away from the masters, i.e. they are expressed in terms of a large number of master numerators, these constraints tend to be computationally expensive to implement on an Ansatz.
Implemented as functional constraint, they can reduce the number of master numerators and simplify the system in general.
An example for this simplification has been seen in paper~\ref{lbl:paper1}, where a two-loop pentagon-triangle diagrams has been chosen to vanish as its maximal cut was zero.

\paragraph{Two-term identities}
In the spirit of the color-kinematics three-term identities~\eqref{eq:jacsComms}, one might want to consider an extension of the numerator constraints from the Jacobi identities and commutation relations of the dual color factors~\eqref{eq:jacsComms}.
Even though
\begin{equation}
  c\left(\gTreeS[eA=quark,eB=aquark,eC=quark,eD=aquark,iA=gluon,eLA=$i$,eLB=$j$,eLC=$k$,eLD=$l$]{}\right)\overset{?}{=} c\left(\gTreeT[eA=quark,eB=aquark,eC=quark,eD=aquark,iA=gluon,eLA=$i$,eLB=$j$,eLC=$k$,eLD=$l$]{}\right)
\end{equation}
is not true for a generic gauge group, the corresponding identity for numerators,
\begin{equation}
  n\left(\gTreeS[eA=quark,eB=aquark,eC=quark,eD=aquark,iA=gluon,eLA=$i$,eLB=$j$,eLC=$k$,eLD=$l$]{}\right) = n\left(\gTreeT[eA=quark,eB=aquark,eC=quark,eD=aquark,iA=gluon,eLA=$i$,eLB=$j$,eLC=$k$,eLD=$l$]{}\right)\,,
\end{equation}
turns out to be useful as it allows diagrams with several closed matter loops to be expressed in terms of diagrams with a single closed matter loop.
Thus, these identities are powerful when implemented in a functional way.
They significantly reduce the number of masters, but also give further constraints when imposed on an Ansatz.

For $\cN=2$ SQCD, the two-term identity can be justified by the cut rule~\eqref{eq:neq2IntRule3} from the supersum discussion.
The factor from this internal rule can be decomposed as $s_{ac} = -s_{ab} -s_{bc}$.
These terms can be used to cancel the $s$ and $t$-pole respectively of the corresponding tree level amplitude.
So, it seems possible to assign these two contributions to the two graphs that are exactly related by a two-term identity.
This idea is closely tied to an off-shell lift construction discussed in the following section.

\paragraph{Supersymmetric decomposition}
From the discussion in sec.~\ref{ch:susy} it is clear that the on-shell state content of $\cN=4$ SYM and $\cN=2$ SQCD with a single hypermultiplet are equivalent
\begin{equation}\label{eq:susyDecomp}
  \cV_{\cN=4} = \cV_{\cN=2} + \Phi_{\cN=2} + \overline{\Phi}_{\cN=2}\,.
  \end{equation}
From an amplitudes point of view, these two theories only differ by the gauge group representation of the hypermultiplet.
Since the numerators are color-independent, one can imagine that there exists a (color-kinematics dual) representation of the amplitude fulfilling relations of the sort
\begin{equation}
  %\tikzset{external/force remake}
  \gBoxBox[all=gluon]{
    \node at (0.9,1.15) {$\cN=4$};
  } =
  \gBoxBox[all=gluon]{
    \node at (0.9,1.15) {$\cN=2$};
  }
  + \gBoxBox[all=gluon,iA=line,iB=line,iF=line,iG=line]{
    \node at (0.9,1.15) {$\cN=2$};
  }
  + \gBoxBox[all=gluon,iC=line,iD=line,iE=line,iG=line]{
    \node at (0.9,1.15) {$\cN=2$};
  }
  + \gBoxBox[all=gluon,iA=line,iB=line,iC=line,iD=line,iE=line,iF=line]{
    \node at (0.9,1.15) {$\cN=2$};
  }\,
\end{equation}
where a bold line stands for both parts of the hypermultiplet added up:
\begin{equation}
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize]
    \draw[line] (0,0) -- (1,0) node[right] (A) {$=$};
    \draw[quark] (A) -- ++(1,0) node[right] (B) {$+$};
    \draw[aquark] (B) -- ++(1,0);
  \end{tikzpicture}\,.
\end{equation}
We also dropped the $n(\dots)$ notation to denote numerators --- from this point on a diagram represents the corresponding numerator factor.

As amplitudes with higher degree of supersymmetry are simpler to compute, we can recycle them as known expressions into our system.
Already the color-kinematics dual representation of the amplitude in the simpler theory might not be unique.
Its choice influences the representation of the amplitude in the decomposed theory.

Equations of this form are most conveniently solved for the diagram with purely vectorial content in the decomposed theory as this diagram only appears once in the equation (while other diagrams might appear with different labelings which are related by crossing symmetry).

\paragraph{Minimal denominator solution}
A brute force way of simplifying numerator expressions is by implementing an extremization criterion on the numerical expressions of the coefficients.
One criterion that can be efficiently implemented is via a so-called~\emph{minimal denominator solution}~\cite{Chen:2005bbc}.
The idea is simply to find a solution to the collected set of constraint equations --- a linear system --- that has a minimal denominator for the coefficients~$\{a_{(\ubar{i}\ubar{j});k}\}$ in the Ansatz~\eqref{eq:ansatz} and minimizes the norm of the coefficient vector
\begin{equation}
  \left(\sum_{(\ubar{i}\ubar{j}),k} (a_{(\ubar{i}\ubar{j});k})^2\right)^{\frac{1}{2}}\,.
\end{equation}
The explicit form of the norm can also be changed and might lead to different results.

An implementation is for example included in the \emph{integer matrix library}~\cite{Chen:2005bbc}.

\paragraph{Matter reversal symmetry}
Restricting to $\cN=2$, it has been observed in~\cite{Johansson:2014zca} that both halves of the on-shell hypermultiplet $\Phi$ and $\overline{\Phi}$ contain the same states, interacting in the same way with the vector multiplet.
A possible constraint arising from this is the following identity:
\begin{equation}
  %\tikzset{external/force remake}
  \gBoxBox[all=gluon,iA=quark,iB=quark,iF=quark,iG=quark]{}
  = \gBoxBox[all=gluon,iA=aquark,iB=aquark,iF=aquark,iG=aquark]{}
  = \frac{1}{2}\gBoxBox[all=gluon,iA=line,iB=line,iF=line,iG=line]{}\,,
\end{equation}
which in general is the statement that diagrams with matter can be related to the diagram with (individually) reversed matter arrows.
For matter on external legs, one also needs to take the state configuration into account, e.g.
\begin{equation}
  %\tikzset{external/force remake}
  \gBox[all=gluon,eA=quark,eB=quark,eC=aquark,eD=aquark,iB=aquark,iD=quark,eLA=1,eLB=2,eLC=3,eLD=4]{}
  = -\left.\gBox[all=gluon,eA=quark,eB=aquark,eC=quark,iB=quark,iD=quark,eD=aquark,eLA=1,eLB=2,eLC=3,eLD=4]{}\right|_{\eta_2^4\rightarrow\eta_2^3,\eta_3^3\rightarrow\eta_3^4}\,.
\end{equation}
$\cN=2$ SYM permits the hypers to transform in a pseudoreal representation of the gauge group.
Since in such a representation the arrow has no meaning, the above identity is justified.
This symmetry can be implemented functionally as well as imposed on the Ansatz.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Candidate Numerators from Cuts}\label{sec:guess}
The iterated two-particle cuts for $\cN=2$ SQCD discussed in chapter~\ref{ch:supersums} are manifestly local and handle the propagator poles in a transparent manner.
An idea from paper~\ref{lbl:paper3} is to massage the cut expressions into a form such that each term cancels either the $s$ or $t$ pole of all contributing tree level amplitudes and as such can be interpreted as the contribution of the graph with the corresponding remaining propagator poles.
These expressions obey the cut kinematics and might not necessary correspond to a valid representation of the amplitude with off-shell kinematics.
Nevertheless, this procedure has been successfully applied to various one- and two-loop amplitudes for $\cN=2$ SQCD.

As a simple example, consider the one-loop cut
\begin{equation}
  \begin{aligned}
    %\tikzset{external/force remake}
    \cBoxA[all=gluon,iA=aquark,iB=aquark,eLA=$1^-$,eLB=$2^+$,eLC=$3^-$,eLD=$4^+$,
      iLA=$\uset{\rightarrow}{5}$,iLB=$\oset{\rightarrow}{6}$,iLC=$l_1\uparrow$,iLD=$\downarrow l_2$]{}
    &\propto -\frac{\braSqket{1|l_1|2}}{s l_1^2} \times s_{56} \times \frac{\braSqket{3|l_2|4}}{s l_2^2} \times \sqbraket{13}\braket{24}\\
    &= \frac{s_{56}\trm(1l_124l_23)}{s^2l_1^2l_2^2}\,,
  \end{aligned}
\end{equation}
where we have dropped an overall factor of $\hat{\kappa}_{(13)(13)}$.
The goal of the following transformations is to use the Clifford algebra anti-commutation relation (see appendix~\ref{sec:traces} for conventions) to split this expression into several summands.
We demand that each of them has either the $s$ or $l_1^2$ pole from the left tree and either the $s$ or $l_1^2$ poles from the right tree removed.
We start by moving the $l_1$ and $l_2$ towards the middle
\begin{equation}
  \begin{aligned}
    \cBoxA[all=gluon,iA=aquark,iB=aquark,eLA=$1^-$,eLB=$2^+$,eLC=$3^-$,eLD=$4^+$,
      iLA=$\uset{\rightarrow}{5}$,iLB=$\oset{\rightarrow}{6}$,iLC=$l_1\uparrow$,iLD=$\downarrow l_2$]{}
    &\propto \frac{s_{56}\left(l_1^2 \trm(14l_23) -l_2^2\trm(12l_13) + \trm(12l_1l_243)\right)}{s^2l_1^2l_2^2}\,.
  \end{aligned}
\end{equation}
Using momentum conservation (and more anti-commutations), we rewrite the first two traces as
\begin{equation}
  \begin{aligned}
    \trm(14l_23) &= -\trm(1463) = -l_2^2 u + \trm(1653)\,,\\
    \trm(12l_13) &= \trm(1253) = - \trm(1653)\,,\\
  \end{aligned}
\end{equation}
and the six-trace as
\begin{equation}
  \trm(12l_1l_243) = -\trp(2l_1l_2421) = -s \trp(2l_1l_24) = -s \trm(1l_1l_23)\,.
\end{equation}
Canceling propagator poles leads then to
\begin{equation}
  \cBoxA[all=gluon,iA=aquark,iB=aquark,eLA=$1^-$,eLB=$2^+$,eLC=$3^-$,eLD=$4^+$,
    iLA=$\uset{\rightarrow}{5}$,iLB=$\oset{\rightarrow}{6}$,iLC=$l_1\uparrow$,iLD=$\downarrow l_2$]{}
  \propto -\frac{u s_{56}}{s^2} + \frac{\trm(1653)}{s l_2^2} + \frac{\trm(1653)}{s l_1^2} - \frac{\trm(1l_1l_23)}{l_1^2l_2^2}\,.
\end{equation}
Now, these four summands carry exactly the poles of the four contributing graphs, whereof two are related by a crossing symmetry.
Furthermore, the color-kinematics duality for the numerator factors are fulfilled if these expressions are interpreted off-shell in the following way
\begin{equation}\label{eq:exCutLift}
  \begin{aligned}
    %\tikzset{external/force remake}
    \gBox[all=gluon,iA=quark,iB=quark,iC=quark,iD=quark,eLA=$1^-$,eLB=$2^+$,eLC=$3^-$,eLD=$4^+$,iLB=$\uset{\rightarrow}{\ell_2}$,iLD=$\oset{\leftarrow}{\ell_1}$]{}
    &= \trm(1\ell_1\ell_23)\hat{\kappa}_{(13)(13)}\,,\\
    %\tikzset{external/force remake}
    \gTriB[all=gluon,iA=quark,iB=quark,iC=quark,eLA=$1^-$,eLB=$2^+$,eLC=$3^-$,eLD=$4^+$,iLB=$\!\!\uset{\searrow}{\ell_2}$,iLC=$\!\!\oset{\swarrow}{\ell_1}$]{}
    &= \trm(1\ell_1\ell_23)\hat{\kappa}_{(13)(13)}\,,\\
    \gBubB[all=gluon,iB=quark,iC=quark,eLA=$1^-$,eLB=$2^+$,eLC=$3^-$,eLD=$4^+$,iLB=$\uset{\rightarrow}{\ell_2}$,iLC=$\oset{\leftarrow}{\ell_1}$]{}
    &= -2u\,\ell_1\cdot\ell_2\hat{\kappa}_{(13)(13)}\,.
  \end{aligned}
\end{equation}
By a similar treatment for the other external configurations and with the help of color-kinematics duality, all other numerators are obtained.
Note that these are results in four dimensions.
For dimensionally reduced expressions, one can supplement the construction with cuts in six dimensions to obtain the missing information for the $4-2\epsilon$ dimensional part.
For the complete solution see paper~\ref{lbl:paper3}.

The box numerator in eq.~\eqref{eq:exCutLift}, together with further external configurations thereof, can be chosen as the only master for this one-loop amplitudes --- assuming that a supersymmetric decomposition as discussed in section~\ref{sec:constraints} holds.
Hence, the full solution can be encoded via a single numerator (including several external configurations).
This provides an efficient way of obtaining color-kinematic dual representations.
Many examples for one and two loops have been worked out in paper~\ref{lbl:paper3} and we will discuss their structure in the next section.

It appears that it is a coincidence that the expression for the derived numerators via Jacobi identities/commutation relations and their expression through the off-shell lift of the cut agree.
Even without seeking a color-kinematics dual representation, it is not clear how one should massage each cut expression to get a consistent off-shell lift from all cuts.
For some cases with external matter states at two-loop level, only a subset of the master numerators has been constructed in this way.
For these examples, it seems that a simple off-shell lift is incompatible with the color-kinematics duality.
Hence, one would need to work much harder in order to bring the cut expressions into a form that upon an off-shell lift respects all diagram symmetries and the color-kinematics duality.
