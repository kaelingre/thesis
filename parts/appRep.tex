\chapter{Summary of On-Shell Supermultiplets and their Double Copy}\label{app:rep}

We review the particle content of four and six dimensional on-shell supermultiplets in terms of its representation theory of the little/helicity and R-symmetry group.
Furthermore, we summarize how the tensor product represents the particle content of double copied theories.

The little (helicity) group of massless particles in four dimensions is given by $\text{U}(1)$.
We denote a representation thereof by its helicity~$\pm$ together with the spin.
Representations in the R-symmetry group~$\text{U}(\cN)$ are given by its integer dimension.
Table~\ref{tbl:repr4D} summarizes all multiplets in $\cN=8,6,4,2,1$ up to spin~2, see e.g.~\cite{Freedman:2012zz} for the construction of this table.

\newcommand{\tbloplus}[1]{\multicolumn{1}{c@{~$\oplus$}}{#1}}
\newcommand{\tblplus}[1]{\multicolumn{1}{c@{~$+$}}{#1}}
\newcommand{\tbleq}[1]{\multicolumn{1}{c@{~$=$}}{#1}}
\newcommand{\tblspace}[1]{\multicolumn{1}{c@{~$\quad$}}{#1}}
\begin{table}
  \centering
  \begin{tabular}[t]{l l c c c c c }
    \toprule
    $\cN$ & & \multicolumn{5}{c}{representation/counting}\\
    \midrule
    \multirow{4}{*}{8} & \multirow{4}{*}{$\cH$} & \tbloplus{$(\pm 2,1)$} & \tbloplus{$(\pm\frac{3}{2},8)$} & \tbloplus{$(\pm1,28)$} & \tbloplus{$(\pm\frac{1}{2},56)$} \\
    & & \tblplus{2} & \tblplus{16} & \tblplus{56} & \tblplus{128} \\
    & & $(0,70)$\\
    & & \tbleq{70} & 256\\
    \midrule
    \multirow{4}{*}{6} & \multirow{4}{*}{$\cH$} & \tbloplus{$(\pm 2,1)$} & \tbloplus{$(\pm\frac{3}{2},6)$} & \tbloplus{$(\pm 1,15+1)$} & \tbloplus{$(\pm\frac{1}{2}, 20+6)$} \\
    & & \tblplus{2} & \tblplus{12} & \tblplus{32} & \tblplus{52} &\\
    & & $(0,15+15)$\\
    & & \tbleq{30} & 128\\
    \midrule
    \multirow{6}{*}{4} & \multirow{4}{*}{$\cH$} & \tbloplus{$(\pm2,1)$} & \tbloplus{$(\pm\frac{3}{2},4)$} & \tbloplus{$(\pm1,6)$} & \tbloplus{$(\pm\frac{1}{2},4)$} \\
    & & \tblplus{2} & \tblplus{8} & \tblplus{12} & \tblplus{8} &\\
    & & $(0,1+1)$ \\
    & & \tbleq{2} & 32\\
    \cmidrule{2-7} 
    & \multirow{2}{*}{$\cV$} & \tbloplus{$(\pm1,1)$} & \tbloplus{$(\pm\frac{1}{2},4)$} & $(0,6)$ \\
    & & \tblplus{2} & \tblplus{8} & \tbleq{6} & 16 \\
    \midrule
    \multirow{8}{*}{2} & \multirow{2}{*}{$\cH$} & \tbloplus{$(\pm2,1)$} & \tbloplus{$(\pm\frac{3}{2},2)$} & $(\pm1,1)$ \\
    & & \tblplus{2} & \tblplus{4} & \tbleq{2} & 8\\
    \cmidrule{2-7}
    & \multirow{2}{*}{$\cV$} & \tbloplus{$(\pm1,1)$} & \tbloplus{$(\pm\frac{1}{2},2)$} & $(0,2)$ \\
    & & \tblplus{2} & \tblplus{4} & \tbleq{2} & 8\\
    \cmidrule{2-7}
    & \multirow{4}{*}{$\Phi$} & \tbloplus{$(\pm\frac{1}{2},1)$} & $(0,2)$\\
    & & \tblplus{2} & \tbleq{2} & 4\\
    & & \tbloplus{$(\pm\frac{1}{2},1)$} & $(0,2)$\\
    & & \tblplus{2} & \tbleq{2} & 4\\
    \midrule
    \multirow{8}{*}{1} & \multirow{2}{*}{$\cH$} & \tbloplus{$(\pm2,1)$} & $(\pm\frac{3}{2},1)$\\
    & & \tblplus{2} & \tbleq{2} & 4\\
    \cmidrule{2-7}
    & \multirow{2}{*}{$\cV$} & \tbloplus{$(\pm1,1)$} & $(\pm\frac{1}{2},1)$\\
    & & \tblplus{2} & \tbleq{2} & 4\\
    \cmidrule{2-7}
    & \multirow{4}{*}{$\Phi$} & \tbloplus{$(+\frac{1}{2},1)$} & $(0,1)$\\
    & & \tblplus{1} & \tbleq{1} & 2\\
    & & \tbloplus{$(0,1)$} & $(-\frac{1}{2},1)$\\
    & & \tblplus{1} & \tbleq{1} & 2\\
    \bottomrule
  \end{tabular}
  \vspace{5pt}
  \caption{This table summarizes the particle content of various four dimensional on-shell supermultiplet. The first row gives the helicity with the spin and the dimensionality of the R-symmetry representation. The second row counts the number of states.}
  \label{tbl:repr4D}
\end{table}

For six (and higher) dimensional theories, the supermultiplets for gravity and gauge theories have been nicely reviewed in~\cite{deWit:2002vz}.
We summarize these results here.
Six-dimensional states are categorized by their little group~$\text{SU}(2)\times\text{SU}(2)$ and R-symmetry group~$\text{USp}(2\cN)\times\text{USp}(2\widetilde{\cN})$ representations.
There exist six different types of states categorized by the little group.
Graviton states transform under the $(3,3)$ representation, gravitini either under $(2,3)$ or $(3,2)$.
For spin one states there are the usual vectors transforming under $(2,2)$, but we also include tensor states transforming under $(1,3)$ or $(3,1)$.
Corresponding representations for gauginos are $(1,2)$ and $(2,1)$.
Scalars finally trivially transform under a $(1,1)$ representation of the little group.
The graviton~$\cH$, vector~$\cV$, tensor~$\cT$ and matter~$\Phi$ on-shell supermultiplets are summarized in table~\ref{tbl:repr6D}.


\begin{table}
  \centering
  \begin{tabular}[t]{l l c c c c c c c c c}
    \toprule
    $(\cN,\widetilde{\cN})\!$ & & \multicolumn{4}{c}{representation/counting}\\
    \midrule
    \multirow{6}{*}{$(2,2)$} & \multirow{6}{*}{$\cH$} & \tbloplus{$(3,3;1,1)$} & \tbloplus{$(3,2;4,1)$} & \tbloplus{$(2,3;1,4)$} & \tbloplus{$(2,2;4,4)$} \\
    & & \tblplus{9} & \tblplus{$6\cdot4$} & \tblplus{$6\cdot4$} & \tblplus{$4\cdot{16}$}\\
    & & \tbloplus{$(3,1;1,5)$} & \tbloplus{$(1,3;5,1)$} & \tbloplus{$(2,1;4,5)$} & \tbloplus{$(1,2;5,4)$} \\
    & & \tblplus{$3\cdot5$} & \tblplus{$3\cdot5$} & \tblplus{$2\cdot20$} & \tblplus{$2\cdot20$}\\
    & & \tblspace{$(1,1;5,5)$}\\
    & & \tbleq{25} & 256\\
    \midrule
    \multirow{6}{*}{$(2,1)$} & \multirow{6}{*}{$\cH$} & \tbloplus{$(3,3;1,1)$} & \tbloplus{$(3,2;1,2)$} & \tbloplus{$(2,3;4,1)$} &\tbloplus{$(2,2;4,2)$}\\
    & & \tblplus{9} & \tblplus{$6\cdot2$} & \tblplus{$6\cdot4$} & \tblplus{$4\cdot{8}$}\\
    & & \tbloplus{$(1,3;5,1)$} & \tbloplus{$(3,1;1,1)$} & \tbloplus{$(1,2;5,2)$} & \tbloplus{$(2,1;4,1)$}\\
    & & \tblplus{$3\cdot{5}$} & \tblplus{3} & \tblplus{$2\cdot10$} & \tblplus{$2\cdot4$}\\
    & & \tblspace{$(1,1;5,1)$}\\
    & & \tbleq{5} & 128\\
    \midrule
    \multirow{8}{*}{$(1,1)$} & \multirow{6}{*}{$\cH$} & \tbloplus{$(3,3;1,1)$} & \tbloplus{$(3,2;1,2)$} & \tbloplus{$(2,3;2,1)$} & \tbloplus{$(2,2;2,2)$}\\
    & & \tblplus{9} & \tblplus{$6\cdot2$} & \tblplus{$6\cdot2$} & \tblplus{$4\cdot4$}\\
    & & \tbloplus{$(1,3;1,1)$} & \tbloplus{$(3,1;1,1$)} & \tbloplus{$(1,2;1,2)$} & \tbloplus{$(2,1;2,1)$}\\
    & & \tblplus{3} & \tblplus{3} & \tblplus{$2\cdot2$} & \tblplus{$2\cdot2$}\\
    & & \tblspace{$(1,1;1,1)$}\\
    & & \tbleq{1} & 64\\
    \cmidrule{2-7}
    & \multirow{2}{*}{$\cV$} & \tbloplus{$(2,2;1,1)$} & \tbloplus{$(2,1;1,2)$} & \tbloplus{$(1,2;2,1)$} & $(1,1;2,2)$\\
    & & \tblplus{4} & \tblplus{$2\cdot2$} & \tblplus{$2\cdot2$} & \tbleq{$1\cdot4$} & $\!16$\\
    \midrule
    \multirow{4}{*}{$(2,0)$} & \multirow{2}{*}{$\cH$} & \tbloplus{$(3,3;1,1)$} & \tbloplus{$(2,3;4,1)$} & \tblspace{$(1,3;5,1)$}\\
    & & \tblplus{9} & \tblplus{$6\cdot4$} & \tbleq{$3\cdot5$} & 48\\
    \cmidrule{2-7}
    & \multirow{2}{*}{$\cT$} & \tbloplus{$(3,1;1,1)$} & \tbloplus{$(2,1;4,1)$} & \tblspace{$(1,1;5,1)$}\\
    & & \tblplus{3} & \tblplus{$2\cdot4$} & \tbleq{5} & 16\\
    \midrule
    \multirow{10}{*}{$(1,0)$} & \multirow{2}{*}{$\cH$} & \tbloplus{$(3,3;1,1)$} & \tbloplus{$(2,3;2,1)$} & \tblspace{$(1,3;1,1)$}\\
    & & \tblplus{9} & \tblplus{$6\cdot2$} & \tbleq{3} & 24\\
    \cmidrule{2-7}
    & \multirow{2}{*}{$\cV$} & \tbloplus{$(2,2;1,1)$} & \tblspace{$(1,2;2,1)$}\\
    & & \tblplus{4} & \tbleq{$2\cdot2$} & 8\\
    \cmidrule{2-7}
    & \multirow{2}{*}{$\cT$} & \tbloplus{$(3,1;1,1)$} & \tbloplus{$(1,2;2,1)$} & \tblspace{$(1,1;1,1)$}\\
    & & \tblplus{3} & \tblplus{$2\cdot2$} & \tbleq{1} & 8\\
    \cmidrule{2-7}
    & \multirow{4}{*}{$\Phi$} & \tbloplus{$(2,1;1,1)$} & \tblspace{$(1,1;2,1)$}\\
    & & \tblplus{2} & \tbleq{$1\cdot2$} & 2\\
    & & \tbloplus{$(1,2;1,1)$} & \tblspace{$(1,1;2,1)$}\\
    & & \tblplus{2} & \tbleq{$1\cdot2$} & 2\\
    \bottomrule
  \end{tabular}
  \vspace{5pt}
  \caption{This table summarizes the particle content of various six dimensional supermultiplet.
    The first row gives the dimensionalities $(m,n;\tilde{m},\tilde{n})$ of representations of the little and R-symmetry group $\text{SU}(2)\times\text{SU}(2)\times\text{USp}(2\cN)\times\text{USp}(2\widetilde{\cN})$.
    The second row counts the number of states.}
  \label{tbl:repr6D}
\end{table}

The double copy can be used to construct supergravity on-shell multiplets (or sums thereof) as explained in section~\ref{sec:sugra}.
We summarize construction of gravity supermultiplets as tensor products of vector and matter multiplets for $\cN=0,1,2,4,6,8$ supergravity in table~\ref{tbl:doubleCopy4D}.

\newcommand{\tbleqR}[1]{\multicolumn{1}{r@{~$=$}}{#1}}
\begin{table}
  \centering
  \begin{tabular}[t]{l r l}
    \toprule
    \multirow{3}{*}{$\cN = 0 + 0$} & \tbleqR{$A^\mu\otimes A^\mu$} & $h^{\mu\nu}\oplus\phi\oplus\overline{\phi}$ \\
    & \tbleqR{$\psi^+\otimes\psi^-$} & $\overline{\phi}$\\
    & \tbleqR{$\psi^-\otimes\psi^+$} & $\phi$\\
    \midrule
    \multirow{3}{*}{$\cN = 1 + 0$} & \tbleqR{$\cV_{\cN=1}\otimes A^\mu$} & $\cH_{\cN=1}\oplus\Phi_{\cN=1}\oplus\overline{\Phi}_{\cN=1}$\\
    & \tbleqR{$\Phi_{\cN=1}\otimes\psi^-$} & $\overline{\Phi}_{\cN=1}$\\
    & \tbleqR{$\overline{\Phi}_{\cN=1}\otimes\psi_+$} & $\Phi_{\cN=1}$\\
    \midrule
    \multirow{3}{*}{$\cN = 2 + 0$} & \tbleqR{$\cV_{\cN=2}\otimes A^\mu$} & $\cH_{\cN=2}\oplus\cV_{\cN=2}$\\
    & \tbleqR{$\Phi_{\cN=2}\otimes\psi_-$} & $\overline{V}_{\cN=2}$\\
    & \tbleqR{$\overline{\Phi}_{\cN=2}\otimes\psi_+$} & $V_{\cN=2}$\\
    \midrule
    \multirow{3}{*}{$\cN= 1 + 1$} & \tbleqR{$\cV_{\cN=1}\otimes\cV_{\cN=1}$} & $\cH_{\cN=2}\oplus\Phi_{\cN=2}\oplus\overline{\Phi}_{\cN=2}$\\
    & \tbleqR{$\Phi_{\cN=1}\otimes\overline{\Phi}_{\cN=1}$} & $\overline{\Phi}_{\cN=2}$\\
    & \tbleqR{$\overline{\Phi}_{\cN=1}\otimes\Phi_{\cN=1}$} & $\Phi_{\cN=2}$\\
    \midrule
    $\cN= 4 + 0$ & \tbleqR{$\cV_{\cN=4}\otimes A^\mu$} & $\cH_{\cN=4}$\\
    \midrule
    \multirow{3}{*}{$\cN= 2 + 2$} & \tbleqR{$\cV_{\cN=2}\otimes\cV_{\cN=2}$} & $\cH_{\cN=4}\oplus\cV_{\cN=4}\oplus\overline{\cV}_{\cN=4}$\\
    & \tbleqR{$\Phi_{\cN=2}\otimes\overline{\Phi}_{\cN=2}$} & $\overline{\cV}_{\cN=4}$\\
    & \tbleqR{$\overline{\Phi}_{\cN=2}\otimes\Phi_{\cN=2}$} & $\cV_{\cN=4}$\\
    \midrule
    $\cN= 4 + 2$ & \tbleqR{$\cV_{\cN=4}\otimes\cV_{\cN=2}$} & $\cH_{\cN=6}$\\
    \midrule
    $\cN= 4 + 4$ & \tbleqR{$\cV_{\cN=4}\otimes\cV_{\cN=4}$} & $\cH_{\cN=8}$\\
    \bottomrule
  \end{tabular}
  \vspace{5pt}
  \caption{This table summarizes the double copy construction for various on-shell supermultiplets in $\cN=0,1,2,4,6,8$ supergravity in four dimensions. For the notation of the various multiplets see chapter~\ref{ch:susy}.}
  \label{tbl:doubleCopy4D}
\end{table}

The same construction for various six dimensional supergravity theories is given in table~\ref{tbl:doubleCopy6D}, see also paper~\ref{lbl:paper1}.
It can be derived by computing the tensor product of the $\text{SU}(2)\times\text{SU}(2)$ little group representations and fitting them into multiplets in table~\ref{tbl:repr6D}.

\begin{table}
  \centering
  \begin{tabular}[t]{l r l}
    \toprule
    \multirow{5}{*}{$\cN=(0,0)\otimes(0,0)$} & \tbleqR{${A^a}_{\dot{a}}\otimes {A^b}_{\dot{b}}$} & ${h^{ab}}_{\dot{a}\dot{b}}\oplus B^{ab}\oplus B_{\dot{a}\dot{b}}\oplus\phi$\\
    & \tbleqR{$\chi^a\otimes\tilde{\chi}_{\dot{a}}$} & ${A^a}_{\dot{a}}$\\
    & \tbleqR{$\tilde{\chi}_{\dot{a}}\otimes\chi^a$} & ${A^a}_{\dot{a}}$\\
    & \tbleqR{$\chi^a\otimes\chi^b$} & $B^{ab}\oplus\phi$\\
    & \tbleqR{$\tilde{\chi}_{\dot{a}}\otimes\tilde{\chi}_{\dot{b}}$} & $B_{\dot{a}\dot{b}}\oplus\phi$\\
    \midrule
    \multirow{4}{*}{$\cN=(1,0)\otimes(0,0)$} & \tbleqR{$\cV_{\cN=(1,0)}\otimes{A^a}_{\dot{a}}$} & $\cH_{\cN=(1,0)}\oplus\cT_{\cN=(1,0)}$\\
    & \tbleqR{$\Phi_{\cN=(1,0)}\otimes\chi^a$} & $\cT_{\cN=(1,0)}$\\
    & \tbleqR{$\Phi_{\cN=(1,0)}\otimes\tilde{\chi}_{\dot{a}}$} & $\cV_{\cN=(1,0)}$\\
    & \tbleqR{$\overline{\Phi}_{\cN=(1,0)}\otimes\chi^a$} & $\cV_{\cN=(1,0)}$\\
    \midrule
    \multirow{2}{*}{$\cN=(1,0)\otimes(1,0)$} & \tbleqR{$\cV_{\cN=(1,0)}\otimes\cV_{\cN=(1,0)}$} & $\cH_{\cN=(2,0)}\oplus\cT_{\cN=(2,0)}$\\
    & \tbleqR{$\Phi_{\cN=(1,0)}\otimes\Phi_{\cN=(1,0)}$} & $\cT_{\cN=(2,0)}$\\
    \midrule
    $\cN=(1,1)\otimes(0,0)$ & \tbleqR{$\cV_{\cN=(1,1)}\otimes{A^a}_{\dot{a}}$} & $\cH_{\cN=(1,1)}$\\
    \midrule
    \multirow{3}{*}{$\cN=(1,0)\otimes(0,1)$} & \tbleqR{$\cV_{\cN=(1,0)}\otimes\cV_{\cN=(0,1)}$} & $\cH_{\cN=(1,1)}$\\
    & \tbleqR{$\Phi_{\cN=(1,0)}\otimes\overline{\Phi}_{\cN=(0,1)}$} & $\cV_{\cN=(1,1)}$\\
    & \tbleqR{$\overline{\Phi}_{\cN=(1,0)}\otimes\Phi_{\cN=(0,1)}$} & $\cV_{\cN=(1,1)}$\\
    \midrule
    $\cN=(1,1)\otimes(1,0)$ & \tbleqR{$\cV_{\cN=(1,1)}\otimes\cV_{\cN=(1,0)}$} & $\cH_{\cN=(2,1)}$\\
    \midrule
    $\cN=(1,1)\otimes(1,1)$ & \tbleqR{$\cV_{\cN=(1,1)}\otimes\cV_{\cN=(1,1)}$} & $\cH_{\cN=(2,2)}$\\
    \bottomrule
  \end{tabular}
  \vspace{5pt}
  \caption{This table summarizes the double copy construction for various on-shell supermultiplets in $\cN=(0,0),(1,0),(2,0),(1,1),(2,1),(2,2)$ supergravity in six dimensions. For the notation of the various multiplets see chapter~\ref{ch:susy}.}
  \label{tbl:doubleCopy6D}
\end{table}

