\chapter{Higher Loop Generalizations}\label{ch:colorHigherLoop}

The discussion in this section is rather speculative and is supported only by a few explicit checks at lower multiplicity at two- and three-loop level.
Starting from two loops it is not possible to find a basis of planar kinematic building blocks.
The discussion at tree and one-loop level do not suggest an obvious generalizations for the definition of primitive objects.

For tree-level amplitudes the primitive gauge-invariant building blocks were color-ordered amplitudes on a disk, or equivalently on a sphere with one boundary.
At one-loop we defined the elementary kinematic objects as amplitudes on an annulus --- or a sphere with two boundaries.
It was possible to find a basis without having to include non-planar objects.
Nevertheless it has been checked for up to 10 points for certain cases, that also the non-planar primitive amplitudes are gauge invariant.
It can essentially be checked by choosing a different basis of primitive amplitudes, including non-planar objects, leading to a different color decomposition.
If the corresponding color coefficients are independent, i.e. the decomposition is over a minimal color basis, the primitive amplitudes are necessarily gauge invariant.

To get some further input we require that primitive amplitudes factor into lower loop amplitudes upon cutting into them --- or conversely suggest a recursive construction via a BCFW-like procedure.
We can already see this structure by cutting into one-loop primitive amplitudes.
A cut through the annulus from the outside to the inside, fixing the legs between which we start and end, leads to properly defined tree-level limits since we have fixed the routing of the quark objects, in particular the quark loop.
In reverse the gluing of two external legs in a tree-level amplitude to form a loop leads to the topology of an annulus, with exactly the properties we described for a primitive one-loop amplitude.
For example we can map the following diagram between the one-loop cut and the tree-level amplitude
\begin{equation}
  \begin{aligned}
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize,
        scale=1.3]
      \draw[black!40!blue] (-1,-0.5) .. controls (-1.3,-0.3) and (-1.3,0.3) .. (-1,0.5) node[pos=0.5,inner sep=0] (lD) {};
      \draw[line,gray] (0,-0.5) .. controls (-0.3,-0.3) and (-0.3,0.3) .. (0,0.5) node[pos=0.5,inner sep=0] (mB) {};
      \draw[gluon,gray] ($(lD) + (-0.01,0)$) node[left] {$2$} -- ($(mB) + (0.01,0)$);
      
      \draw[black!40!blue] (-1,0.5) .. controls (-0.7,0.3) and (-0.7,-0.3) .. (-1,-0.5) node[pos=0.3,inner sep=0] (lA) {} node[pos=0.9,inner sep=0] (lB) {} node[pos=0.25,inner sep=0] (lCut) {};
      \draw[white!60!blue] (1,-0.5) .. controls (0.7,-0.3) and (0.7,0.3) .. (1,0.5);
      \draw[black!40!blue] (1,0.5) .. controls (1.3,0.3) and (1.3,-0.3) .. (1,-0.5) node[pos=0.5,inner sep=0] (rA) {} node[pos=0.25,inner sep=0] (rCut) {};
      \draw[black!40!blue] (-1,0.5) -- (1,0.5);
      \draw[black!40!blue] (-1,-0.5) -- (1,-0.5);
      
      \draw[quark] (0,-0.5) .. controls (0.3,-0.3) and (0.3,0.3) .. (0,0.5) node[pos=0.2,inner sep=0] (mA) {} node[pos=0.6,inner sep=0] (mC) {};
      
      \draw[quark] ($(lA) + (-0.01,0)$) node[left] {$\ol{1}$} .. controls ($(lA) + (0.45,0)$) and ($(lB) + (0.6,0)$) .. ($(lB) + (-0.01,0)$) node[above left=-0.05] {$\ul{1}$} node[pos=0.8,inner sep=0] (lC) {};
      \draw[gluon] ($(lC) + (-0.01,0)$) -- ($(mA) + (0.01,0)$);
      \draw[gluon] ($(mC) + (-0.01,0)$) -- ($(rA) + (0.01,0)$) node[right] {$3$};
      \draw[red,dashed] ($(lCut) + (-0.01,0)$)  -- ($(rCut) + (0.01,0)$) node[pos=0.66,above=-0.11] {\ScissorLeftBrokenBottom};
    \end{tikzpicture}
    &\longleftrightarrow
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize,
        scale=1]
      \draw[black!40!blue] (0,0) circle (0.8);
      \draw[quark] (0:0.8) node[right] {$\ol{1}$} .. controls (0:0.5) and (-60:0.5) .. (-60:0.8) node[below] {$\ul{1}$} node[pos=0.7,inner sep=0] (A) {};
      \draw[quark] (180:0.8) node[left] {$\ol{\ell}$} .. controls (180:0.5) and (60:0.5) .. (60:0.8) node[above right] {$\ul{\ell}$} node[pos = 0.2,inner sep=0] (B) {} node[pos=0.3,inner sep=0] (C) {} node[pos=0.7,inner sep=0] (D) {};
      \draw[gluon] (-120:0.8) node[below] {$2$} -- ($(B) + (0,0.01)$);
      \draw[gluon] ($(A) + (0.01,0)$) -- ($(C) + (-0.01,0.01)$);
      \draw[gluon] (120:0.8) node[above left] {$3$} -- ($(D) + (0.01,0)$);
    \end{tikzpicture}\,,\\
    A^q(\ol{1},\ul{1},2\mid 3) &\longleftrightarrow A^{(0)}(\ol{1},\ul{1},2,\ol{\ell},3,\ul{\ell})\,.
  \end{aligned}
\end{equation}

To proceed to higher loop one can also imagine to glue not only external legs from the same boundary but from two different boundaries.
For example gluing two legs from a one-loop amplitude by connecting a leg from the inside with a leg from the outside leads to the topology of a torus with a single boundary.
\begin{equation}
  \begin{aligned}
    \hspace{-30pt}
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize,
        scale=1.3]
      \draw[gluon,gray] (0.1,0.5) .. controls (-0.2,0.3) and (-0.2,-0.3) .. (0.1,-0.5);
      
      \draw[black!40!blue] (-1,-0.5) .. controls (-1.3,-0.3) and (-1.3,0.3) .. (-1,0.5) node[pos=0.5,inner sep=0] (lC) {};

      \draw[gluon,gray] ($(lC) + (0.01,0)$) node[left] {$2$} -- (-0.15,0);
      
      \draw[black!40!blue] (-1,0.5) .. controls (-0.7,0.3) and (-0.7,-0.3) .. (-1,-0.5) node[pos=0.35,inner sep=0] (lA) {} node[pos=0.9,inner sep=0] (lB) {} node[pos=0.2,inner sep=0] (lLoop) {};
      \draw[white!60!blue] (1,-0.5) .. controls (0.7,-0.3) and (0.7,0.3) .. (1,0.5);
      \draw[black!40!blue] (1,0.5) .. controls (1.3,0.3) and (1.3,-0.3) .. (1,-0.5) node[pos=0.5,inner sep=0] (rA) {} node[pos=0.2,inner sep=0] (rLoop) {};
      \draw[black!40!blue] (-1,0.5) -- (1,0.5);
      \draw[black!40!blue] (-1,-0.5) -- (1,-0.5);
      
      \draw[quark] ($(lA) + (-0.01,0)$) node[left] {$\ol{1}$} .. controls ($(lA) + (0.45,0)$) and ($(lB) + (0.6,0)$) .. ($(lB) + (-0.01,0)$) node[above left=-0.05] {$\ul{1}$} node[pos=0.7,inner sep=0] (lQ) {};
      \draw[quark] ($(lLoop) + (-0.01,0)$)  -- ($(rLoop) + (0.01,0)$) node[pos=0.55,inner sep=0] (mA) {} node[pos=0.3,inner sep=0] (mB) {};
      \draw[dscalar,dash pattern=on 3pt off 2pt] ($(rLoop) + (-0.01,0)$) .. controls ($(rLoop) + (1.5,0.6)$) and ($(lLoop) + (-1.5,0.6)$) .. ($(lLoop) + (0.01,0)$);
      \draw[gluon] ($(mA) + (0,-0.01)$) .. controls ($(mA) + (0,0.2)$) and (-0.1,0.45) .. (0,0.45);
      \draw[gluon] (0,-0.5) .. controls (0.25,-0.3) and ($(mB) + (0.3,-0.2)$) .. ($(mB) + (0,-0.01)$);
      \draw[gluon] ($(rA) + (0.01,0)$) node[right] {$3$} -- (0.08,0);
      \draw[gluon] ($(lQ) + (-0.01,0)$) -- (0.15,-0.3);
    \end{tikzpicture}
    \hspace{-40pt}&\longleftrightarrow
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize,
        scale=1.3]
      \draw[gluon,gray] (1.4,0.15) .. controls (1,0.3) and (0.9,0.7) .. (1.1,0.9);
      \draw[gluon,gray] (0.65,-0.7) .. controls (0.45,-0.5) and (0.8,0.1) .. (1.2,0.3);
      
      \draw[black!40!blue] (0,-0.5) .. controls (0.5,-0.5) and (1,-1) .. (1.5,-1)
      arc (-90:0:1);
      \draw[black!40!blue] (0,0.5) .. controls (0.5,0.5) and (1,1) .. (1.5,1)
      arc (90:0:1);
      
      \draw[black!40!blue] (0,-0.5) .. controls (-0.3,-0.3) and (-0.3,0.3) .. (0,0.5) node[pos=0.7,inner sep=0] (lC) {};

      \draw[gluon,gray] ($(lC) + (0.01,0)$) node[left] {$2$} .. controls ($(lC) + (0.1,0.1)$) and (0.8,0.6) .. (1,0.6);

      \draw[black!40!blue] (0,-0.5) .. controls (-0.3,-0.3) and (-0.3,0.3) .. (0,0.5) node[pos=0.72,inner sep=0] (lC) {};

      \draw[black!40!blue] (0,-0.5) .. controls (0.3,-0.3) and (0.3,0.3) .. (0,0.5) node[pos=0.5,inner sep=0] (lA) {} node[pos=0.1,inner sep=0] (lB) {} node[pos=0.68,inner sep=0] (lD) {};
      
      \draw[black!40!blue] (1,0) .. controls (1.2,0.2) and (1.6,0.2) .. (1.8,0);
      \draw[black!40!blue] (1.1,0.03) .. controls (1.3,-0.1) and (1.5,-0.1) .. (1.7,0.03);
      \draw[aquark] (0.8,0) arc (-180:180:0.6);
      \draw[gluon] (1.35,0.15) .. controls (1.4,0.3) and (1.3,0.5) .. (1.2,0.55);
      \draw[gluon] (1.45,0.6) .. controls (1.35,0.7) and (1.45,0.9) .. (0.98,0.88);

      \draw[quark] ($(lA) + (-0.01,0)$) node[left] {$\ol{1}$} .. controls ($(lA) + (0.35,0)$) and ($(lB) + (0.6,0)$) .. ($(lB) + (-0.01,0)$) node[above left=-0.05] {$\ul{1}$} node[pos=0.7,inner sep=0] (lQ) {};
      \draw[gluon] (0.6,-0.7) .. controls (0.6,-0.5) and ($(lQ) + (0.1,-0.1)$) .. ($(lQ) + (-0.01,0)$);
      \draw[gluon] ($(lD) + (-0.01,0)$) node[left] {$3$} .. controls ($(lD) + (0.7,0)$) and (0.5,-0.8) .. (1.5,-0.8)
      arc (-90:97:0.77);
    \end{tikzpicture}\,,\\
    A^g(\ol{1},\ul{1},2,\ol{\ell}\mid 3,\ul{\ell}) &\longleftrightarrow A^{(qg)}(\ol{1},\ul{1},2,3)\,.
  \end{aligned}
\end{equation}

Hence we propose to define primitive amplitudes via diagrams on (possibly higher genus) Riemann surfaces with boundaries where the external partons live.
Each additional boundary requires another loop, and increasing the genus by one allows for two more loops.
This leads to a count for the number of loops $L = 2g + b - 1$ where $g$ is the genus of the surface and $b$ counts the number of boundaries.
The required information to uniquely specify a primitive amplitude is then the cyclic ordering of external legs for each boundary and the routing of quark lines, i.e. on which side of every quark parton (loop or external line) each element (loop or external leg of any type) lies.

For amplitudes without any gluonic (or mixed) loop, higher genus surfaces are excluded as quark loops can never cross.
This is the simplest case from that point of view.
To support the above conjecture we have explicitly checked some four and five point amplitudes with two quark loops.
The number of independent primitive amplitudes coincides with the number of independent color factors --- and it is possible to construct a minimal color decomposition with any random minimal basis picked out of the set primitive amplitudes, proving the gauge independence of these objects.

The ultimate goal is a closed form for a color decomposition, i.e. a systematic choice of basis for primitive amplitudes for all combinations of external and internal partons.
We expect the color objects in such a color decomposition to have a similar 'Mario world' structure as for the tree and one-loop case.
Furthermore it would be interesting to find a generalization of the KK relations at one and higher loops including all types of particles.
An understanding of identities among primitive amplitudes will not only lead to a rigorous proof of the color decomposition but also allow the construction of all possible minimal color decompositions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \section{Dual Coordinates}

%% Loop level integrands suffer from loop momentum labeling ambiguities as, upon integration, the loop momenta can be arbitrarily shifted --- and from a diagrammatically point of view there is in general no distinguished propagator to carry a specific loop momentum.
%% For planar amplitudes this problem has been overcome via dual coordinates~\cite{Drummond:2006rz}, leading to a unique scheme of loop momentum labels for a given planar diagram and hence a proper definition of the corresponding integrand.
%% Diagrammatically one assigns a dual coordinate~$y_i$ to each ``zone'' in a graph and recovers the loop momentum of a propagator as the difference of the zone coordinates the line separates:
%% \begin{equation}
%%   %\tikzset{external/force remake}
%%   \gBox[scale=1.5,eLA=$1$,eLB=$2$,eLC=$3$,eLD=$4$,iLA=$y_2$,iLB=$y_3$,iLC=$y_4$,iLD=$y_1$]{
%%     \node at (0.5,0.35) {$\leftarrow\!\ell$};
%%     \node at (0.5,0.5) {$y_0$};
%%   }\,.
%% \end{equation}
%% The external momenta are given as $p_i = y_i - y_{i+1}$, and the loop momentum as $\ell=y_1-y_0$.
%% These definitions clearly enforce an ordering for the external legs.
%% The convention for the loop momentum is completely arbitrary.
%% The other three lines carrying loop momentum are given by the same rule of difference, subtracting the zone variable lying on the right from the one on the left:
%% \begin{equation}
%%   \ell-p_1=y_2-y_0\,, \quad \ell-p_1-p_2=y_3-y_0\,, \quad \ell+p_4=y_4-y_0\,.
%% \end{equation}
%% The dual coordinates have a second purpose.
%% They trivialize the momentum conservation for the external momenta.
%% Geometrically a set of momentum conserving external momenta form the edges of a polytope.
%% The cusps are given by the dual coordinates.
%% As only the relative position of the dual coordinates matter they can be arbitrarily moved by a global shift.

%% For non-planar amplitudes we cannot any longer talk about zones in the same sense, as the ordering is complicated by legs lying on different boundaries of a Riemann surface.
%% Additionally an ordering of these boundaries needs to be taken into account.
%% We propose a labeling scheme and as such a definition of dual variables.
%% We introduce the scheme via a simple example at two loops for the primitive amplitude $A(1,2,3,4|5|6)$ where we do not further specify the line types or quark routings.
%% A simple diagram contributing to this amplitude can be drawn on a plane:
%% \tikzset{
%%   rr/.style={black!20!red,line width=0.5pt,dash pattern=on 0.5pt off 0.5pt}
%% }
%% \begin{equation}
%%   %\tikzset{external/force remake}
%%   \begin{tikzpicture}
%%     [line width=1pt,
%%       baseline={([yshift=-0.5ex]current bounding box.center)},
%%       font=\scriptsize,
%%       scale=1.3
%%     ]
%%     \draw[black!50!green,dashed] (0,0) ellipse (3.08 and 1.3);
%%     \draw[black!50!green,dashed] (0.78,0) circle (0.2);
%%     \draw[black!50!green,dashed] (-0.78,0) circle (0.2);
    
%%     \draw (-1.5,-0.5) -- node[below right] (A1) {$y_1$} node[above left] (A2) {$y_2$} (-2,-1) node[below left] {$1$};
%%     \draw (-1.5,0.5) -- node[below left] (A4) {$y_2$} node[above right] (A5) {$y_3$} (-2,1) node[above left] {$2$};
%%     \draw (1.5,0.5) -- node[above left] (A8) {$y_3$} node[below right] (A9) {$y_4$} (2,1) node[above right] {$3$};
%%     \draw (1.5,-0.5) -- node[above right] (A11) {$y_4$} node[below left] (A12) {$y_5$} (2,-1) node[below right] {$4$};

%%     \draw (-1.5,0.5) -- node[left] (A3) {$y_2$} node[right] (A26) {$y_1$} (-1.5,-0.5);
%%     \draw (0,1) -- node[above] (A6) {$y_3$} node[below] (A25) {$y_1$} (-1.5,0.5);
%%     \draw (1.5,0.5) -- node[above] (A7) {$y_3$} node[below] (A16) {$y_5$} (0,1);
%%     \draw (1.5,-0.5) -- node[right] (A10) {$y_4$} node[left] (A15) {$y_5$} (1.5,0.5);
%%     \draw (0,-1) -- node[pos=0.7,above] (A14) {$y_5$}  node[pos=0.7,below] (A13) {$y_5$} node[pos=0.4,below] {$\oset{\rightarrow}{\ell_2}$} (1.5,-0.5);
%%     \draw (0,1) -- node[left] (A24) {$y_1$} node[right] (A17) {$y_5$} (0,0.333);
%%     \draw (0,0.333) -- node[left] (A22) {$y_6$} node[right] (A18) {$y_5$} (0,-0.333);
%%     \draw (0,-0.333) -- node[left=-0.01] (A21) {$y_6$} node[right=0.01] (A20) {$y_6$} (0,-1);
%%     \draw (0,0.333) -- node[above=0.05] (A23) {$y_1$} (-0.6,0) node[left=0.03] {$6$};
%%     \draw (0,-0.333) -- node[below=0.08] (A19) {$y_6$} (0.6,0) node[right=0.03] {$5$};
%%     \draw (-1.5,-0.5) -- node[pos=0.3,below] (A28) {$y_1$} node[pos=0.3,above] (A27) {$y_1$} node[pos=0.6,below] {$\oset{\leftarrow}{\ell_1}$} (0,-1);

%%     \draw[rr] (A1.north) to [bend right] (A2.east);
%%     \draw[rr] (A2.east) to [bend right] (A3.south);
%%     \draw[rr] (A3.north) to [bend right] (A4.east);
%%     \draw[rr] (A4.east) to [bend right] (A5.south);
%%     \draw[rr] (A5.south) to [bend right] (A6.west);
%%     \draw[rr] (A6.east) to [bend left] (A7.west);
%%     \draw[rr] (A7.east) to [bend right] (A8.south);
%%     \draw[rr] (A8.south) to [bend right] (A9.west);
%%     \draw[rr] (A9.west) to [bend right] (A10.north);
%%     \draw[rr] (A10.south) to [bend right] (A11.west);
%%     \draw[rr] (A11.west) to [bend right] (A12.north);
%%     \draw[rr] (A12.north) to [bend right] (A13.east);
%%     \draw[rr] (A13.east) to [bend right] (A14.east);
%%     \draw[rr] (A14.east) to [bend right] (A15.south);
%%     \draw[rr] (A15.north) to [bend right] (A16.east);
%%     \draw[rr] (A16.west) to (A17.east);
%%     \draw[rr] (A17.south) to (A18.north);
%%     \draw[rr] (A18.south) to [bend right] (A19.west);
%%     \draw[rr] (A19.west) to [bend right] (A20.north);
%%     \draw[rr] (A20.north) to [bend right] (A21.north);
%%     \draw[rr] (A21.north) to (A22.south);
%%     \draw[rr] (A22.north) to [bend right] (A23.east);
%%     \draw[rr] (A23.east) to [bend right] (A24.south);
%%     \draw[rr] (A24.west) to (A25.east);
%%     \draw[rr] (A25.west) to [bend right] (A26.north);
%%     \draw[rr] (A26.south) to [bend right] (A27.west);
%%     \draw[rr] (A27.west) to [bend right] (A28.west);
%%     \draw[rr] (A28.west) to [bend right] (A1.north);

%%     \node[black!50!green,fill=white] at (0,1.3) {$x_0$};
%%     \node[black!50!green,fill=white,inner sep=0] at (0.78,0.2) {$x_1$};
%%     \node[black!50!green,fill=white,inner sep=0] at (-0.78,0.2) {$x_2$};
%%   \end{tikzpicture}\,.
%% \end{equation}
%% Green dashed lines mark the three boundaries of the genus-zero Riemann surface.
%% To both sides of each line we assign a dual external variable~$y_i$ by following the external legs along the red dotted line.
%% We essentially assign dual zones following the color ordering and increase the variable by one each time we cross an external leg.
%% To have a consistent scheme we orbit, following the red line, each boundary in the same relative direction.
%% Additionally we assign a second type of dual loop variables~$x_i$ to each loop.

%% The momentum of each line are given by the difference of the sum of $y_i$ and $x_i$ on both sides.
%% For example all external legs are as in the planar case expressed as $p_i = y_i - y_{i+1}$ since the dual loop variable is always the same on both sides and cancels out in the difference, e.g.
%% \begin{equation}
%%   p_1 = (y_1+x_0) - (y_2+x_0)\,.
%% \end{equation}
%% For the loop momenta we have for example
%% \begin{equation}
%%   \begin{aligned}
%%     \ell_1 &= (y_1+x_0) -(y_1+x_2) = x_0-x_2\,, \\
%%     \ell_2 &= (y_5+x_0) - (y_5+x_1) = x_0-x_1\,,\\
%%     \ell_1 + p_1 &= (y_2+x_0) - (y_1+x_2)\,,\\
%%     \ell_1+\ell_2+p_5+p_6&= (y_5+x_1) - (y_1+x_2)\,,\\
%%     &\dots
%%   \end{aligned}
%% \end{equation}
%% The three dual loop variables introduce an artificial degree of freedom which we can remove by randomly choosing a value for one of them, e.g. $x_0=\ell_1$.
%% Planar dual variables also follow the scheme, for example for the above one-loop box by setting $x_0=\ell_1$ and $x_1=y_0$.

%% A more complicated situation occurs if we cannot immediately pass into the next loop after having gone around a boundary.
%% The following diagram contributing to the same amplitude explains the situation:
%% \begin{equation}
%%   %\tikzset{external/force remake}
%%   \begin{tikzpicture}
%%     [line width=1pt,
%%       baseline={([yshift=-0.5ex]current bounding box.center)},
%%       font=\scriptsize,
%%       scale=1.3
%%     ]
%%     \draw[black!50!green,dashed] (0.75,0) ellipse (2.43 and 1.73);
%%     \draw[black!50!green,dashed] (0.22,0) circle (0.2);
%%     \draw[black!50!green,dashed] (1.78,0) circle (0.2);

%%     \node[black!50!green,fill=white] at (0.75,1.73) {$x_0$};
%%     \node[black!50!green,fill=white,inner sep=0] at (1.78,0.2) {$x_1$};
%%     \node[black!50!green,fill=white,inner sep=0] at (0.22,0.2) {$x_2$};

%%     \draw (0,-1) -- node[below left] (A1) {$y_1$} node[above right] (A28) {$y_1$} (-1,0);
%%     \draw (-1,0) -- node[below] (A2) {$y_1$} node[above] (A3) {$y_2$} (-1.7,0) node[left] {$1$};
%%     \draw (-1,0) -- node[above left] (A4) {$y_2$} node[below right] (A27) {$y_1$} (0,1);
%%     \draw (0,1) -- node[below left] (A5) {$y_2$} node[above right] (A6) {$y_3$} (-0.5,1.5) node[above] {$2$};
%%     \draw (0,1) -- node[above] (A7) {$y_3$} node[below] (A26) {$y_1$} (1,1);
%%     \draw (1,1) to [bend left] node[above right] (A8) {$y_3$} node[below left] (A17) {$y_5$} (2.5,0);
%%     \draw (2.5,0) -- node[above] (A9) {$y_3$} node[below] (A10) {$y_4$} (3.2,0) node[right] {$3$};
%%     \draw (1,-1) to [bend right] node[below right] (A11) {$y_4$} node[above left] (A16) {$y_5$} (2.5,0);
%%     \draw (1,-1) -- node[below] (A12) {$y_4$} node[above] (A15) {$y_5$} (0,-1);
%%     \draw (0,-1) -- node[below right] (A13) {$y_4$} node[above left] (A14) {$y_5$} (-0.5,-1.5) node[below] {$4$};
%%     \draw (1,1) -- node[right] (A18) {$y_5$} node[left] (A25) {$y_1$} (1,0.333);
%%     \draw (1,0.333) -- node[above=0.05] (A24) {$y_1$} (0.4,0) node[left=0.1] {$6$};
%%     \draw (1,0.333) -- node[right] (A19) {$y_5$} node[left] (A23) {$y_6$} (1,-0.333);
%%     \draw (1,-0.333) -- node[below=0.08] (A20) {$y_6$} (1.6,0) node[right=0.1] {$5$};
%%     \draw (1,-0.333) -- node[right] (A21) {$y_6$} node[left] (A22) {$y_6$}(1,-1);
    
%%     \draw[rr] (A1.north) to [bend right] (A2.east);
%%     \draw[rr] (A2.east) to [bend right] (A3.east);
%%     \draw[rr] (A3.east) to [bend right] (A4.south);
%%     \draw[rr] (A4.east) to [bend right] (A5.east);
%%     \draw[rr] (A5.east) to [bend right] (A6.south);
%%     \draw[rr] (A6.south) to [bend right] (A7.west);
%%     \draw[rr] (A7.east) to [bend left=15] (A8.west);
%%     \draw[rr] (A8.south) to [bend right=5] (A9.west);
%%     \draw[rr] (A9.west) to [bend right] (A10.west);
%%     \draw[rr] (A10.west) to [bend right=5] (A11.north);
%%     \draw[rr] (A11.west) to [bend left=15] (A12.east);
%%     \draw[rr] (A12.west) to [bend right] (A13.north);
%%     \draw[rr] (A13.north) to [bend right] (A14.east);
%%     \draw[rr] (A14.east) to [bend left] (A15.west);
%%     \draw[rr] (A15.east) to [bend right=15] (A16.south);
%%     \draw[rr] (A16.east) to [bend right=60] (A17.east);
%%     \draw[rr] (A17.north) to [bend right] (A18.north);
%%     \draw[rr] (A18.south) to (A19.north);
%%     \draw[rr] (A19.south) to [bend right] (A20.west);
%%     \draw[rr] (A20.west) to [bend right] (A21.north);
%%     \draw[rr] (A21.north) to [bend right] (A22.north);
%%     \draw[rr] (A22.north) to (A23.south);
%%     \draw[rr] (A23.north) to [bend right] (A24.east);
%%     \draw[rr] (A24.east) to [bend right] (A25.south);
%%     \draw[rr] (A25.north) to [bend right] (A26.east);
%%     \draw[rr] (A26.west) to [bend right] (A27.north);
%%     \draw[rr] (A27.west) to [bend right] (A28.west);
%%     \draw[rr] (A28.west) to [bend right] (A1.north);
%%   \end{tikzpicture}\,.
%% \end{equation}
%% The definition of loop and external momenta in terms of dual variables stays the same and leads to a consistent assignment for all legs.
%% An extension to more complicated cases and higher genus surfaces seems possible, but a complete scheme for all cases requires a careful definition and of course many more checks or even a thorough proof.
