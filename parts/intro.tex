\chapter{Introduction}

The theoretical framework describing the most fundamental objects in nature and their interactions is known as quantum field theory (QFT).
A central realization of a QFT is the standard model (SM) of particle physics, unifying three out of the four fundamental forces.
It has been extremely successful in predicting the outcome of experiments and explaining natural phenomena in the quantum regime.
A commonly studied type of observables are scattering amplitudes (or the $S$-matrix).
They describe the outcome of scattering events involving two or more fundamental particles.

The last missing piece for the SM, the Higgs boson, has been experimentally confirmed in 2012 by a joint effort of experiments at the Large Hadron Collider (LHC) at CERN~\cite{Aad:2012tfa,Chatrchyan:2012xdj}.
In contrast, it is next to impossible to observe any effects due to a quantization of the fourth force: gravity.
Quantum effects occur at small scales, which requires particle collisions at high energy to capture them.
Due to the relatively weak strength of the gravitational interaction the required energy for experiments to uncover gravitational quantum effects is out of our reach.

From a theoretical point of view, the determination of many quantities in a QFT is plagued by infinities at high energies, called ultraviolet (UV) divergences.
For the theory described by the SM, a gauge theory, these unphysical divergences can be systematically removed by counterterms.
This allows the SM to do accurate predictions for observables.
An equivalent mechanism for quantum gravity is not known as the procedure would require infinitely many counterterms leading to a formalism without any predictive power.
A complete understanding of the underlying structure of UV divergences of extended models of gravity is lacking until today.

The perturbative study of scattering amplitudes in general gauge and gravity theories has seen an increased interest since the end of the last century.
New computational techniques, methods and representations~\cite{Chetyrkin:1981qh,Tkachov:1981wb,Kotikov:1990kg,Bern:1994zx,Bern:1994cg,Remiddi:1997ny,Witten:2003nn,Cachazo:2004kj,Britto:2004ap,Britto:2005fq,Cheung:2009dc,ArkaniHamed:2010kv,Cachazo:2013hca} have led to a better understanding of the mathematical structures encoding amplitudes.
This led to many novel results for seemingly untractable problems.
One of these structures was discovered in 2008 as a duality between color and kinematics~\cite{Bern:2008qj}.
It induces a novel representation of gravity amplitudes as a \emph{double copy} of two gauge theory amplitudes~\cite{Bern:2010ue}.
A similar relation between open and closed string amplitudes was discovered by Kawai, Lewellen and Tye (KLT)~\cite{Kawai:1985xq} already in 1986.
KLT relations are known at leading order of the perturbative expansion in the coupling, and at one-loop level in the field theory description~\cite{He:2016mzd,He:2017spx}.
The aforementioned color-kinematics duality and double copy by Bern, Carrasco, and Johansson (BCJ) has a conjectured realization for any order.
The duality organizes the amplitude in terms of trivalent graphs, similar to Feynman diagrams, and splits the contribution for each graph into a color and a kinematic part.
These two parts are related by the duality in the sense that they obey the same algebraic identities.
The perturbative expansion is then realized order by order in the number of loops in the involved diagrams --- or equivalently in powers of the coupling constant.

An extremely powerful consequence of the duality is the construction of gravity amplitudes via simpler gauge theory building blocks.
The simplest case of a gravity theory in four spacetime dimensions is the maximally supersymmetric extension of Einstein gravity, $\cN=8$ supergravity (SG)~\cite{Freedman:1976xh,Deser:1976eh,Cremmer:1978km,Cremmer:1978ds,Cremmer:1979up}.
Therein the double copy construction has resulted in an impressive computation of the UV behavior at five loops~\cite{Bern:2018jmv} in general spacetime dimension.
Symmetry arguments exclude a divergence by the absence of a valid counterterm up to a critical dimension $D_c<24/5$~\cite{Bjornsson:2010wm,Bjornsson:2010wu} at five loops.
The divergence has been confirmed --- and the explicit expression in the critical dimension has been presented.
Even though a particular UV divergence has been predicted to vanish in four dimensions by symmetry arguments~\cite{Green:2010sp,Holst:2007np,Beisert:2010jx,Vanhove:2010nf,Bjornsson:2010wm,Bjornsson:2010wu,Bossard:2011tq}, the explicit computation has revealed new patterns relating the divergence structure of different loop orders.

There are cases with a vanishing UV divergence, where a valid counterterm exists and cannot be ruled out by any known (symmetry) argument.
This effect has been named \emph{enhanced cancellation}~\cite{Bern:2012cd,Bern:2014sna,Bern:2012gh,Bern:2017lpv,Bossard:2012xs,Bossard:2013rza,Bern:2013qca}.
The cancellations leading to a vanishing divergence are highly non-trivial from the point of view of currently known amplitude representations, i.e. it requires a conspiracy between contributions coming from different diagrammatic structures.

This thesis is centered around the BCJ construction and discusses various aspects thereof for lower degree or even in absence of supersymmetry.
Supersymmetric theories form a clean testing ground for new methods and ideas and allow us to do explicit computations at perturbative orders that are not yet accessible for the SM or Einstein gravity.
The basic building blocks are purely \emph{kinematic} numerator factors associated to trivalent graphs of a gauge theory.
These numerators --- counterparts of the color objects through the duality --- are far from being unique.
They are fixed by the duality up to a residual generalized gauge transformation, which leaves room for many different representations of the same amplitude.
The search for an underlying kinematic Lie algebra~\cite{Monteiro:2011pc,Cheung:2016prv} that would allow for a direct construction of such numerators has as of yet not been successful.

The main idea followed here is based upon an Ansatz construction for the building blocks of the gauge theory.
Apart from the duality property of these numerators, a larger set of constraints manifesting symmetries or various other properties have been identified in publications enclosed in this thesis.
The resulting representations of numerators are often conveniently expressed as a single trace over contractions of momenta with Dirac matrices.
This form of the integrand is especially useful when it comes to integration or assembling of gravitational amplitudes via the double copy.

If the goal is to only compute amplitudes in a gauge theory, seeking a color-kinematics dual representation might seem unnecessary.
However, it is clear from publication~\ref{lbl:paper3} that such a representation may be easy to obtain in some cases, and exposes novel properties.
For example, the amplitudes obtained in publication~\ref{lbl:paper1} and~\ref{lbl:paper3} are manifestly local, and exhibit an infrared (IR) behavior that manifests collinear and soft limits.
Most of the simplicity is coming from a reduced number of independent kinematic quantities and novel properties that fix the residual generalized gauge freedom.

Amplitude computations at higher loop orders become intractable to a brute-force approach even with modern computers.
The complexity of amplitude computations tend to grow severely with each additional external parton or order in the loop expansion.
For example the number of graphs in a Feynman computation grows factorially and integration may be intractable for even a simple scalar theory at higher orders.
Some of the new ideas in article~\ref{lbl:paper1} and \ref{lbl:paper3} arose from the need to cope with this growth of complexity.

A Feynman diagram, or a numerator in a BCJ construction, is not a measurable quantity and generically is gauge dependent.
Color-ordered amplitudes are another type of building blocks, built out of a special set of diagrams using modified Feynman rules.
They are gauge-independent objects that naturally appear after a so called \emph{color decomposition}.
The amplitude is split into purely color-dependent factors and kinematics-dependent objects --- exactly these color-ordered amplitudes.
For certain (planar) theories, color-ordered amplitudes may be recursively constructed, removing the dependence on a diagrammatic description entirely.
Their number is still growing factorially but at a slower rate.
For planar Yang-Mills theory, the so called Kleiss-Kuijf (KK) relations~\cite{Kleiss:1988ne} --- and corresponding loop-level generalizations --- together with the color-kinematics duality (BCJ relations) reduce the number of independent color-ordered amplitudes.

Removing redundancies among the color-ordered amplitudes and the color factors leads to a \emph{minimal} color decomposition.
Different types of (minimal) color decompositions, for example for pure Yang-Mills by Del Duca, Dixon, and Maltoni (DDM)~\cite{DelDuca:1999iql,DelDuca:1999rs} or for quantum chromodynamics (QCD) by Johansson and Ochirov~\cite{Johansson:2014zca}, are well understood at tree level.
At loop level much less is known and even the definition of primitive amplitudes for non-planar contributions is far from being understood.
Publication~\ref{lbl:paper2} discusses a color decomposition at one-loop level for a general QCD amplitude for an arbitrary multiplicity of external partons.

The thesis is organized into four main parts.
In part~\ref{part:background}, we start by discussing general background material and review the core subjects.
It contains an introduction to computational tools like the \emph{spinor helicity} formalism and \emph{generalized unitarity}.
We discuss the (supersymmetric) quantum field theories of interest and the color-kinematics duality together with the double copy and BCJ relations on a formal level.

The second part builds up the main discussion of the thesis.
It contains a review of the general methods and explicit computations for amplitudes in (supersymmetric) gauge and gravity theories.
The focus will lie on the construction of additional matter states on both sides of the double copy.
Introducing fundamental matter allows us to reach a larger set of (super)gravity theories via an extension of the usual double copy prescription.
A major part will focus on $\cN=2$ supersymmetric QCD (SQCD), a supersymmetric extension of QCD.
This theory is interesting as it has features resembling the well-studied $\cN=4$ super Yang-Mills (SYM) theory as well as ordinary QCD.
It is a first stepping stone introducing the complications of QCD into the simpler structures of maximally supersymmetric YM.
We summarize the study of the integrated form of the two-loop $\cN=2$ SQCD amplitude and its transcendentality properties presented in article~\ref{lbl:paper4}.
Via the double copy of (S)QCD a plethora of different (super)gravity theories --- with or without the inclusion of matter --- can be reached.
Most interestingly, we discuss general methods for the extraction of UV divergences, followed by an explicit computation thereof for half-maximal supergravity at two loops.

Part~\ref{part:color} focuses on color decompositions for QCD or supersymmetric extensions thereof.
We review a decomposition at tree level and the results from article~\ref{lbl:paper2}, attached to this thesis, for the one-loop case.

The final part collects several technical methods and ideas that have been used for the various amplitude computations discussed in this thesis.
A first chapter includes algorithms for the reduction of tensor integrals to basic scalar integrals.
In a second chapter we present the use of finite field methods that can significantly speed up many computations arising in the broader field of scattering amplitudes.

We conclude with a short summary of the thesis and an outlook into future developments.
