\chapter{All One- and Two-Loop Four-Point Amplitudes in $\cN=2$ SQCD}\label{ch:ampResults}

In paper~\ref{lbl:paper1} and \ref{lbl:paper3}, the methods from the previous sections have been applied to compute all four-point amplitudes for the MHV sector at one and two loops for $\cN=2$ SQCD in a color-kinematics dual representation.
Also some examples of indegrands in the all-chiral sector have been computed, that we will not discuss here.
This includes three distinct cases for each loop level: Four external vector states, two vector and two matter multiplets, and finally four matter states on external legs.
We will not present explicit expressions here, but rather discuss the exact method and the properties of the color-kinematics dual numerators.

\section{The Method}
All of the amplitudes in question can be obtained by the following ``brute-force'' approach:
\begin{enumerate}
\item
  Identify all trivalent graphs for the given external configuration and as such all numerators required for a color-kinematics dual representation.
  See section~\ref{sec:colorKinDual}.
\item
  Combine the constraints from color-kinematics duality, i.e. the Jacobi-identities and commutation relations, with the following set of additional constraints from section~\ref{sec:constraints}: crossing symmetry, CPT conjugation, two-term identity, supersymmetric decomposition (with exception of the two-loop four-matter amplitude) and matter reversal symmetry.
  Identify a set of master numerators for this functional system of equations according to section~\ref{sec:masters}.
\item Make an Ansatz for each of the master numerators.
  Collect the constraints from above that have not yet been trivially solved by the choice of master numerators as a linear system in the coefficients.
\item Extend the system by constraints from unitarity cuts, see section~\ref{sec:unitarity}.
  A good strategy is to separate the purely four-dimensional part from the extra-dimensional contributions.
  We can use the simple cuts worked out in sec.~\ref{ch:supersums} for the four-dimensional part.
  The missing contributions are obtained after a subtraction of the four-dimensional result from six-dimensional cuts.
  In the attached publications we have used the six-dimensional spinor-helicity formalism to compute these cuts.
\item Solve the linear system, using for example \emph{finite field} methods as will be discussed in chapter~\ref{ch:finiteFields}.
  Any solution is a valid color-kinematics dual representation of the amplitude.
  Optionally, clean up the solution and fix the residual freedom by constructing a \emph{minimal denominator solution}, see sec.~\ref{sec:constraints}.
\end{enumerate}

The workload is significantly reduced by off-shell lifts of cut expressions as discussed in section~\ref{sec:guess}.
All one-loop amplitudes and the two-loop amplitude with four external vector multiplets, have been demonstrated to be completely constructible from an off-shell lift.
For the two-loop amplitude with four external hypermultiplets the situation is more complicated.
One out of two master numerators suggests two different natural off-shell lifts and an Ansatz solely consisting of these two terms has been proposed --- effectively reducing the number of free parameters from several hundreds to two.
The second master appears in more complicated cuts and no simplified Ansatz was constructed.
The brute force algorithm above has then been applied to resolve the residual freedom.

Finally, for the mixed case of two external vectors and two external matter multiplets two of the eight master numerators have been obtained directly from the cuts, whereas an Ansatz construction for the rest was necessary.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Properties}

The representations of the amplitude obtained this way have a variety of properties manifested.
Table~\ref{tbl:solProperties} summarizes the properties of each solution as discussed in sec.~\ref{sec:constraints}.

\begin{table}
  \centering
  \begin{tabular}{l c c c c}
    & Two-term  & Manifest CPT & Matter reversal & $\cN=4$\\
    \midrule
    1-loop vectors & \checkmark & \checkmark & \checkmark & \checkmark\\
    1-loop mixed & \checkmark & \checkmark & \checkmark & \checkmark\\
    1-loop matter & \checkmark & \checkmark & \checkmark & \checkmark\\
    \midrule
    2-loop vectors & \checkmark & \checkmark & \checkmark & \checkmark\\
    2-loop mixed & \checkmark & \checkmark & \checkmark & \checkmark\\
    2-loop matter & \checkmark & \checkmark & $\times^\ast$ & $\times$\\
    \bottomrule
  \end{tabular}
  \vspace{5pt}
  \caption{\small Properties of the various solutions summarized: two-term identities, manifest CPT invariance, matter-reversal symmetry, and the supersymmetric decomposition, i.e. adding up to $\cN=4$. ${}^\ast$Matter-reversal symmetry works for all numerators except for certain matter tadpoles. The symmetry can still be used to reduce the set of masters for all other topologies. (This table is partially extracted from paper~\ref{lbl:paper3}.)}
  \label{tbl:solProperties}
\end{table}

More interestingly, some of the representations transparently expose the infrared (IR) behavior of single diagrams.
The trace structure that naturally appears through our cut construction regulates regions of small or collinear loop momenta.
Let us discuss these features with an explicit example, the two-loop all-vector amplitude.

This solution contains a subset of numerators that are all equal:
\begin{align}
    %\tikzset{external/force remake}
    \MoveEqLeft[2] \gBoxBox[all=gluon,iA=quark,iB=quark,iC=quark,iD=quark,iE=quark,iF=quark,eLA=1,eLB=2,eLC=3,eLD=4,iLA=$\ell_1\!\uparrow$,iLD=$\uparrow\!\ell_2$]{}
    = \gBoxBox[all=gluon,iA=quark,iB=quark,iF=quark,iG=quark,eLA=1,eLB=2,eLC=3,eLD=4,iLA=$\ell_1\!\uparrow$,iLG=$\uparrow\!\ell_2$]{}
    = \gBoxBoxNP[scale=0.85,all=gluon,iA=quark,iB=quark,iC=quark,iD=quark,iE=,eLA=1,eLB=2,eLC=4,eLD=3,iLA=$\ell_1\!\uparrow$,iLD=$\!\!\!\nearrow\!\ell_2$]{}
    = -\frac{1}{2}\gTriTri[all=gluon,iA=quark,iB=quark,iC=quark,eLA=1,eLB=2,eLC=3,eLD=4,iLA=$\ell_1\!\uparrow$,iLF=$\uparrow\!\ell_2$]{}\nn\\
    =&\trm(1\ell_124\ell_23)\hat{\kappa}_{13}+\trp(1\ell_124\ell_23)\hat{\kappa}_{24}\nn\\
    &+\trm(1\ell_123\ell24)\hat{\kappa}_{14}+\trp(1\ell_123\ell_24)\hat{\kappa}_{23}\\
    &-s \mu_{12}(s(\hat{\kappa}_{12}+\hat{\kappa}_{34})+t(\hat{\kappa}_{23}+\hat{\kappa}_{14})+u(\hat{\kappa}_{13}+\hat{\kappa}_{24}))\nn\\
    &+i\,\epsilon(\mu_1,\mu_2)s^2(\hat{\kappa}_{12}-\hat{\kappa}_{34})\nn\,,
\end{align}
where we used the shorthand $\hat{\kappa}_{ij}\equiv\hat{\kappa}_{(ij)(ij)}$.

These numerators have a supressed soft behavior for hyper legs: They manifestly vanish whenever the momentum of an internal matter leg goes to zero.
This can be directly seen from the trace representation.
Since $\tr(\cdots p p \cdots)=0$ for an on-shell momentum $p$, we can move the loop momentum label across an external leg, e.g. for the kinematics of the first double box we have:
\begin{equation}
  \begin{aligned}
    \tr(1\ell_124\ell_23) &= \tr(1(\ell_1+p_1)24\ell_23) = \tr(1(\ell_1-p_2)24\ell_23)\\
    &= \tr(1\ell_124(\ell_2-p_3)3) = \tr(1\ell_124(\ell_2+p_4)3)\,.
  \end{aligned}
\end{equation}
This trace hence vanishes whenever one of the six matter legs carries a zero momentum.
Similar equalities hold for the other diagrams.

In fact, this trace numerator equipped with propagators from the double-box diagram has already been studied in~\cite{CaronHuot:2012ab} in the context of IR-finite integral bases.
A paper studying the IR properties of these amplitudes in more detail is in preparation~\cite{Kalin:2019tba}.

