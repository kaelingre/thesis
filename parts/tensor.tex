\chapter{Tensor Reduction}\label{ch:tensorRed}

Following ideas in~\cite{Anastasiou:thesis,Fiorentin:2015vha}, we present an efficient algorithm for an all-loop all-multiplicity tensor reduction in arbitrary dimension~$D$.

In the second subsection we describe a strategy for an efficient tensor reduction in the case of vacuum diagrams that commonly appear in the computation of UV divergences.

In dimensional regularization it is often advantageous to express numerators of Feynman integrals using the extra dimensional objects~$\mu_{ij}$ defined in sec.~\ref{sec:masters}.
Writing these objects as $\mu_{ij}=\tilde{\eta}_{\mu\nu}\ell_i^\mu\ell_j^\nu$ with the extra-dimensional part of the metric
\begin{equation}
  \tilde{\eta}_{\mu\nu}=\text{diag}(0,\dots,0,-,-,\dots)\,,
\end{equation}
 allows the application of the tensor reduction formulae also for these terms.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General Reduction via Schwinger Parametrization}

The basic idea is to perform a Schwinger parametrization and absorb terms appearing from tensor structures into the Schwinger parametrized form of the integrand.
Ultimately, we obtain a reduction to scalar integrals with shifted numerator powers and dimension.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Gaussian Form of a Schwinger Parametrization}
Let us start with some preparations by inspecting the Schwinger pa\-ram\-e\-trized form of a general Feynman denominator
\begin{equation}\label{eq:schwinger}
  \begin{aligned}
    \frac{1}{D_1^{\nu_1}\cdots D_m^{\nu_m}}&=\prod_{i=1}^m\frac{(-1)^{\nu_i}}{\Gamma(\nu_i)}\int_0^\infty\dd x_i x_i^{\nu_i-1} \exp\left(\sum_{j=1}^m x_j D_j\right) \\
    &\equiv \int\mathcal{D}x \exp\left(\sum_{j=1}^m x_j D_j\right)\,.
  \end{aligned}
\end{equation}
Each denominator factor is of the general form~$D_i=(\sum_j \#_j \ell_j+p_i)^2-m_i^2$, where $\ell_j$ denote loop momenta, $p_i$ are an arbitrary vectors, and $m_i$ are scalar mass terms.
The hash stands for an arbitrary numerical factor.
The exponent in~\eqref{eq:schwinger} accordingly has the structure
\begin{equation}\label{eq:exponent}
  \sum_{j=1}^m x_j D_j = \sum_{i,j=1}^L b_{ij} \ell_i\cdot\ell_j + \sum_{i=1}^L 2 c_i \cdot \ell_i + d\,,
\end{equation}
where $b_{ij}$, $c_i$ and $d$ are scalar/vector functions depending on $x_i$, masses, and momenta.
The number~$L$ counts the total number of loop momenta.
We also set $b_{ij}=b_{ji}$.
Performing a change of variables
\begin{align}\label{eq:changeOfVar}
  \ell_i^\mu = \sum_{j=1}^L x_{ij} k_j^\mu + y_i^\mu & & \text{s.t.} & & \sum_{j=1}^m x_j D_j = \sum_{i=1}^L A_i k_i^2 + D\,,
\end{align}
will bring the integration over the loop momenta into a Gaussian form.

We observe that the unknowns $x_{ij}$ and $y_i$ have a simple form if we assume that $x_{ij}=0~\forall i>j$ and $x_{ii}=1$.
To show this we start by defining some useful objects.
Let $B_{(\beta)}$ be the $(\beta-1)\times(\beta-1)$-matrix with entries $b_{ij}$.
Furthermore we define the vector quantities
\begin{align}
  b_{(\beta)} \equiv \begin{pmatrix}b_{1\beta} \\ \vdots \\ b_{\beta-1,\beta} \end{pmatrix}, & & x_{(\beta)} \equiv \begin{pmatrix}x_{1\beta} \\ \vdots \\ x_{\beta-1,\beta} \end{pmatrix}, & & y = \begin{pmatrix} y_1 \\ \vdots \\y_L \end{pmatrix}, & & c = \begin{pmatrix}c_1 \\ \vdots \\ c_L \end{pmatrix}\,.
\end{align}

By a direct computation it can be checked that
\begin{align}
  x_{(\beta)} &\equiv - B_{(\beta)}^{-1}\cdot b_{(\beta)} & y &\equiv - B_{(L+1)}^{-1}\cdot c
\end{align}
fulfills~\eqref{eq:changeOfVar}.
This leads to an explicit expression for the exponent in Gaussian form in terms of the parameters $b_{ij}$, $c_i$ and $d$
\begin{equation}
  \begin{aligned}
    A_i &= \frac{\det B_{(i+1)}}{\det B_{(i)}}\,,\\
    D &= d - c^\intercal \cdot B_{(L+1)}^{-1} \cdot c = \frac{d \det B_{(L+1)} - c^\intercal\cdot \adj B_{(L+1)}\cdot c}{\det B_{(L+1)}}\,,
  \end{aligned}
\end{equation}
where we have explicitly exposed the denominator by expressing the inverse matrix through its adjugate matrix $B^{-1}_{(L+1)}=\adj B_{(L+1)}/\det B_{(L+1)}$.
In summary, the argument of the exponent is given by
\begin{align}\label{eq:transSum}
  \sum_{j=1}^m x_j D_j &= \sum_{i=1}^L \frac{P_{i+1}}{P_i}k_i^2 + \frac{Q_{L+1}}{P_{L+1}},
\end{align}
where we used the shortcuts
\begin{equation}
  \begin{aligned}
    P_i &\equiv \det B_{(i)}\,,\\
    Q_{L+1}&\equiv d P_{L+1} - c^\intercal\cdot \adj B_{(L+1)}\cdot c\,.
  \end{aligned}
\end{equation}
Note that for the boundary case, $P_1 = \det B_{(1)} = 1$.

The expression for $x_{ij}$ can be rewritten such that one finds the substitution in a form
\begin{align}\label{eq:transMom}
    l_i^\mu &= k_i^\mu + \sum_{j=i+1}^L \frac{(-1)^{i+j} P_{j+1}^{ij}}{P_j}k_j^\mu-\frac{a_{(L+1)}^{[i]}\cdot c^\mu}{P_{L+1}},
\end{align}
where $a_{(L+1)}\equiv \adj(B_{(L+1)})$ and the superscript $[i]$ denotes the $i$'th row.
Furthermore, $A^{ij}$ denotes the matrix $A$ with row $i$ and column $j$ removed.
We are now prepared to perform the tensor reduction.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Reduction Formula}

Our goal is to express the general $L$-loop tensor Feynman integral
\begin{align}
  I^D\left[\{\ell_i^{\mu_{i,s}}\},\{\nu_i\}\right] \equiv
  \int \underbrace{\prod_{i=1}^L \frac{\dd^D \ell_i}{i\pi^{D/2}}}_{\equiv\mathcal{D}\ell}
  \prod_{s=1}^{r_i} \ell_i^{\mu_{i,s}} \frac{1}{D_1^{\nu_1}\cdots D_m^{\nu_m}}\,,
\end{align}
as a sum over tensor structures built out of $\eta^{\mu\nu}$ and scalar integrals.
Using eq.~\eqref{eq:schwinger} to Schwinger parametrize the denominator factors, the above integral takes the form
\begin{align}\label{eq:afterSchwinger}
  I^D\left[\{\ell_i^{\mu_{i,s}}\},\{\nu_i\}\right] = \int \mathcal{D}x\mathcal{D}\ell
  \prod_{s=1}^{r_i} \ell_i^{\mu_{i,s}}
  \exp\left(\sum_{i=1}^m x_i D_i\right)\,.
\end{align}
We replace factors of $\ell_i^{\mu_{i,s}}$ in eq.~\eqref{eq:afterSchwinger} with a derivative with respect to $c_i^{\mu_{i,s}}$ sitting inside the exponent~\eqref{eq:exponent}
\begin{align}
  I^D\left[\{\ell_i^{\mu_{i,s}}\},\{\nu_i\}\right] = \int \mathcal{D}x\mathcal{D}\ell
  \prod_{s=1}^{r_i} \frac{1}{2}\frac{\partial}{\partial c_i^{\mu_{i,s}}}
  \exp\left(\sum_{i=1}^m x_i D_i\right).
\end{align}
Pulling the derivatives out of the inner integral and performing the substitution~\eqref{eq:transMom} leads to
\begin{align}
  I^D\left[\{\ell_i^{\mu_{i,s}}\},\{\nu_i\}\right] &= \int \!\mathcal{D}x
  \prod_{i=1}^{L}\prod_{s=1}^{r_i} \frac{1}{2}\frac{\partial}{\partial c_i^{\mu_{i,s}}}
  \int\!\mathcal{D}k
  \exp\left(\sum_{i=1}^L \frac{P_{i+1}}{P_i} k_i^2 + \frac{Q_{L+1}}{P_{L+1}}\right).
\end{align}
The integral over the loop momenta~$k$ is in a Gaussian form.
It is conveniently rewritten as
\begin{equation}\label{eq:tensorRedFinal}
  \begin{aligned}
    I^D\left[\{\ell_i^{\mu_{i,s}}\},\{\nu_i\}\right] &= \int \mathcal{D}x
    \prod_{i=1}^{L} \prod_{s=1}^{r_i}
    \frac{1}{2}\frac{\partial}{\partial c_i^{\mu_{i,s}}}
    \prod_{i=1}^L \left(\frac{P_i}{P_{i+1}}\right)^{D/2}
    \exp\left(\frac{Q_{L+1}}{P_{L+1}}\right)\\
    &= \int\mathcal{D}x\frac{1}{P_{L+1}^{D/2}}
    \prod_{i=1}^{L}\prod_{s=1}^{r_i}\frac{1}{2}\frac{\partial}{\partial c_i^{\mu_{i,s}}}
    \exp\left(\frac{Q_{L+1}}{P_{L+1}}\right)\,.
  \end{aligned}
\end{equation}
The derivatives acting on the exponential will increase the powers of $P_{L+1}$ which can be absorbed into the denominator factor by increasing the dimension.
Since $P_{L+1}=\det B_{(L+1)}$ is independent of $c_i$ and
\begin{align}
  \frac{\partial}{\partial c_i^{\mu_{i,s}}} Q_{L+1} &= -2 (\adj B_{L+1}\cdot c^{\mu_{i,s}})_i,\\
  \frac{\partial^2}{\partial c_i^{\mu_{i,s}}\partial c_j^{\mu_{j,t}}} Q_{L+1} &= -2 (\adj B_{L+1})_{ij} \eta^{\mu_{i,s}\mu_{j,t}},
\end{align}
we see that the numerator of the integrand will be a polynomial in $x_i$.
These can be absorbed into $\mathcal{D}x$ by shifting the power of the corresponding propagator.
This relates the given tensor integral to a sum of scalar integrals with a tensor structure that is given by the derivatives acting on $Q_{L+1}$.

Formula~\eqref{eq:tensorRedFinal} is well suited for an automatized computer implementation since the calculation of determinants is already efficiently implemented in many libraries and derivatives can be performed fast.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{A Reduction for Vacuum Integrals}\label{sec:tensRedVac}

The tensor reductions for vacuum integrals can be done in a more efficient way, since any object of the form $\ell_i\cdot\ell_j$ can be absorbed into a (inverse) propagator.
Consider an $L$-loop vacuum integral of the form
\begin{equation}\label{eq:vacInt}
  \int \underbrace{\prod_{i=1}^L \frac{\dd^D \ell_i}{i\pi^{D/2}}}_{\equiv\mathcal{D}\ell} \frac{\ell_{i_1}^{\mu_1}\cdots\ell_{i_n}^{\mu_n}}{D}\,,
\end{equation}
where the denominator is a product of inverse vacuum Feynman propagators~$(\sum_j \#_j\ell_j)^2-m^2$.
The hash stands for an arbitrary numerical factor.

To identify all possible tensor structures, we define $\mathbb{P}_n$ as the set of all inequivalent ways of combining objects of a permutation into pairs
\begin{align}
  \mathbb{P}_n = \{(\sigma_1\sigma_2)\cdots(\sigma_{n-1}\sigma_n)|\sigma\in S_n\}/\text{equivalent configurations}\,,
\end{align}
where two configurations are equivalent upon swapping the two members of a pair and a permutation of its set of pairs.
For $\sigma\in\mathbb{P}_n$ we set
\begin{equation}
  \begin{aligned}
    \eta^\sigma &\equiv \eta^{\mu_{\sigma_1}\mu_{\sigma_2}}\cdots\eta^{\mu_{\sigma_{n-1}}\mu_{\sigma_n}}\,,\\
    \ell_\sigma &\equiv \ell_{i_{\sigma_1}}\cdot\ell_{i_{\sigma_2}}\cdots\ell_{i_{\sigma_{n-1}}}\cdot\ell_{i_{\sigma_n}}\,,
  \end{aligned}
\end{equation}
for a given set of spacetime indices~$\{\mu_j\}_{j\in J}$ and loop momentum labels~$\{i_k\}_{k\in K}$.
The tensor structure of the general vacuum integral~\eqref{eq:vacInt} is expressible in terms of tensor structures in $\mathbb{P}_n$ multiplied by Lorentz products of loop momenta $\ell_i\cdot\ell_j$.
We make the Ansatz
\begin{equation}
  \int\mathcal{D}\ell \frac{\ell_{i_1}^{\mu_1}\cdots\ell_{i_n}^{\mu_n}}{D} = \sum_{\sigma,\rho\in\mathbb{P}_n}{c_\sigma}^\rho\int\mathcal{D}\ell\frac{\ell_\rho \eta^\sigma}{D},
\end{equation}
where ${c_\sigma}^\rho$ are coefficients that we will determine in what follows.
In order to solve this equation for ${c_\sigma}^\rho$, we drop the integral and assume --- by a slight abuse of notation --- an implicit summation over the indices~$\sigma,\rho\in\mathbb{P}_n$.
Contracting both sides with an arbitrary $\eta_\pi$, $\pi\in\mathbb{P}_n$ leads to
\begin{equation}
  \ell_\pi = (\eta_\pi\eta^\sigma) {c_\sigma}^\rho\ell_\rho\,.
\end{equation}
Implicit spacetime indices in $(\eta_\pi\eta^\sigma)$ can be contracted and lead, using ${\eta_\mu}^\mu=D$, to an interpretation of $(\eta_\pi\eta^\sigma)$ as a matrix with scalar entries.
The rows and columns of this matrix are labeled by $\pi$ and $\sigma$.
Interpreting this statement as a matrix equation and choosing $\pi=\rho$, we can solve for
\begin{equation}
  {c_\sigma}^\rho = (\eta_\sigma\eta^\rho)^{-1}.
\end{equation}
To conclude, a tensor reduction for vacuum integrals can be done by replacing the tensor structure via
\begin{equation}\label{eq:finalVac}
  \ell_{i_1}^{\mu_1}\cdots\ell_{i_n}^{\mu_n} \rightarrow \eta^\sigma{c_\sigma}^\rho\ell_\rho\quad\text{(implicit summation)},
\end{equation}
and absorbing factors containing loop momenta, i.e. $\ell_\rho$, into the propagators.

For massive propagators one needs to introduce appropriate factors of the masses in the numerator to perform this absorption.
In general, this procedure leads to negative power propagators.
Integrals containing propagators with vanishing or negative power can be factorize into lower loop vacuum integrals.
This factorization procedure for the two-loop sunset diagram with different masses is for example described in~\cite{Davydychev:1992mt}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Implementation strategies}
Contracting the free spacetime indices after a tensor reduction back into a full amplitude expression is computationally costly.
Using formula~\eqref{eq:finalVac}, the contraction can be done locally with each element of $\eta^\sigma$ separately.
The resulting vector is then simply dotted into the vector ${c_\sigma}^\rho\ell_\rho$, which can be hard-coded.

It turns out that this procedure, in general, does not give us the shortest possible expressions, and the object ${c_\sigma}^\rho$ is a large matrix that takes a lot of time to compute and uses a lot of space in memory.
A useful representation of the matrix is found by an eigenvalue decomposition.
Since the matrix is symmetric, there exists a decomposition
\begin{equation}
  c = P D P^\intercal,
\end{equation}
where $D$ is a diagonal matrix containing the eigenvalues and $P$ is a matrix of (orthogonalized) eigenvectors.
By inspecting the cases for $n=2,4,6,8,10$, it turns out that the eigenvalues tend to have a high multiplicity and the eigenvectors are numerical, i.e. they do not depend on the spacetime dimension~$D$.
Let $E_i$ be the $i$'th eigenvalue and $\{p_{i;j}\}_j$ a basis of corresponding orthonormal eigenvectors.
We can then write
\begin{equation}
  c = \sum_i E_i \sum_j p_{i;j} p_{i;j}^\intercal.
\end{equation}
Using slightly schematic notation, this leads to a partially factorized form of the answer
\begin{equation}
  \ell_{i_1}^{\mu_1}\cdots\ell_{i_n}^{\mu_n} \rightarrow \sum_i E_i \sum_{j} (\eta\cdot p_{i;j})(\ell\cdot p_{i;j}),
\end{equation}
where $\eta$ can be thought of as already having been contracted with external vectors.
Not only are the resulting expressions relatively short, but also the matrix~$c$ represented as a list of eigenvalues and eigenvectors takes significantly less space compared to a brute force implementation of formula~\eqref{eq:finalVac}.
