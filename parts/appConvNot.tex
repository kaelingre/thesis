%\chapter{Conventions, Notations and Useful Identities}\label{app:conv}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \section{Spinor Helicity}

%% The metric is $\eta^{\mu\nu} = \text{diag}(+,-,-,-,\cdots)$. The notation for Pauli matrices is
%% \begin{align}
%%   (\sigma^\mu)_{a\dot{a}} = (\mathds{1},\sigma^i)_{a\dot{a}}, & & (\bar{\sigma}^\mu)^{\dot{a}a} = (\mathds{1},-\sigma^{i})^{\dot{a}a}\,,
%% \end{align}
%% where the three ordinary $2\times2$ Pauli matrics are
%% \begin{align}
%%   \sigma^1 = \begin{pmatrix} 0 & 1\\1 & 0\end{pmatrix}
%%     & & \sigma^2 = \begin{pmatrix} 0 & -i \\ i & 0\end{pmatrix}
%%           & & \sigma^3 = \begin{pmatrix} 1 & 0 \\ 0 & -1\end{pmatrix}\,.
%% \end{align}

%% A collection of spinor identities:
%% \begin{equation}
%%   \begin{aligned}
%%     \braket{p q} &= - \braket{q p}\,, & \sqbraket{p q} &= - \sqbraket{qp}\,,\\
%%     \braket{p q}\sqbraket{q p} &= 2 p\cdot q = (p+q)^2\,,\\
%%     \sqket{p}\sqbraket{qr} &= -\sqket{r}\sqbraket{pq} -\sqket{q}\sqbraket{rp}\,, & \ket{p}\braket{qr} &= -\ket{r}\braket{pq} -\ket{q}\braket{rp}\,.
%%   \end{aligned}
%% \end{equation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Dirac Traces}\label{sec:traces}

A classical amplitude computation via Feynman diagrams naturally leads to expressions with traces of Dirac matrices contracted to internal or external momenta.
The cut construction discussed in chapter~\ref{ch:supersums} also leads to similar trace structures via combinations of four-dimensional spinor-helicity objects.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Four dimensions}

We adopt the Weyl basis of gamma matrices for the Clifford algebra
\begin{align}
   \{\gamma^\mu,\gamma^\nu\} &= 2\eta^{\mu\nu}\,, &
    \gamma^\mu &= \begin{pmatrix}0 & \sigma^\mu \\ \bar{\sigma}^\mu & 0
                 \end{pmatrix}\,, &
    \gamma_5 &= i \gamma^0\gamma^1\gamma^2\gamma^3
     = \begin{pmatrix}-\mathds{1}  & 0\\0 & \mathds{1} \end{pmatrix}\,,
\end{align}
where $\sigma^\mu$ are the Pauli matrices. The Dirac traces fulfill the following identities:
\begin{equation}
   \tr(\gamma^\mu) = 0\,, \qquad \quad
   \tr(\gamma^\mu\gamma^\nu) = 4 \eta^{\mu\nu}\,, \qquad \quad
   \tr(\gamma^\mu\gamma^\nu\gamma_5) = 0\,.\\
\end{equation}
Using the relation
\begin{equation}
  \gamma^\mu\gamma^\nu\gamma^\rho = \eta^{\mu\nu}\gamma^\rho
   - \eta^{\mu\rho}\gamma^\nu + \eta^{\nu\rho}\gamma^\mu
   - i\epsilon^{\sigma\mu\nu\rho}\gamma_{\sigma}\gamma_5\,,
\end{equation}
we can recursively reduce traces of gamma matrices with a larger number of arguments. To get from traces of $\gamma$ matrices to traces of Pauli matrices we use
\begin{equation}
  \begin{aligned}
    \tr(\gamma^{\mu_1}\gamma^{\mu_2}\cdots\gamma^{\mu_{2n}}) &= \tr(\bar{\sigma}^{\mu_1}\sigma^{\mu_2}\cdots\sigma^{\mu_{2n}}) + \tr(\sigma^{\mu_1}\bar{\sigma}^{\mu_2}\cdots\bar{\sigma}^{\mu_{2n}})\,,\\ 
    \tr(\gamma_5\gamma^{\mu_1}\gamma^{\mu_2}\cdots\gamma^{\mu_{2n}}) &=  \tr(\bar{\sigma}^{\mu_1}\sigma^{\mu_2}\cdots\sigma^{\mu_{2n}}) - \tr(\sigma^{\mu_1}\bar{\sigma}^{\mu_2}\cdots\bar{\sigma}^{\mu_{2n}})\,,
  \end{aligned}
\end{equation}
which is justified by direct computation using the Weyl basis. This implies
\begin{equation}
  \begin{aligned}
    \tr(\bar{\sigma}^{\mu_1}\sigma^{\mu_2}\cdots\sigma^{\mu_{2n}}) &= \frac{1}{2}\left(\tr(\gamma^{\mu_1}\gamma^{\mu_2}\cdots\gamma^{\mu_{2n}}) + \tr(\gamma_5\gamma^{\mu_1}\gamma^{\mu_2}\cdots\gamma^{\mu_{2n}})\right)\,,\\
    \tr(\sigma^{\mu_1}\bar{\sigma}^{\mu_2}\cdots\bar{\sigma}^{\mu_{2n}}) &= \frac{1}{2}\left(\tr(\gamma^{\mu_1}\gamma^{\mu_2}\cdots\gamma^{\mu_{2n}}) - \tr(\gamma_5\gamma^{\mu_1}\gamma^{\mu_2}\cdots\gamma^{\mu_{2n}})\right)\,,\\
  \end{aligned}
\end{equation}
and the expressions on the right-hand side coincide with the usual definition of $\trp$ and $\trm$ respectively. This directly leads to
\begin{equation}
  \begin{aligned}
    [i_1i_2]\braket{i_2i_3}\cdots[i_{k-1}i_k]\braket{i_ki_1} &= (p_{i_1})_{\mu_1}(p_{i_2})_{\mu_2}\cdots (p_{i_k})_{\mu_k} \tr(\bar{\sigma}^{\mu_1}\sigma^{\mu_2}\cdots\bar{\sigma}^{\mu_k})\\
    &\equiv \trp(i_1i_2\cdots i_k)\,,\\
    \braket{i_1i_2}[i_2i_3]\cdots\braket{i_{k-1}i_k}[i_ki_1] &= (p_{i_1})_{\mu_1}(p_{i_2})_{\mu_2}\cdots (p_{i_k})_{\mu_k} \tr(\sigma^{\mu_1}\bar{\sigma}^{\mu_2}\cdots\sigma^{\mu_k})\\
    &\equiv \trm(i_1i_2\cdots i_k)\,,
  \end{aligned}
\end{equation}
which we use throughout this thesis to assemble four-dimensional traces.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% \section{General Integer Dimensions}
%% Clifford algebra properties in $D$ even dimensions are summarized by
%% \begin{equation}
%%   \{\Gamma^\mu,\Gamma^\nu\}=2\eta^{\mu\nu}, \qquad \Gamma_{\chi} \equiv i^{D/2-1}\Gamma^0\Gamma^1\cdots \Gamma^{D-1}
%% \end{equation}
%% Reduce traces using
%% \begin{equation}
%%   \begin{aligned}
%%     \prod_{i=1}^{D-1} \Gamma^{\mu_i} = \sum_{i<j} [
%%       &(-1)^{\text{sign}(i,j)}\eta^{\mu_i\mu_j} \Gamma^{\mu_1}\cdots\widehat{\Gamma^{\mu_i}}\cdots\widehat{\Gamma^{\mu_j}}\cdots\Gamma^{\mu_{D-1}} \\
%%       &+(-i)^{D/2-1}{\epsilon^{\mu_1\cdots\mu_{D-1}}}_{\nu}\Gamma^{\nu}\Gamma_{\chi}]
%%     \end{aligned}
%% \end{equation}
%% \todo{extend or remove}
