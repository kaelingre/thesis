\chapter{Supersums and Cut Combinatorics}\label{ch:supersums}

One difficulty for the computation of unitarity cuts in supersymmetric theories lies in performing the sum over all on-shell states propagating on cut lines.
This sum is referred to as the \emph{supersum}.
With the on-shell superspace formalism discussed in chapter~\ref{ch:susy}, this sum becomes an integral over the Grassmann variables~$\eta^A$ in four dimensions or their six dimensional counterparts respectively.
Inspired by ideas in~\cite{Bern:2009xq,Bern:2010tq,Arkani-Hamed:2014bca} and the so-called \emph{rung rule}~\cite{Bern:1997nh,Bern:1998ug}, we found manifestly local expressions for iterated two-particle cuts for $\cN=2$ SQCD, published in paper~\ref{lbl:paper3}.
Local formulae for unitarity cuts are not only efficient for organizing the cuts but can, for some cases, be lifted off-shell --- directly giving us expressions for integrands.

Combined with the color-kinematics duality, one can simply try to read off expressions for a small set of master numerators from the cuts.
If the resulting amplitude is consistent on all cuts and if the Jacobi identities are fulfilled, one has obtained a color-kinematics dual representation.
This thesis goes a step further and presents the general form of iterated two-particle MHV cuts (at four points) for any amount of supersymmetry in four dimensions.

For the following discussion, it will be essential to present tree-level building blocks in a favorable form for supersums.
An important development in paper~\ref{lbl:paper3} was the observation that unitarity cuts in four-dimensional $\cN=0,1,2,4$ SYM can be performed in a locality sensitive way.
We keep track of the physical poles and consistently remove spurious poles from cut expressions.

Thus, we will first discuss the pole structure of the tree-level amplitudes and the importance of the given representations in sec.~\ref{sec:tree}.
In sec.~\ref{sec:generalSupersum} and \ref{sec:graphicalRules} the general result is presented.
Finally we discuss some simplifications for $\cN=2$ and examples thereof in sec.~\ref{sec:neq2cuts}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Notation and Tree-Level Amplitudes}\label{sec:tree}

Consider the following two-particle cut
\begin{equation}\label{eq:supersum}
  %\tikzset{external/force remake}
  \cBoxA[all=line,eLA=1,eLB=2,eLC=3,eLD=4,iLA=$\uset{\rightarrow}{5}$,iLB=$\oset{\rightarrow}{6}$]{} \propto \int \dd^{\cN}\eta_5 \dd^{\cN}\eta_6 A_{\text{L}}(1,2,5,6) A_{\text{R}}(3,4,-6,-5)\,,
\end{equation}
where $A_{\text{L/R}}$ are color-ordered four-point MHV tree-level superamplitudes.
The explicit form of this amplitude for $\cN=4$ is given by a supersymmetric generalization of the Parke-Taylor formula~\eqref{eq:PT}.
For less supersymmetry, we use the projection from $\cN=4$ supermultiplets onto the wanted states discussed in section~\ref{sec:4DSYM}.

We introduce a combinatorial notation for tree-level amplitudes to treat all possible configurations of supermultiplets on external and internal legs.
The object that keeps track of the Grassmann variables, i.e. that represents the state configuration is
\begin{equation}
  \kappa_{(i_1j_1)\cdots(i_{\tilde{\cN}}j_{\tilde{\cN}})} \equiv \frac{\sqbraket{12}\sqbraket{34}}{\braket{12}\braket{34}}\delta^{2\cN}(Q)\eta_{i_1}^{\cN+1}\braket{i_1j_1}\eta_{j_1}^{\cN+1}\cdots\eta_{i_{\tilde{\cN}}}^4\braket{i_{\tilde{\cN}}j_{\tilde{\cN}}}\eta_{j_{\tilde{\cN}}}^4\,,
\end{equation}
where $\tilde{\cN}=4-\cN$.
The rational prefactor of spinor helicity products captures the correct overall helicity weight --- at four points there is only one independent quantity that has the correct weight.
The $\delta^{2\cN}(Q)$ is the supermomentum conserving delta function introduced in eq.~\eqref{eq:superQ}.
The remaining objects are combinations of Grassmann~$\eta$ and spinor angle brackets accounting for different state configurations and their helicity weight.
For future convenience, we introduce a set of collected indices~$\ubar{i} \equiv (i_1,\dots,i_{\tilde{\cN}})$, such that the above object is written as~$\kappa_{(\ubar{i}\,\ubar{j})}$.

The correspondence between $\kappa$ and a configuration of external legs is found by comparison with the supermultiplets in sec.~\ref{sec:4DSYM}.
For example, the color-ordered SYM amplitudes are
\begin{equation}
  \begin{aligned}
    A_{\cN=4}(\cV_1\cV_2\cV_3\cV_4) &= -\frac{i}{st} \kappa\,,\\
    A_{\cN=2}(V_1^-V_2^+V_3^-V_4^+) &= -\frac{i}{st} \kappa_{(13)(13)}\,,\\
    A_{\cN=2}(V_1^-V_2\Phi_3\overline{\Phi}_4) &= -\frac{i}{st}  \kappa_{(13)(14)}\,,\\
    A_{\cN=1}(\Phi_1\overline{\Phi}_2\Phi_3\overline{\Phi}_{4}) &= -\frac{i}{st} \kappa_{(13)(24)(24)}\,,\\
    A_{\cN=0}(A_1^-A_2^+\Psi_3^+\Psi_4^-) &= -\frac{i}{st} \kappa_{(13)(14)(14)(14)}\,,
  \end{aligned}
\end{equation}
where $V^\pm$ stands for the negative- and positive-helicity part of the vector multiplet
\begin{equation}
  \cV = V^+ + V^-\,,
\end{equation}
e.g.
\begin{equation}
  V_{\cN=2}^+ = A^+ + \eta^A\psi_A^+ + \eta_1\eta_2\phi_{12}\,,
\end{equation}
for $\cN=2$ SYM.

For higher point MHV and \MHVb{} tree level amplitudes, there is a generalization of these objects --- using chiral superspace variables~$\eta$ for MHV and antichiral~$\bar{\eta}$ for \MHVb --- ignoring the helicity factor.
At multiplicity four, MHV and \MHVb{} are equivalent.
As such there is an equivalent representation of the four-point amplitude using the antichiral superspace.
To relate these equivalent formulations, we introduce a \emph{complement operation} acting on two legs of a four-point amplitude or diagram.
Let $\{1,2,3,4\}$ be the set of legs and $\{i,j\}$ a choice of two different elements thereof.
We define $\{\overline{i,j}\}\equiv\{1,2,3,4\}\setminus\{i,j\}$, where the overline always goes over two elements.
The object carrying the state configuration in the antichiral superspace is
\begin{equation}
  \bar{\kappa}_{(\ubar{i}\,\ubar{j})} = \int \dd^{4\times\cdot4}\eta e^{\eta\cdot\bar{\eta}} \kappa_{(\overline{\ubar{i}\,\ubar{j}})}
  =\kappa_{(\ubar{i}\,\ubar{j})}\bigg|_{\sqbraket{ij}\leftrightarrow\braket{ij},\eta\leftrightarrow\bar{\eta},Q\leftrightarrow\bar{Q}}\,.
\end{equation}
We define some shorthand notations using collective indices to express the final result in a compact form:
\begin{equation}
  \begin{aligned}
    \sqbraket{\ubar{i}\ubar{j}} &\equiv \sqbraket{i_1j_1}\cdots\sqbraket{i_{\tilde{\cN}}j_{\tilde{\cN}}}\,,\\
    \braket{\ubar{i}\ubar{j}} &\equiv \braket{i_1j_1}\cdots\braket{i_{\tilde{\cN}}j_{\tilde{\cN}}}\,,\\
    s_{\ubar{i}\,\ubar{j}} &\equiv s_{i_1j_1} \cdots s_{i_{\tilde{\cN}}j_{\tilde{\cN}}}\,.
  \end{aligned}
\end{equation}
This completes the set of kinematic objects appearing in the final formula for the two-particle cut.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General Two-Particle Four-Point Supersum}\label{sec:generalSupersum}

For the supersum~\eqref{eq:supersum}, the full $\eta$ dependence is contained in $\kappa_{(\ubar{i}\ubar{j})}$ and $\kappa_{(\ubar{k}\ubar{l})}$.
Thus, we only consider a supersum over these objects.
All other factors are contributing to the cut via simple multiplication. 
The general result for this supersum is
\begin{equation}\label{eq:supersumGeneral}
  \begin{aligned}
    I_{(\ubar{i}\ubar{j})(\ubar{k}\ubar{l})}&\equiv\int\dd^{\cN}\eta_5\dd^{\cN}\eta_6 \kappa_{(\ubar{i}\ubar{j})}(1,2,5,6)\kappa_{(\ubar{k}\ubar{l})}(3,4,-6,-5)\\
    &= (-1)^{\text{sign}(\ubar{i},\ubar{j},\ubar{k},\ubar{l})} \frac{\sqbraket{56}^{\tilde{\cN}}\braket{\ubar{i}\ubar{j}}\braket{\ubar{k}\ubar{l}}\sqbraket{\ubar{q}\ubar{r}}}{s_{56}^{2-\cN}}\hat{\kappa}_{(\ubar{q}\ubar{r})}\,,
  \end{aligned}
\end{equation}
where the collective indices $\ubar{q}$ and $\ubar{r}$ are determined by $\{q_m,r_m,5,6\}=\{i_m,j_m,k_m,l_m\}$ for $m=1,\dots,\tilde{\cN}$, i.e. they encode the external state configuration of the overall cut.
The sign of an ordered four-tuple is determined by the signature of its permutation $(i_m,j_m,k_m,l_m)$ with respect to $(q_m,r_m,5,6)$.
The overall sign is then given by the sum $\text{sign}(\ubar{i},\ubar{j},\ubar{k},\ubar{l})=\sum_m\text{sign}(i_m,j_m,k_m,l_m)$.
And finally we absorb some poles by defining
\begin{equation}
  \hat{\kappa}_{\ubar{i}\ubar{j}}\equiv\frac{\kappa_{\ubar{i}\ubar{j}}}{s_{\ubar{i}\ubar{j}}^{\tilde{\cN}}}\,.
\end{equation}
With this notation, a local integrand representation is simply built out of $\hat{\kappa}$, local polynomials in the numerator, and physical poles in the denominator.

One can equivalently write the formula using the antichiral superspace
\begin{equation}\label{eq:supersumGeneralBar}
  \bar{I}_{(\overline{\ubar{i}\ubar{j}})(\overline{\ubar{k}\ubar{l}})} = \int\dd^{4\times4}\eta e^{\eta\cdot\bar{\eta}} I_{(\ubar{i}\ubar{j})(\ubar{k}\ubar{l})} = (-1)^{\text{sign}(\overline{\ubar{i},\ubar{j}},\overline{\ubar{k}\ubar{l}})}\frac{\braket{56}^{\tilde{\cN}}\sqbraket{\overline{\ubar{i}\ubar{j}}}\sqbraket{\overline{\ubar{k}\ubar{l}}}\braket{\overline{\ubar{q}\ubar{r}}}}{s_{56}^{2-\cN}}\hat{\bar{\kappa}}_{\overline{\ubar{q}\ubar{r}}}\,.
\end{equation}
This form has the advantage that it can be iterated.
We can glue another four-point blob into our cut and reuse~\eqref{eq:supersumGeneral} or~\eqref{eq:supersumGeneralBar} to perform the supersum over the two newly glued legs.
Furthermore, since the helicity prefactor and the supermomentum-conserving delta function are permutation invariant, the same formula also holds for a non-planar cut, e.g.
\begin{equation}
  %\tikzset{external/force remake}
  \cNPBox[all=line,eLA=1,eLB=2,eLC=3,eLD=4,iLA=$\uset{\rightarrow}{5}$,iLB=$\oset{\rightarrow}{6}$]{}\,.
\end{equation}
This non-planar cut then only differs from its planar cousin by the poles and other prefactors of $\kappa$ that we dropped in this computation. 

The formulae~\eqref{eq:supersumGeneral} and~\eqref{eq:supersumGeneralBar} have further useful properties apart from their recursive structure.
The spinor-helicity factors in the numerator can naturally be combined into helicity Dirac traces.
Ultimately, the resulting expressions are manifestly built out of Lorentz-invariant objects.
Our conventions for the definition of these traces are given in appendix~\ref{sec:traces}.

The cut formula introduces two types of (unphysical) poles.
For $\cN<2$ there is a factor of $s_{56}^{2-\cN}$ in the denominator.
This factor can always be removed by spinor-helicity identities on the cut kinematics as we will momentarily show.
The second factor is the denominator that we absorbed into $\hat{\kappa}$.
These factors are canceled in an iterated application of the supersum formula as we will discuss in the next sections.
Evidently, only an overall factor of $1/s_{\ubar{q}\ubar{r}}^{\tilde{\cN}}$, where $(\ubar{q}\ubar{r})$ denotes the overall external state configuration, will be present in any iterated two-particle cut.
This factor is an artifact of our use of the spinor helicity formalism.
More concretely, these poles are coming from denominators introduced by polarization vectors expressed in the spinor helicity formalism as defined in eq.~\eqref{eq:polVect4D}.
Hence, we obtain a formula for the cuts that is free of unwanted unphysical poles.
As the Grassmann Fourier transform that relates $I$ and $\bar{I}$ only acts on $\kappa$ and $\bar{\kappa}$ respectively --- these two objects are exactly related by a Fourier transform --- we conclude that the coefficients of these two objects in equations~\eqref{eq:supersumGeneral} and \eqref{eq:supersumGeneralBar} are the same.
Using that $\braket{56}\sqbraket{65}=s_{56}$, the cut is equivalently expressed in a more symmetric form as
\begin{equation}\label{eq:supersumMaster}
  I_{(\ubar{i}\ubar{j})(\ubar{k}\ubar{l})} = \left(s_{56}^{\cN}\braket{\ubar{i}\ubar{j}}\sqbraket{\overline{\ubar{i}\ubar{j}}}\braket{\ubar{k}\ubar{l}}\sqbraket{\overline{\ubar{k}\ubar{l}}}\sqbraket{\ubar{q}\ubar{r}}\braket{\overline{\ubar{q}\ubar{r}}}\right)^{\frac{1}{2}}\hat{\kappa}_{(\ubar{q}\ubar{r})}\,,
\end{equation}
trading the unwanted pole for a square root.

For $\cN=4$ and $\cN=2$, the square root can be easily removed as will be discussed in the following sections.
For less supersymmetry, the square root does not pose a problem, but some more work is required to get rid of it.
It requires the use of kinematic spinor-helicity identities to write its argument as a square.
We also note that there is an ambiguity of the overall sign for the cut.
In practice this sign can be inferred through other consistency criteria.

The supersum formula~\eqref{eq:supersumGeneral} for $\cN=4$ has already been obtained in~\cite{Bern:1996je}; for $\cN=2$, proof was given in paper~\ref{lbl:paper3}.
It is shown by a direct computation of the supersum and applications of spinor helicity identities on the cut kinematics.
The general proof is a minor generalization thereof and gives no further insight into the structure of the answer and we skip it here.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Iteration and Graphical Rules}\label{sec:graphicalRules}

Formula~\eqref{eq:supersumMaster} is well suited for iteration, i.e. we can iteratively glue four-point blobs into the cut and construct higher loop cuts in the \emph{Mondrian} (box-like) family~\cite{Bern:2005iz}.
Consider a two-loop cut constructed by gluing another four-point tree into the above cut.
The two newly glued legs are numbered 7 and 8.
The supersum takes the form
\begin{equation}
  \begin{aligned}
    I_{(\ubar{i}\ubar{j})(\ubar{k}\ubar{l})(\ubar{m}\ubar{n})}
      &\equiv\int\dd^{\cN}\eta_5\dd^{\cN}\eta_6\dd^{\cN}\eta_7\dd^{\cN}\eta_8\kappa_{(\ubar{i}\ubar{j})}\kappa_{(\ubar{k}\ubar{l})}\kappa_{(\ubar{m}\ubar{n})}\\
    &=\int\dd^{\cN}\eta_7\dd^{\cN}\eta_8
      \left(s_{56}^\cN\braket{\ubar{i}\ubar{j}}\sqbraket{\overline{\ubar{i}\ubar{j}}}\braket{\ubar{k}\ubar{l}}\sqbraket{\overline{\ubar{k}\ubar{l}}}\sqbraket{\ubar{q}\ubar{r}}\braket{\overline{\ubar{q}\ubar{r}}}\right)^{\frac{1}{2}}\hat{\kappa}_{(\ubar{q}\ubar{r})}\kappa_{(\ubar{m}\ubar{n})}\,,
  \end{aligned}
\end{equation}
where we plugged in the above result for the integration over $\eta_5$ and $\eta_6$ to arrive at the second line.
Using formula~\eqref{eq:supersumGeneral} a second time for the integration over $\eta_7$ and $\eta_8$ leads to
\begin{equation}
  \begin{aligned}
    \MoveEqLeft[0.3] I_{(\ubar{i}\ubar{j})(\ubar{k}\ubar{l})(\ubar{m}\ubar{n})}\\
    &= \frac{\left(
      s_{56}^\cN s_{78}^\cN\braket{\ubar{i}\ubar{j}}\sqbraket{\overline{\ubar{i}\ubar{j}}}\braket{\ubar{k}\ubar{l}}\sqbraket{\overline{\ubar{k}\ubar{l}}}\sqbraket{\ubar{q}\ubar{r}}\braket{\overline{\ubar{q}\ubar{r}}}\braket{\ubar{m}\ubar{n}}\sqbraket{\overline{\ubar{m}\ubar{n}}}\braket{\ubar{q}\ubar{r}}\sqbraket{\overline{\ubar{q}\ubar{r}}}\sqbraket{\ubar{s}\ubar{t}}\braket{\overline{\ubar{s}\ubar{t}}}\right)^{\frac{1}{2}}
    }{
      s_{(\ubar{q}\ubar{r})}
    }\hat{\kappa}_{(\ubar{s}\ubar{t})}\\
    &=\left(
    s_{56}^\cN s_{78}^\cN\braket{\ubar{i}\ubar{j}}\sqbraket{\overline{\ubar{i}\ubar{j}}}\braket{\ubar{k}\ubar{l}}\sqbraket{\overline{\ubar{k}\ubar{l}}}\braket{\ubar{m}\ubar{n}}\sqbraket{\overline{\ubar{m}\ubar{n}}}\sqbraket{\ubar{s}\ubar{t}}\braket{\overline{\ubar{s}\ubar{t}}}
   \right)^{\frac{1}{2}}\hat{\kappa}_{(\ubar{s}\ubar{t})}\,,
  \end{aligned}
\end{equation}
where we have used that $\braket{\ubar{q}\ubar{r}}\sqbraket{\ubar{r}\ubar{q}}=\braket{\overline{\ubar{q}\ubar{r}}}\sqbraket{\overline{\ubar{r}\ubar{q}}}=s_{(\ubar{q}\ubar{r})}$.
The overall state configuration is given by $(\ubar{s}\ubar{t})$.
As promised, the intermediate unphysical pole $1/s_{(\ubar{q}\ubar{r})}$ is canceled out.
Apart from the pole sitting inside $\hat{\kappa}$ --- an artifact from the spinor-helicity formalism as noted before --- there are no further unphysical poles present.

Furthermore, we observe a factorization into several building blocks coming from different parts of the cut.
It is possible to assemble a cut of this form directly from graphical rules.
In the following, we provide graphical rules to obtain the full analytic expression for any two-particle iterated cut with any combination of vector- and mattermultiplet on external and internal lines.
Consider, for example, a cut of the form
\begin{equation}
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize]
    \fill[blob] (0,0) ellipse (0.2 and 0.3);
    \fill[blob] (0.7,0) ellipse (0.2 and 0.3);
    \fill[blob] (0.35,-0.7) ellipse (0.55 and 0.2);
    \fill[blob] (1.4,-0.3) ellipse (0.2 and 0.6);
    \fill[blob] (2.1,-0.3) ellipse (0.2 and 0.6);
    \draw[line] ($(0,0)+(135:0.24)$) -- ($(0,0)+(135:0.7)$) node[left] {$2$};
    \draw[line] ($(0,-0.6)+(-135:0.23)$) -- ($(0,-0.6)+(-135:0.7)$) node[left] {$1$};
    \draw[line] ($(2.2,0)+(45:0.1)$) -- ($(2.2,0)+(45:0.7)$) node[right] {$3$};
    \draw[line] ($(2.2,-0.6)+(-45:0.1)$) -- ($(2.2,-0.6)+(-45:0.7)$) node[right] {$4$};
    \draw[line] ($(0,0)+(45:0.24)$) -- ($(0.7,0)+(135:0.24)$);
    \draw[line] ($(0,0)+(-45:0.24)$) -- ($(0.7,0)+(-135:0.24)$);
    \draw[line] ($(0,0)+(-90:0.3)$) -- ($(0.35,-0.7)+(150:0.32)$);
    \draw[line] ($(0.7,0)+(-90:0.3)$) -- ($(0.35,-0.7)+(30:0.32)$);
    \draw[line] ($(2,-0.6)+(-135:0.1)$) -- ($(1.5,-0.6)+(-45:0.1)$);
    \draw[line] ($(1.3,-0.6)+(-135:0.1)$) -- ($(0.35,-0.7)+(0:0.55)$);
    \draw[line] ($(2,0)+(135:0.1)$) -- ($(1.5,0)+(45:0.1)$);
    \draw[line] ($(1.3,0)+(135:0.1)$) -- ($(0.7,0)+(0:0.2)$);
  \end{tikzpicture}\,,
\end{equation}
where each line can represent any (half-)supermultiplet in a gauge theory described in section~\ref{sec:4DSYM}.
The basic building blocks are the tree-level amplitudes described above.
We distinguish between the positive- and negative-helicity part of the vector multiplet $V^+$ and $V^-$ and between $\Phi$ and $\overline{\Phi}$ for matter multiplets.
This graph can, with the rules described in this section, directly be translated into a closed mathematical expression for the cut.

From the form of the tree level four-point amplitude, one can immediately obtain a rule for the overall pole factors.
Each tree-level blob contributes with a factor
\begin{equation}\label{eq:rulePoles}
  %\tikzset{external/force remake}
  \cTree[eLA=$a$,eLB=$b$,eLC=$c$,eLD=$d$]{}\rightarrow -\frac{i}{s_{ab}s_{bc}}\,,
\end{equation}
independently from the state configuration --- $a$, $b$, $c$ and $d$ denote particle labels.

From eq.~\eqref{eq:supersumMaster}, a blob with a tree level contribution proportional to $\kappa_{(\ubar{i}\bar{j})}$ comes with a factor of $(\braket{\ubar{i}\ubar{j}}\sqbraket{\overline{\ubar{i}\ubar{j}}})^{\frac{1}{2}}$, graphically
\begin{equation}\label{eq:ruleBlob}
  \cTree[]{
    \draw[fill=white,draw=white,opacity=0.7] (0,0) circle (0.22);
    \node[inner sep=0pt] at (0,-0.02) {$(\ubar{i}\ubar{j})$};
  }\rightarrow (\braket{\ubar{i}\ubar{j}}\sqbraket{\overline{\ubar{i}\ubar{j}}})^{\frac{1}{2}}\,.
\end{equation}
The ordering of the legs is irrelevant for this rule as it only depends on the state configuration.

The factors~$s_{ij}^\cN$ under the square root can be seen as a rule for the simultaneous gluing of two legs
\begin{align}\label{eq:ruleGlue}
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \fill[pattern=north west lines] (-0.5,0) -- (-0.5,-0.25) arc (-90:90:0.25) -- cycle;
    \draw (-0.5,-0.25) arc (-90:90:0.25);
    \fill[pattern=north west lines] (0.5,0) -- (0.5,0.25) arc (90:270:0.25) -- cycle;
    \draw (0.5,0.25) arc (90:270:0.25);
    \draw ($(-0.5,0) + (30:0.25)$) -- node[above] {$\uset{\rightarrow}{l_1}$} ($(0.5,0) + (150:0.25)$);
    \draw ($(-0.5,0) + (-30:0.25)$) -- node[below] {$\oset{\rightarrow}{l_2}$} ($(0.5,0) + (-150:0.25)$);
  \end{tikzpicture}
  &\rightarrow \left(s_{l_1l_2}^\cN\right)^{\frac{1}{2}}\,.
\end{align}

Finally, there is a rule associated to the external configuration $(\ubar{s}\ubar{t})$ of the overall cut
\begin{equation}\label{eq:ruleExt}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \draw[dotted] (0,0) circle (0.25);
    \draw[line] (45:0.25) -- (45:0.6);
    \draw[line] (-45:0.25) -- (-45:0.6);
    \draw[line] (-135:0.25) -- (-135:0.6);
    \draw[line] (135:0.25) -- (135:0.6);
    \node at (90:0.4) {$(\ubar{s}\ubar{t})$};
  \end{tikzpicture}\rightarrow (\sqbraket{\ubar{s}\ubar{t}}\braket{\overline{\ubar{s}\ubar{t}}})^{\frac{1}{2}}\hat{\kappa}_{(\ubar{s}\ubar{t})}\,.
\end{equation}
Also this rule is agnostic of the ordering of the legs and solely depends on the state configuration.
The four rules~\eqref{eq:rulePoles}-\eqref{eq:ruleExt} completely determine a cut.
For $\cN=4$ and $\cN=2$, we discuss a prescription to rewrite these rules free of square root terms.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Cuts for $\cN=4$ SYM and $\cN=2$ SQCD}\label{sec:neq2cuts}

The details for the maximal and half-maximal case have been worked out in detail in paper~\ref{lbl:paper3}.
We review the results here.

For $\cN=4$ SYM the rule~\eqref{eq:ruleBlob} is obsolete as $\braket{\ubar{i}\ubar{j}}=\sqbraket{\overline{\ubar{i}\ubar{j}}}=1$. The remaining three rules furthermore simplify to
\begin{equation}
  \begin{aligned}
    \cTree[eLA=$a$,eLB=$b$,eLC=$c$,eLD=$d$]{}&\rightarrow -\frac{i}{s_{ab}s_{bc}}\,,\\
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \fill[pattern=north west lines] (-0.5,0) -- (-0.5,-0.25) arc (-90:90:0.25) -- cycle;
      \draw (-0.5,-0.25) arc (-90:90:0.25);
      \fill[pattern=north west lines] (0.5,0) -- (0.5,0.25) arc (90:270:0.25) -- cycle;
      \draw (0.5,0.25) arc (90:270:0.25);
      \draw ($(-0.5,0) + (30:0.25)$) -- node[above] {$\uset{\rightarrow}{l_1}$} ($(0.5,0) + (150:0.25)$);
      \draw ($(-0.5,0) + (-30:0.25)$) -- node[below] {$\oset{\rightarrow}{l_2}$} ($(0.5,0) + (-150:0.25)$);
    \end{tikzpicture}
    &\rightarrow s_{l_1l_2}^2\,,\\
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \draw[dotted] (0,0) circle (0.25);
      \draw[line] (45:0.25) -- (45:0.6);
      \draw[line] (-45:0.25) -- (-45:0.6);
      \draw[line] (-135:0.25) -- (-135:0.6);
      \draw[line] (135:0.25) -- (135:0.6);
    \end{tikzpicture}&\rightarrow\kappa\,,
  \end{aligned}
\end{equation}
where we dropped any state configuration indices as there is only a single vector multiplet.
This reproduced the rung rule prescription~\cite{Bern:1997nh,Bern:1998ug}.

For $\cN=2$, we specify the blob rule~\eqref{eq:ruleBlob} and the external rule~\eqref{eq:ruleExt} for the three different non-zero combinations of vector and hyper multiplets on external legs.
Graphically, we represent the vector multiplet (for both chiralities) as a coiled line and the hypermultiplet $\Phi$ and $\overline{\Phi}$ as arrows going in different directions.
Consider as an example the cut from above with an explicit choice of multiplets assigned to each propagator line
\begin{equation}
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize]
    \fill[blob] (0,0) ellipse (0.2 and 0.3);
    \fill[blob] (0.7,0) ellipse (0.2 and 0.3);
    \fill[blob] (0.35,-0.7) ellipse (0.55 and 0.2);
    \fill[blob] (1.4,-0.3) ellipse (0.2 and 0.6);
    \fill[blob] (2.1,-0.3) ellipse (0.2 and 0.6);
    \draw[gluon] ($(0,0)+(135:0.24)$) -- ($(0,0)+(135:0.7)$) node[left] {$2^-$};
    \draw[gluon] ($(0,-0.6)+(-135:0.23)$) -- ($(0,-0.6)+(-135:0.7)$) node[left] {$1^+$};
    \draw[quark] ($(2.2,0)+(45:0.1)$) -- ($(2.2,0)+(45:0.7)$) node[right] {$3$};
    \draw[aquark] ($(2.2,-0.6)+(-45:0.1)$) -- ($(2.2,-0.6)+(-45:0.7)$) node[right] {$4$};
    \draw[agluon] ($(0,0)+(45:0.24)$) -- ($(0.7,0)+(135:0.24)$);
    \draw[gluon] ($(0,0)+(-45:0.24)$) -- ($(0.7,0)+(-135:0.24)$);
    \draw[gluon] ($(0,0)+(-90:0.3)$) -- ($(0.35,-0.7)+(150:0.32)$);
    \draw[aquark] ($(0.7,0)+(-90:0.3)$) -- ($(0.35,-0.7)+(30:0.32)$);
    \draw[quark] ($(2,-0.6)+(-135:0.1)$) -- ($(1.5,-0.6)+(-45:0.1)$);
    \draw[quark] ($(1.3,-0.6)+(-135:0.1)$) -- ($(0.35,-0.7)+(0:0.55)$);
    \draw[aquark] ($(2,0)+(135:0.1)$) -- ($(1.5,0)+(45:0.1)$);
    \draw[aquark] ($(1.3,0)+(135:0.1)$) -- ($(0.7,0)+(0:0.2)$);
  \end{tikzpicture}\,,
\end{equation}
with two helicity vector states~$V_1^+$ and $V_2^-$ and two matter constituents~$\Phi_3$ and $\overline{\Phi}_4$.
This cut construction also requires us to specify the helicities for internal vector lines.
It is then necessary to sum over all possible configurations of helicity assignments for internal vector lines to assemble the full cut.

The rule for the pole factors~\eqref{eq:rulePoles} is independent of the state configuration and remains unchanged for $\cN=2$.
The internal rule for tree-level numerator factors can be simplified for three non-vanishing cases to
\begin{subequations}
  \begin{align}
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \fill[blob] (0,0) circle (0.2);
    \draw[gluon] (45:0.2) -- (45:0.5) node[right] {$c^+$};
    \draw[gluon] (-45:0.2) -- (-45:0.5) node[right] {$d^+$};
    \draw[gluon] (-135:0.2) -- (-135:0.5) node[left] {$a^-$};
    \draw[gluon] (135:0.2) -- (135:0.5) node[left] {$b^-$};
  \end{tikzpicture}
  &\rightarrow \braket{ab}[cd]\,,\\
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \fill[blob] (0,0) circle (0.2);
    \draw[quark] (45:0.2) -- (45:0.5) node[right] {$c$};
    \draw[aquark] (-45:0.2) -- (-45:0.5) node[right] {$d$};
    \draw[gluon] (-135:0.2) -- (-135:0.5) node[left] {$a^-$};
    \draw[gluon] (135:0.2) -- (135:0.5) node[left] {$b^+$};
  \end{tikzpicture}
  &\rightarrow \langle a|c|b]\,,\\ %= -\langle a|p_d+p_a|b]\,,\\
  %\tikzset{external/force remake}
  \label{eq:neq2IntRule3}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \fill[blob] (0,0) circle (0.2);
    \draw[quark] (45:0.2) -- (45:0.5) node[right] {$c$};
    \draw[aquark] (-45:0.2) -- (-45:0.5) node[right] {$d$};
    \draw[quark] (-135:0.2) -- (-135:0.5) node[left] {$a$};
    \draw[aquark] (135:0.2) -- (135:0.5) node[left] {$b$};
  \end{tikzpicture}~\,
  &\rightarrow s_{ac} = s_{bd}\,.
\end{align}
\end{subequations}
Note that these rules are still agnostic to the ordering of the external legs.

The gluing rule~\eqref{eq:ruleGlue} is only simplified insofar the square root and the square cancel (ignoring any overall sign issues)
\begin{align}
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \fill[pattern=north west lines] (-0.5,0) -- (-0.5,-0.25) arc (-90:90:0.25) -- cycle;
    \draw (-0.5,-0.25) arc (-90:90:0.25);
    \fill[pattern=north west lines] (0.5,0) -- (0.5,0.25) arc (90:270:0.25) -- cycle;
    \draw (0.5,0.25) arc (90:270:0.25);
    \draw ($(-0.5,0) + (30:0.25)$) -- node[above] {$\uset{\rightarrow}{l_1}$} ($(0.5,0) + (150:0.25)$);
    \draw ($(-0.5,0) + (-30:0.25)$) -- node[below] {$\oset{\rightarrow}{l_2}$} ($(0.5,0) + (-150:0.25)$);
  \end{tikzpicture}
  &\rightarrow s_{l_1l_2}\,.
\end{align}
Last but not least, the external rule~\eqref{eq:ruleExt} is specified for the three non-trivial cases in a similar manner as the above rule for internal blobs
\begin{subequations}
\begin{align}
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \draw[dotted] (0,0) circle (0.2);
    \draw[gluon] (45:0.2) -- (45:0.5) node[right] {$s^+$};
    \draw[gluon] (-45:0.2) -- (-45:0.5) node[right] {$t^+$};
    \draw[gluon] (-135:0.2) -- (-135:0.5) node[left] {$q^-$};
    \draw[gluon] (135:0.2) -- (135:0.5) node[left] {$r^-$};
  \end{tikzpicture}
  &\rightarrow [qr]\braket{st}\hat{\kappa}_{(qr)(qr)}\,,\\
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \draw[dotted] (0,0) circle (0.2);
    \draw[aquark] (45:0.2) -- (45:0.5) node[right] {$s$};
    \draw[quark] (-45:0.2) -- (-45:0.5) node[right] {$t$};
    \draw[gluon] (-135:0.2) -- (-135:0.5) node[left] {$q^-$};
    \draw[gluon] (135:0.2) -- (135:0.5) node[left] {$r^+$};
  \end{tikzpicture}
  &\rightarrow  [q|s|r\rangle\hat{\kappa}_{(qs)(qt)}\,,\\
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
    baseline={([yshift=-0.5ex]current bounding box.center)},
    font=\scriptsize]
    \draw[dotted] (0,0) circle (0.2);
    \draw[quark] (45:0.2) -- (45:0.5) node[right] {$s$};
    \draw[aquark] (-45:0.2) -- (-45:0.5) node[right] {$t$};
    \draw[quark] (-135:0.2) -- (-135:0.5) node[left] {$q$};
    \draw[aquark] (135:0.2) -- (135:0.5) node[left] {$r$};
  \end{tikzpicture}~\,
  &\rightarrow s_{rt}\hat{\kappa}_{(qs)(rt)} = s_{qs}\hat{\kappa}_{(qs)(rt)}\,,
\end{align}
\end{subequations}
where we have explicitly written out the full subscript of $\hat{\kappa}$.
As before these formulae are independent of the ordering of the legs.
