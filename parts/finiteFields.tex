\chapter{Finite Fields}\label{ch:finiteFields}

One of the bottlenecks of the computations discussed in part~\ref{part:amps} is the solving of the constraint equations for the coefficients in the Ansatz.
First of all, the full system tends to be quite large with a lot of redundant equations.
For example, the Ansatz for $\cN=1$ SQCD at two loops contains around 10000 free parameters for solely the four-dimensional part.
The various constraints discussed in sec.~\ref{sec:constraints} lead to a linear system with over 200000 equations.

A further complication comes from the property of this system that tends to produce intermediate expressions with enormous rational numbers --- making the memory requirements and CPU time explode.
In contrast, the final expressions tend to contain only very simple numerical coefficients.
Both of these purely computational complications can be overcome by the use of finite fields.

The basic idea is to solve, or more precisely row reduce, the numerical linear system over different finite fields (most conveniently chosen as integers modulo some prime number) and reconstruct the rational result afterwards.
This prevents intermediate expressions to blow up.
Since the final expressions often have a tame behavior, not many runs over different finite fields are required.

Finite fields have been successfully applied to many other problems in mathematics, physics and computational science, for example polynomial factorization, the computation of the greatest common divisor (GCD), or integration by parts (IBP) techniques~\cite{vonManteuffel:2014ixa}.

We follow Peraro~\cite{Peraro:2016wsq} to review the most important formulae and algorithms.
All the algorithms referred to here can be found there.

Most conveniently, we will use finite fields of integers modulo a prime~$p$ denoted by $\mathbb{Z}_p$.
The operation of taking inverses is defined via
\begin{equation}
  (a a^{-1}) \mod p = 1\,.
\end{equation}
This uniquely defines the element $a^{-1}\in\mathbb{Z}_p$ for all non-zero $a\in\mathbb{Z}_p$.
The inverse can be efficiently computed via the extended Euclidean algorithm.
Together with the definition of addition, subtraction and multiplication modulo $p$, this defines all required rational operations.
We can map any rational number $q=a/b$ into the finite field via
\begin{equation}
  q \mod p = (a b^{-1}) \mod p\,.
\end{equation}

Once the system is translated into the finite field, one can use special purpose libraries for the row reduction.
An implementation in \Cpp is for example the SpaSM (Sparse Solver Module $p$) library~\cite{Bouillaguet:2016abc}.

After having reduced the system, we can apply rational reconstruction methods to obtain the row reduced system over the full set of rational numbers.
A variation of the extended Euclidean algorithm allows one to make a guess for a number $a$ mapped back into the rational numbers $q\in\mathbb{Q}$.
The guess is, in general, correct if the numerator and denominator of $q$ are much smaller than the prime $p$.
Since integer numbers on computers are restricted in size for efficient implementations, this simple procedure will fail for systems with representationally large rational coefficients.

The Chinese remainder theorem saves the day.
It allows a unique reconstruction of a number $a\in\mathbb{Z}_n$ from its images in $\mathbb{Z}_{p_i}$, where $n= p_1 \cdots p_k$ and all the $p_i$ are pairwise co-prime.
Thus, we can solve the system several times over different finite fields $\mathbb{Z}_{p_i}$ to increase the probability of correctly reconstructing the rational number.
In practice, one can add more and more primes $p_i$ and check if the result is correct by resubstituting it back into the original system.
Heuristically, a faster method is to check if for a reconstructed $q=a/b$ the numerator and denominator are below the threshold $\sqrt{n}$, i.e. $a<\sqrt{n}$ and $b<\sqrt{n}$.
