\chapter{Tree Level Review}\label{ch:colorRes}

A tree-level DDM-like color-decomposition for QCD~\cite{Johansson:2015oia} is completely understood for any multiplicity and has been proven in~\cite{Melia:2015ika}.
Here we will mostly rely on diagrammatic explanations and intuitive notations, and refer to the original papers for more mathematically rigorous statements.

As discussed in section~\ref{sec:bcjRels}, the DDM construction realizes a decomposition of pure Yang-Mill amplitudes into a minimal basis of color factors.
With the inclusion of quarks, the tree level color decomposition for any number and type of external legs has been worked out in~\cite{Johansson:2015oia}.
Let us introduce some notation before we present the final formula.

We consider the case for mutually different quark flavors for each quark-antiquark pair and internal closed quark loop.
The equal-flavor case is obtained by summing over all pairings of quarks and antiquarks of the same flavor~\cite{Melia:2013epa}.
A valid configuration of external legs for a non-vanishing primitive tree-level amplitude in QCD is represented by a \emph{Dyck word}~\cite{Duchon:2000,Kasa:2010}.

Consider a disk where the external legs of a diagram are cyclically fixed on the boundary.
The color-ordered amplitude associated with this setup is computed by all graphs that can be drawn on this disk without any crossing of legs --- using color-ordered Feynman rules~\cite{Dixon:1996wi} to obtain a mathematical expression.
Since the quark lines connect a quark with the corresponding antiquark of same flavor, it cannot intersect another quark line.
Mathematically, this means that the external leg configuration has the same structure as a ``valid'' combination of opening and closing parentheses, i.e. a Dyck word.
For example, '()()(())' is such a valid configuration.
Each pair of opening and closing bracket is identified with a quark-antiquark pair.
Gluons do not have any such restriction and can be inserted at any point.

Using the notation that quarks are labeled by underlined numbers $\ul{i}$ and the corresponding antiquark with the same number overlined $\ol{i}$, we can assign explicit particle labels to a Dyck word.
Diagrammatically,
\begin{equation}\label{eq:treePrimitive}
  ()()(())\rightarrow A^{(0)}(\ul{1},\ol{1},\ol{2},3,\ul{2},\ol{4},\ol{5},6,\ul{5},\ul{4}) \, = \,
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.5]
    \draw[dashed] (0:2) arc (0:360:2);
    \draw[quark] (-36:2.5) node[right] {$\ol{1}$} .. controls (-36:1) and (0:1) .. (0:2.5) node[right] {$\ul{1}$};
    \draw[quark] (180:2.5) node[left] {$\ol{4}$} .. controls (180:1) and (36:1) .. (36:2.5) node[right] {$\ul{4}$};
    \draw[quark] (144:2.5) node[left] {$\ol{5}$} .. controls (144:1) and (72:1) .. (72:2.5) node[above] {$\ul{5}$};
    \draw[gluon] (108:2.5) node[above] {$6$} -- (108:1.5);
    \draw[quark] (288:2.5) node[below] {$\ol{2}$} .. controls (288:1) and (216:1) .. (216:2.5) node[left] {$\ul{2}$};
    \draw[gluon] (252:2.5) node[below] {$3$} -- (252:1.5);
  \end{tikzpicture},
\end{equation}
where we have randomly chosen the direction of each quark line and inserted gluons with label 3 and 6 at certain positions.
We collect all possible particle assignments of all Dyck words of length $k$ for a total number of $n$ particles in the set $\text{Dyck}_{n,k}$.
We additionally demand that a quark comes before the corresponding antiquark with the same flavor, i.e. opening parentheses are quarks and closing parentheses are antiquarks.
Accordingly, $k$ denotes the number of quark-antiquark pairs and $n-2k$ gluons have to be additionally inserted to obtain the correct state counting.

With this setup, we can write down the color decomposition for general $n$ and $k$
\begin{equation}\label{eq:colorDecompTree}
  \cA_{n,k}^{(0)} = g^{n-2}\smashoperator{\sum_{\sigma\in\text{Dyck}_{n-2,k-1}}} C^{(0)}(\ul{1},\sigma,\ol{1})A^{(0)}(\ul{1},\sigma,\ol{1})\,.
\end{equation}
The color factors~$C^{(0)}$ are most conveniently defined diagrammatically, using the definition of colorful objects in eq.~\eqref{eq:colorDef}.
A color factor is obtained by fixing the quark line of legs $\ul{1}$ and $\ol{1}$ at the base of a diagram.
All other legs are cyclically added and the following `Mario world' structure is built:
\begin{equation}
  C^{(0)}(\ul{1},\ul{2},\ul{3},4,\ol{3},\ol{2},\ul{5},6,\ol{5},\ol{1})=
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.8]
    \draw[quark] (1,0) -- (0,0) node[left] {$\ul{1}\strut$};
    \draw[quark] (2,0) -- (1,0);
    \draw[quark] (3,0) -- (2,0);
    \draw[quark] (5,0) -- (3,0);
    \draw[quark] (6,0) -- (5,0);
    \draw[quark] (7,0) node[right] {$\ol{1}\strut$} -- (6,0);
    
    \draw[quark] (1,0.7) -- (0.3,0.7) node[left] {$\ul{2}\strut$};
    \draw[quark] (2,0.7) -- (1,0.7);
    \draw[quark] (3,0.7) -- (2,0.7);
    \draw[quark] (3.7,0.7) node[right] {$\ol{2}\strut$} -- (3,0.7);
    \draw[quark] (5,0.7) -- (4.3,0.7) node[left] {$\ul{5}\strut$};
    \draw[quark] (6,0.7) -- (5,0.7);
    \draw[quark] (6.7,0.7) node[right] {$\ol{5}\strut$} -- (6,0.7);
    \draw[gluon] (1,0) -- (1,0.7);
    \draw[gluon] (2,0) -- (2,0.7);
    \draw[gluon] (3,0) -- (3,0.7);
    \draw[gluon] (5,0) -- (5,0.7);
    \draw[gluon] (6,0) -- (6,0.7);

    \draw[quark] (2,1.4) -- (1.3,1.4) node[left] {$\ul{3}\strut$};
    \draw[quark] (3,1.4) -- (2,1.4);
    \draw[quark] (3.7, 1.4) node[right] {$\ol{3}\strut$} -- (3,1.4);
    \draw[gluon] (2,0.7) -- (2,1.4);
    \draw[gluon] (3,0.7) -- (3,1.4);
    \draw[gluon] (6,0.7) -- (6,1.4) node[above] {6};

    \draw[gluon] (3,1.4) -- (3,2.1) node[above] {4};

    \cC{(1,0)};
    \cC{(2,0)};
    \cC{(3,0)};
    \cC{(5,0)};
    \cC{(6,0)};
    \cC{(2,0.7)};
    \cC{(3,0.7)};
    \cC{(6,0.7)};
    \cC{(3,1.4)};
  \end{tikzpicture}\,.
\end{equation}
The operator that connects gluonic and fermionic lines is defined via
\begin{equation}
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.8]
    \draw[quark] (0.5,0) -- (0,0);
    \draw[quark] (1,0) -- (0.5,0);
    \draw[quark] (1,0.6) -- (0.5,0.6);
    \draw[quark] (0.5,0.6) -- (0,0.6);
    \draw[quark] (1,1.2) -- (0.5,1.2);
    \draw[quark] (0.5,1.2) -- (0,1.2);
    \draw[quark] (1,2) -- (0.5,2);
    \draw[quark] (0.5,2) -- (0,2);
    \draw[decorate, decoration={brace, amplitude=6pt}] (1,2.1) -- node[right=4pt] {$l$} (1,-0.1);
    \draw[gluon] (0.5,0) -- (0.5,0.6);
    \draw[gluon] (0.5,0.6) -- (0.5,1.2);
    \draw[dotted] (0.5,1.3) -- (0.5,1.9);
    \draw[gluon] (0.5,2) -- (0.5,2.6);
    \cC{(0.5,0)};
    \cC{(0.5,0.6)};
    \cC{(0.5,1.2)};
    \cC{(0.5,2)};
  \end{tikzpicture}
  =
  %\tikzset{external/force remake}
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.8]
    \draw[white] (1,-0.1) -- (0,-0.1);%for boundary
    \draw[quark] (0.5,0) -- (0,0);
    \draw[quark] (1,0) -- (0.5,0);
    \draw[quark] (1,0.6) -- (0.5,0.6);
    \draw[quark] (0.5,0.6) -- (0,0.6);
    \draw[quark] (1,1.2) -- (0.5,1.2);
    \draw[quark] (0.5,1.2) -- (0,1.2);
    \draw[quark] (1,2) -- (0.5,2);
    \draw[quark] (0.5,2) -- (0,2);
    \node[fill=white, circle, inner sep=2.5] at (0.5,0.6) {};
    \node[fill=white, circle, inner sep=2.5] at (0.5,1.2) {};
    \node[fill=white, circle, inner sep=2.5] at (0.5,2) {};
    \draw[gluon] (0.5,0) -- (0.5,1.5);
    \draw[dotted] (0.5,1.5) -- (0.5,1.8);
    \draw[gluon] (0.5,1.8) -- (0.5,2.6);
  \end{tikzpicture}
  +
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.8]
    \draw[white] (1,-0.1) -- (0,-0.1);%for boundary
    \draw[quark] (0.5,0) -- (0,0);
    \draw[quark] (1,0) -- (0.5,0);
    \draw[quark] (1,0.6) -- (0.5,0.6);
    \draw[quark] (0.5,0.6) -- (0,0.6);
    \draw[quark] (1,1.2) -- (0.5,1.2);
    \draw[quark] (0.5,1.2) -- (0,1.2);
    \draw[quark] (1,2) -- (0.5,2);
    \draw[quark] (0.5,2) -- (0,2);
    \node[fill=white, circle, inner sep=2.5] at (0.5,1.2) {};
    \node[fill=white, circle, inner sep=2.5] at (0.5,2) {};
    \draw[gluon] (0.5,0.6) -- (0.5,1.5);
    \draw[dotted] (0.5,1.5) -- (0.5,1.8);
    \draw[gluon] (0.5,1.8) -- (0.5,2.6);
  \end{tikzpicture}
  +
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.8]
    \draw[white] (1,-0.1) -- (0,-0.1);%for boundary
    \draw[quark] (0.5,0) -- (0,0);
    \draw[quark] (1,0) -- (0.5,0);
    \draw[quark] (1,0.6) -- (0.5,0.6);
    \draw[quark] (0.5,0.6) -- (0,0.6);
    \draw[quark] (1,1.2) -- (0.5,1.2);
    \draw[quark] (0.5,1.2) -- (0,1.2);
    \draw[quark] (1,2) -- (0.5,2);
    \draw[quark] (0.5,2) -- (0,2);
    \node[fill=white, circle, inner sep=2.5] at (0.5,2) {};
    \draw[gluon] (0.5,1.2) -- (0.5,1.5);
    \draw[dotted] (0.5,1.5) -- (0.5,1.8);
    \draw[gluon] (0.5,1.8) -- (0.5,2.6);
  \end{tikzpicture}
  + \cdots +
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.8]
    \draw[white] (1,-0.1) -- (0,-0.1);%for boundary
    \draw[quark] (0.5,0) -- (0,0);
    \draw[quark] (1,0) -- (0.5,0);
    \draw[quark] (1,0.6) -- (0.5,0.6);
    \draw[quark] (0.5,0.6) -- (0,0.6);
    \draw[quark] (1,1.2) -- (0.5,1.2);
    \draw[quark] (0.5,1.2) -- (0,1.2);
    \draw[quark] (1,2) -- (0.5,2);
    \draw[quark] (0.5,2) -- (0,2);    
    \draw[dotted] (0.5,1.3) -- (0.5,1.9);
    \draw[gluon] (0.5,2) -- (0.5,2.6);
  \end{tikzpicture}\,.
\end{equation}
A mathematically rigorous definition can be found in~\cite{Johansson:2015oia}.
That the primitive amplitudes appearing in this decomposition form a basis of kinematic objects has already been observed earlier~\cite{Melia:2013bta,Melia:2014oza}
\begin{equation}
  \cB_{n,k}^{(0)} = \left\{A^{(0)}(\ul{1},\sigma,\ol{1}) \mid \sigma \in \text{Dyck}_{n-2,k-1}\right\}\,.
\end{equation}
The construction is based on a generalization of the KK relations~\eqref{eq:kk} for fundamental matter, i.e. relations between primitive amplitudes including quarks.
The basis is formally defined as a maximal set of amplitudes independent under these relations.
The size of the basis is $(n-2)!/k!$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{A Decomposition at One-Loop Level}\label{ch:colorResOne}
Paper~\ref{lbl:paper2} presents a conjecture for a one-loop color decomposition for QCD for any multiplicity and any combination of external particles.
The validity of the presented formulae has been explicitly checked for up to ten partons in some cases.

Extending the notation from tree level, we want to consider primitive amplitudes on an annulus, i.e. two boundaries where external legs can live.
The bounderies are separated by either a closed quark or a mixed/purely gluonic loop.
Consider as an example the following primitive amplitude:
\begin{equation}
  %\tikzset{external/force remake}
  A^{q}(\ul{1},\ol{1},\ol{2},3,\ul{2},4,5,6|\ol{7},8,9,\ul{7},10,11) \rightarrow
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=1]
    \draw[dashed] (0,0) circle (0.6);
    \draw[dashed] (0,0) circle (1.5);
    \draw[quark] (0,0) circle (1);
    \draw[quark] (-45:1.7) node[right] {$\ol{1}$} .. controls (-45:1.2) and (0:1.2) .. (0:1.7) node[right] {$\ul{1}$};
    \draw[quark] (-90:1.7) node[below] {$\ol{2}$} .. controls (-90:1.5) and (-180:1.5) .. (-180:1.7) node[left] {$\ul{2}$};
    \draw[gluon] (-135:1.7) node[below] {$3$} -- (-135:1.3);
    \draw[gluon] (135:1.7) node[above] {$4$} -- (135:1.3);
    \draw[gluon] (90:1.7) node[above] {$5$} -- (90:1.3);
    \draw[gluon] (45:1.7) node[above] {$6$} -- (45:1.3);
    \draw[quark] (0:0.5) node[left] {$\ol{7}$} .. controls (0:0.9) .. (30:0.9)
      .. controls (60:1.2) and (120:1.2) .. (150:0.9)
      .. controls (180:0.9) .. (180:0.5) node[right] {$\ul{7}$};
    \draw[gluon] (60:0.5) node[below] {$8$} -- (60:0.8);
    \draw[gluon] (120:0.5) node[below] {$9$} -- (120:0.8);
    \draw[gluon] (-120:0.5) node[above] {$10$} -- (-120:0.8);
    \draw[gluon] (-60:0.5) node[above] {$11$} -- (-60:0.8);
  \end{tikzpicture}\,.
\end{equation}
We have to distinguish different routings of quark lines on this annulus.
The quark line connecting $\ol{2}\rightarrow\ul{2}$ could also go around counter-clockwise instead, or we could reverse the direction of the loop.
In principle, one needs to specify the routing a quark line takes with respect to all other quark lines.
However, often less information is sufficient.
It might be clear which routing a quark line has, since a different configuration would lead to a vanishing amplitude (i.e. it is not possible to draw a single graph with the specified routing).
A notation for routings has been introduced in paper~\ref{lbl:paper2}, but for simplicity we will rely on purely graphical statements here.

For a gluonic loop it is also possible that a quark line stretches between two boundaries.
Effectively, it is possible to find a basis of primitive amplitudes that have no external parton on the inner boundary.
If there is no parton present on the inner boundary, we drop the vertical bar in the argument of the primitive amplitude.
The primitive amplitudes are computed using color-ordered Feynman rules as before.

To extend the color decomposition from tree-level for the case of a closed quark loop, one can imagine to close the lowest quark line $\ul{1}\rightarrow\ol{1}$ to form a loop.
A good guess would then be to consider the same set of Dyck words that we have already seen at tree level and remove all cyclically equivalent configurations.
Denote this set by $\text{Dyck}_{n,k}^\circlearrowleft$.
The proposed basis of primitive amplitudes is then given by
\begin{equation}
  \cB^{q}_{n,k} = \left\{ A^{q}(\sigma) \mid \sigma\in\text{Dyck}_{n,k}^\circlearrowleft\right\}\,,
\end{equation}
where we additionally assume a routing where the quark loop goes counter-clockwise around the hole of the annulus and lies to the left of each external quark line.
Any other convention would work equally well.
Note that there is no leg on the inner boundary of the annulus.

The color decomposition is then simply given as a sum over the basis of kinematic objects combined with the corresponding color factors,
\begin{equation}
  \cA^q_{n,k} = g^n \smashoperator{\sum_{\sigma\in\text{Dyck}^\circlearrowleft_{n,k}}} C^q(\sigma) A^q(\sigma)\,.
\end{equation}
The color factors $C^q(\sigma)$ are defined by fixing the quark loop at the base of a `Mario world' structure and building up the top of it the same way as in the tree-level case, e.g.
\begin{equation}
  \label{eq:ex_color_dec_quark}
  C^q(\ul{1}\ul{2}3\ol{2}\ol{1}\ul{4}5\ol{4}) = \!\!\!\!\!
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.8]
    \draw[quark] (1,0) -- (0,0);
    \draw[quark] (2,0) -- (1,0);
    \draw[quark] (3,0) -- (2,0);
    \draw[quark] (5,0) -- (3,0);
    \draw[quark] (6,0) -- (5,0);
    \draw[quark] (7,0) -- (6,0);
    \draw[quark] (0,0) .. controls (-1,-0.5) and (8,-0.5) .. (7,0);
    
    \draw[quark] (1,0.7) -- (0.3,0.7) node[left] {$\ul{1}\strut$};
    \draw[quark] (2,0.7) -- (1,0.7);
    \draw[quark] (3,0.7) -- (2,0.7);
    \draw[quark] (3.7,0.7) node[right] {$\ol{1}\strut$} -- (3,0.7);
    \draw[quark] (5,0.7) -- (4.3,0.7) node[left] {$\ul{4}\strut$};
    \draw[quark] (6,0.7) -- (5,0.7);
    \draw[quark] (6.7,0.7) node[right] {$\ol{4}\strut$} -- (6,0.7);
    \draw[gluon] (1,0) -- (1,0.7);
    \draw[gluon] (2,0) -- (2,0.7);
    \draw[gluon] (3,0) -- (3,0.7);
    \draw[gluon] (5,0) -- (5,0.7);
    \draw[gluon] (6,0) -- (6,0.7);

    \draw[quark] (2,1.4) -- (1.3,1.4) node[left] {$\ul{2}\strut$};
    \draw[quark] (3,1.4) -- (2,1.4);
    \draw[quark] (3.7, 1.4) node[right] {$\ol{2}\strut$} -- (3,1.4);
    \draw[gluon] (2,0.7) -- (2,1.4);
    \draw[gluon] (3,0.7) -- (3,1.4);
    \draw[gluon] (6,0.7) -- (6,1.4) node[above] {5};

    \draw[gluon] (3,1.4) -- (3,2.1) node[above] {3};

    \cC{(1,0)};
    \cC{(2,0)};
    \cC{(3,0)};
    \cC{(5,0)};
    \cC{(6,0)};
    \cC{(2,0.7)};
    \cC{(3,0.7)};
    \cC{(6,0.7)};
    \cC{(3,1.4)};
  \end{tikzpicture}.
\end{equation}
The size of the basis is $\lvert \text{Dyck}^\circlearrowleft_{n,k}\rvert=(n-1)!/k!$.

In the case of a gluonic or mixed loop, we need to introduce some more notation regarding Dyck words.
So far, we have always assigned quarks before antiquarks, i.e. we identify opening parentheses with quarks and closing parentheses with antiquarks.
We define a modified set of Dyck words with a second type of brackets '[]', where the antiquark is assigned to the opening and the quark to the closing bracket.
For example '()[()][[]]' is a valid modified Dyck word.
The set $\text{mDyck}^\circlearrowleft_{n,k}$ is then given by the set of all modified Dyck words via the same cyclic equivalence as before and demanding that the first bracket in the word is not square.
The color decomposition is written down as
\begin{equation}
  \cA^g_{n,k>0}=g^n\smashoperator{\sum_{\sigma\in\text{mDyck}^\circlearrowleft_{n,k}}}C^g(\sigma)A^g(\sigma)\,.
\end{equation}
Accordingly, the corresponding basis of primitive amplitudes is given by
\begin{equation}
  \cB^g_{n,k>0}=\left\{A^g(\sigma) \mid \sigma\in\text{mDyck}^\circlearrowleft_{n,k}\right\}\,,
\end{equation}
where we demand that the loop lies to the left of a quark line assigned to round brackets and to the right of a quark line assigned to square brackets.
This fixes all other relative routings of quark lines.
The color factors take a slightly more complicated form.
As an example, consider
\begin{equation}\label{eq:ex_color_factor_gluon}
  C^g(\ul{1}\ol{1}\ol{2}\ol{3}4\ul{3}\ul{2}5) =\!\!\!
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize,
      scale=0.8]
    \draw[quark] (2.5,0) -- (2,0);
    \draw[gluon] (2.5,0) -- (4,0);
    \draw[quark] (2,0) -- (1.5, 0.7) node[above] {$\ul{1}\strut$};
    \draw[quark] (3,0.7) node[above] {$\ol{1}\strut$} -- (2.5,0);
    \draw[quark] (3.5,0.7) node[above] {$\ol{2}\strut$} -- (4,0);
    \draw[quark] (4,0) -- (5,0);
    \draw[quark] (5,0) -- (6,0);
    \draw[quark] (6,0) -- (7,0);
    \draw[quark] (7,0) -- (7.5,0.7) node[above] {$\ul{2}\strut$};
    \draw[quark] (4.3,0.7) node[left] {$\ol{3}\strut$} -- (5,0.7);
    \draw[quark] (5,0.7) -- (6,0.7);
    \draw[quark] (6,0.7) -- (6.7,0.7) node[right] {$\ul{3}\strut$};
    \draw[gluon] (5,0) -- (5,0.7);
    \draw[gluon] (6,0) -- (6,0.7);
    \draw[gluon] (6,0.7) -- (6,1.4) node[above] {4};
    \draw[gluon] (8.1,0.1) -- (8.1,0.7) node[above] {$5\strut$};
    \draw[gluon] (7,0) -- (8.6,0) .. controls (9.6,-1) and (0,-1) .. (1,0) -- (2,0);

    \cC{(5,0)};
    \cC{(6,0)};
    \cC{(6,0.7)};
  \end{tikzpicture}\!\!\!.
\end{equation}
The proper mathematical definition can be found in paper~\ref{lbl:paper2}.
The size of the basis is given by $\lvert\text{mDyck}^\circlearrowleft_{n,k}\rvert = 2^{2k-1}(n-1)!k!/(2k)!$.
The special case of a purely gluonic amplitude has already been discussed in~\cite{DelDuca:1999rs}.
It requires to additionally remove primitive amplitudes that are equivalent under a reflection of the arguments.
This reflection redundancy is broken for the general case by requiring the first bracket in the modified Dyck word to be round.
