\chapter{The UV Structure of Half-Maximal Supergravity Amplitudes}\label{ch:uv}

After having obtained a color-kinematics dual form of a gauge theory amplitude, it is a simple task to obtain the gravitational amplitude of the double-copied theory.
Some gravitational theories are obtained from the double copy in a more intricate way, this includes the non-factorizable ones.
We discussed the factorizabilty of a gravity multiplet for some cases in section~\ref{sec:sugra} and a more extensive summary table is given in appendix~\ref{app:rep}.
Consider, for example, the double copy of two pure Yang-Mills states.
The product of two on-shell vector states
\begin{equation}
  A^\mu \otimes A^\mu = h^{\mu\nu} + a + \varphi\,,
\end{equation}
not only produces two graviton states but also an axion~$a$ and dilaton state~$\varphi$.
If one is interested in amplitudes of pure Einstein gravity this direct construction does not work.

In \cite{Johansson:2014zca}, it has been observed that the double copy construction can be supplemented by fundamental matter states that allow the addition or subtraction (ghost statistics) of the corresponding states.

We will first discuss this general construction.
Afterwards, we will specify to a double copy of $(\cN=2 \text{ SQCD})\otimes(\cN=2 \text{ SQCD})$ at two loops and show how the construction can be used to obtained pure supergravity amplitudes for $\cN=4$.
Finally, we discuss how the UV divergence of this amplitude can be extracted and present explicit expressions obtained in paper~\ref{lbl:paper1}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Double Copy with Ghosts}

The double copy of two vector multiplets with supersymmetry $\cN,\cM\leq 2$ leads to the decomposition
\begin{equation}
  \cV_\cN \otimes \cV'_\cM = \cH_{\cN+\cM}\oplus X_{\cN+\cM} \oplus \overline{X}_{\cN+\cM}\,,
\end{equation}
which contains additional matter multiplets.
In order to remove the matter states, one needs to consider the double copy of fundamental matter multiplets
\begin{equation}
  \Phi_\cN \otimes \overline{\Phi}'_\cM = X_{\cN+\cM}\,, \quad \overline{\Phi}_{\cN+\cM}\otimes\Phi'_\cM = \overline{X}_{\cN+\cM}\,.
\end{equation}
Assigning ghosts statistics to the latter double-copied matter states will formally cancel out the matter states in the former double copy.
Effectively the ghost statistics is assigned to one side of the double copy, say $\Phi_\cN$ but not $\Phi'_\cM$.

We could have ignored this problem for external states as the unwanted axion and dilaton states can simply be projected out.
But we want to suppress matter states from propagating in the loop.
This is achieved by modifying the double copy prescription formula~\eqref{eq:doubleCopyM}.
In the case where we want to cancel them out, we can introduce a ghost factor by replacing
\begin{equation}
  c_i \rightarrow (-1)^{\lvert i\rvert}\bar{n}'_i\,,
\end{equation}
where $\lvert i \rvert$ counts the number of closed matter loops in the diagram corresponding to the color factor~$c_i$.
More generally, if we want to add matter, one can introduce an integer quantity~$N_f$ counting the number of matter multiplets in one of the gauge theory factors.
The replacement rule then takes the form
\begin{equation}
  c_i \rightarrow (N_f)^{\lvert i\rvert}\bar{n}'_i\,.
\end{equation}
This amounts to a total number $N_X = 1+N_f$ of matter multiplets on the gravity side.
For consistency, we set $0^0=1$ for the case we choose to not add any additional matter states, i.e. $N_f=0$.

The double copy formula~\eqref{eq:doubleCopyM} takes then the form
\begin{equation}\label{eq:doubleCopyWithMatter}
  \cM_n^{(L)} = i^{L-1}\left(\frac{\kappa}{2}\right)^{n+2L-2} \sum_{i\in\Gamma_n^{(L)}}\int\frac{\dd^{LD}\ell}{(2\pi)^{LD}} \frac{(N_f)^{\lvert i\rvert}}{S_i}\frac{n_i\bar{n}'_i}{D_i}\,,
\end{equation}
where $\bar{n}'_i$ denotes the numerator of the graph with reversed matter arrows with respect to $n_i$.
This construction can be done for any number of supersymmetries and in any spacetime dimension.
Note, however, that it is essential that the double copy of matter exactly produces the unwanted states.
It is currently known how to get pure theories in $D=4$, and in some cases in D=6 as we discuss below.
A complete picture of pure theories in any dimension is not known.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Pure $\cN=4$ Supergravity Amplitudes}

The amplitude construction via $(\cN=2\text{ SQCD})\otimes(\cM=2\text{ SQCD})$ at one and two loops has been discussed in~\cite{Johansson:2014zca} and paper~\ref{lbl:paper1} respectively.
We will focus on the two-loop computation.
The double copy of two vector multiplets leads to unwanted vector states on the supergravity side:
\begin{equation}
  \cV_{\cN=2}\otimes\cV_{\cN=2} = \cH_{\cN=4} \oplus 2 \cV_{\cN=4}\,,
\end{equation}
which can be remove by the double copy of hypermultiplets on both sides
\begin{equation}
  \Phi_{\cN=2}\otimes\overline{\Phi}_{\cN=2} = \overline{\Phi}_{\cN=2}\otimes\Phi_{\cN=2} = 2 \cV_{\cN=4}\,.
\end{equation}

In order to obtain dimensionally regulated amplitudes we have used the six-dimensional uplift
\begin{equation}
  \cV_{\cN=(1,0)}\otimes\cV_{\cN=(0,1)} = \cH_{\cN=(1,1)}\,
\end{equation}
to compute the extra-dimensional part of the answer.
The map between four- and six-dimensional states is one-to-one, see eq.~\eqref{eq:map4D6D}.
For simplicity we stick to four-dimensional notation in the rest of this section.

Instead of directly applying the double copy formula~\eqref{eq:doubleCopyWithMatter}, we group contributions into supergravity numerators if they share the same denominator~$D_i$:
\begin{equation}
  N_{i} = \sum_{D_j = D_i} (N_f)^{\lvert i \rvert} n_j \bar{n}_j\,.
\end{equation}
Diagrammatically, this leads, for example, to the double box supergravity numerator
\begin{equation}
  \begin{aligned}
    %\tikzset{external/force remake}
    N\!\left(\gBoxBox[scale=0.8]{}\right) =
    &\left\lvert n\!\!\left(\gBoxBox[scale=0.8,all=gluon]{}\right)\right\rvert^2
    +2N_f\left(\left\lvert n\!\!\left(\gBoxBox[scale=0.8,all=gluon,iA=quark,iB=quark,iC=quark,iD=quark,iE=quark,iF=quark]{}\right)\right\rvert^2\right.\\
    &\left.+\left\lvert n\!\!\left(\gBoxBox[scale=0.8,all=gluon,iA=quark,iB=quark,iF=quark,iG=quark]{}\right)\right\rvert^2
    + \left\lvert n\!\!\left(\gBoxBox[scale=0.8,all=gluon,iC=quark,iD=quark,iE=quark,iG=quark]{}\right)\right\rvert^2\right)\,,
  \end{aligned}
\end{equation}
where we have used the fact that the SQCD numerators obey a matter reversal symmetry which allowed us to add twice only one direction of the hyper loop.
The modulus square represents the product of a numerator with its barred version $n_j \bar{n}_j$.
We have also dropped the external and loop momentum labels.
For a correct statement these will need to be inserted at the same position for each of the diagrams.

The same procedure applies to each topology.
For topologies allowing for two matter loops there are also terms proportional to $N_f^2$, e.g.
\begin{equation}
  \begin{aligned}
    %\tikzset{external/force remake}
    N\!\left(\gTriTri[scale=0.7]{}\right) =
    &\left\lvert n\!\!\left(\gTriTri[scale=0.7,all=gluon]{}\right)\right\rvert^2
    + 4N_f^2\left\vert n\!\!\left(\gTriTri[scale=0.7,all=quark,eA=gluon,eB=gluon,eC=gluon,eD=gluon,iD=gluon]{}\right)\right\rvert^2\\
    &+2N_f\left(\left\lvert n\!\!\left(\gTriTri[scale=0.7,all=gluon,iA=quark,iB=quark,iC=quark]{}\right)\right\rvert^2
    +\left\vert n\!\!\left(\gTriTri[scale=0.7,all=gluon,iE=quark,iF=quark,iG=quark]{}\right)\right\rvert^2\right).
  \end{aligned}
\end{equation}

The flavor counting parameter~$N_f$ can be eliminated using the relation $N_V=2(1+N_f)$, where $N_V$ counts the number of vector multiplets in the four-dimensional gravity theory.
The results can be interpreted in $D=4,5,6$ due to the six-dimensional construction (with external momenta always living in a four-dimensional subspace).

There is one further interesting detail for the six-dimensional construction.
Four-dimensional $\cN=2$ SQCD can either be mapped to $\cN=(1,0)$ or $\cN=(0,1)$ in six dimensions.
Hence, we can do two inequivalent double copies, reaching either $\cN=(2,0)$ or $\cN=(1,1)$ supergravity.
The pure $\cN=(1,1)$ supergraviton multiplet is factorizable.
The double copy for $\cN=(2,0)$ produces an additional tensor multiplet, which can be removed.
More details on these constructions can be found in appendix~\ref{app:rep} and paper~\ref{lbl:paper1}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{UV Divergences in Supergravity}

An interesting aspect of a supergravity amplitude is its ultraviolet (UV) structure.
We review a method to extract the UV divergence from a supergravity integrand, starting from a double copy construction as discussed in the previous section.
This method has been successfully applied to the supergravity amplitude of $\cN=4$ supergravity at two loops.
The results have also been published in paper~\ref{lbl:paper1}, which we will discuss at the end of this section.

There is no UV divergence for any supersymmetric theory at two loops in four dimensions.
The corresponding counterterms are schematically of the form $R^3$, where $R$ represents the Riemann tensor.
Supersymmetry rules out these counterterms and any amplitude is manifestly UV finite.
Hence, we will mostly consider amplitudes in five dimensions, where a supersymmetric counterterm exists.

A complete integration of the supergravity integrand followed by an expansion in small $\epsilon$ is complicated and leads to a mix up of UV and IR divergences.
A better approach, which is standard since long~\cite{Bern:2010tq}, is to isolate integrand contributions that diverge in the UV.
The basic idea is to first expand the numerator for small external momenta, leading to an expression of vacuum tensor integrals.
At two loops, there are two types of vacuum integrals, that are diagrammatically represented as
\begin{equation}
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize]
    \draw[aquark] (90:0.5) arc (90:270:0.5) node[pos=0.5,left] {$\ell_1$};
    \draw[quark] (-90:0.5) arc (-90:90:0.5) node[pos=0.5,right] {$\ell_3$};
    \draw[quark] (-90:0.5) -- node[right] {$\ell_2$} (90:0.5);
  \end{tikzpicture}
  \,,
  \quad
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex]current bounding box.center)},
      font=\scriptsize]
    \draw[aquark] (-0.2,0) arc (0:360:0.3) node[pos=0.5,left] {$\ell_1$};
    \draw[quark] (0.2,0) arc (-180:180:0.3) node[pos=0.5,right] {$\ell_2$};
    \draw[line] (-0.2,0) -- (0.2,0);
  \end{tikzpicture}\,,
\end{equation}
where we allow for propagator denominator factors of arbitrary power.
Formally, the second vacuum diagram is then a special case of the first one.
Mathematically, we define
\begin{equation}
  \begin{multlined}
    I^D[n(p,\ell),\{\nu_1,\nu_2,\nu_3\}] = \\
    \int\frac{\dd^{D}\ell_1\dd^{D}\ell_2}{(i\pi^{D/2})^2}
    \frac{n(p,\ell)}{(-\ell_1^2+m^2)^{\nu_1}(-\ell_2^2+m^2)^{\nu_2}(-\ell_3^2+m^2)^{\nu_3}}\,,
  \end{multlined}
\end{equation}
where $n(p,\ell)$ is a numerator factor depending on external and loop momenta.
We have also introduced a uniform mass regulator to handle possible infrared (IR) divergences~\cite{Bern:2012cd}.
In the presence of subdivergences, e.g. at two loops  in even spacetime dimensions, it is necessary to introduce the uniform mass regulator before the expansion in small external momenta.
For a vanishing or negative value of $\nu_i$ the integral can be reduced to a product of two one-loop integrals (see for example~\cite{Davydychev:1992mt}).

However, we choose another route by first performing a tensor reduction of $\ell$-dependent factors in the numerator.
A tensor reduction for vacuum integrals can be implemented efficiently using methods that will be discussed in sec.~\ref{sec:tensRedVac}.
The reduction to a basis of scalar integrals is most conveniently done via integration by parts (IBP) identities for which there exist completely automated implementations (for example LiteRed~\cite{Lee:2012cn,Lee:2013mka} and FIRE6~\cite{Smirnov:2019qkx}).
If the basis of scalar integrals is UV finite, the whole UV divergence is captured in the coefficients of the integrals.
This has the advantage that we do not need an explicit form of the integrals to check for cancellations among different contributions.

Let us turn to an example: the UV divergence of $\cN=4$ supergravity via the double copy of $(\cN=2\text{ SQCD})\otimes(\cN=2\text{ SQCD})$ in five dimensions.
A two-loop UV divergence has been shown to be absent via a double copy of $(\cN=4\text{ SYM})\otimes(\text{YM})$~\cite{Bern:2012gh}.
This effect is an example of an \emph{enhanced cancellation}~\cite{Bern:2014sna,Bern:2017lpv} as there is no known argument that would forbid a corresponding counterterm.
The cancellation is highly non-trivial since it only occurs for the assembled amplitude as a sum over all the individual diagrams and their permutations of external legs.

A choice of a basis of scalar integrals in five dimensions consists of the two UV finite integrals
\begin{equation}
  I^{(5-2\epsilon)}[1,\{1,2,3\}]\,, \quad I^{(5-2\epsilon)}[1,\{2,2,2\}]\,.
\end{equation}
In four dimensions, a basis of finite integrals is
\begin{equation}
  I^{(4-2\epsilon)}[1,\{1,2,2\}]\,, \quad I^{(4-2\epsilon)}[1,\{2,2,2\}]\,.
\end{equation}
Explicit expressions for these integrals in terms of generalized hypergeometric functions are known~\cite{Davydychev:1992mt}.

For the example of $\cN=4$ supergravity in $D=5-2\epsilon$, there are in total five diagrams that do not vanish upon integration.
For example, the UV divergence of the double box is
\begin{equation}
  %\tikzset{external/force remake}
  \begin{multlined}
    \left.(4\pi)^5\int\! \frac{\dd^{LD}\ell}{(2\pi)^{LD}D_{\text{db}}} N\!\left(\gBoxBox[scale=0.8,eLA=1,eLB=2,eLC=3,eLD=4,iLF=$\oset{\leftarrow}{\ell_1}$,iLE=$\oset{\rightarrow}{\ell_2}$]{}\right)\right\rvert_{\text{div}}=\\
    -\frac{(2+N_V)\pi}{70\epsilon}(\kappa_{12}^2+\kappa_{34}^2)-\frac{(29N_V-26)\pi}{210\epsilon}(\kappa_{13}^2+\kappa_{14}^2+\kappa_{23}^2+\kappa_{24}^2)\,,
  \end{multlined}
\end{equation}
where $D_{\text{db}}$ is the propagator denominator factor of the double box topology.
Similar expressions for the other four topologies have been given in publication~\ref{lbl:paper1}.
The sum over all permutations of the five topologies, including symmetry factors, reproduces the enhanced cancellation for the MHV sector in $D=5$ $\cN=4$ supergravity.
