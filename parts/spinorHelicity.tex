\chapter{Spinor Helicity Formalism}\label{ch:spinorHelicity}

Kinematic building blocks appearing in the integrand of scattering amplitudes are Lorentz-invariant objects built out of external data.
For us, the information consists of momenta and polarization vectors that are contracted with invariant objects --- for example the metric~$\eta^{\mu\nu}$ and the totally antisymmetric Levi-Civita symbol~$\epsilon^{\mu\nu\rho\sigma}$.
The external data for a physical process encoded inside an amplitude is constrained, for example, by on-shell criteria and momentum convervation.
Such constraints in general lead to non-linear relations between certain building blocks.
Hence, there exist many representations of the same integrand which differ significantly in structure and size.
Now, the challenge is to identify building blocks and find algorithms that bring a given amplitudes into a ``useful'' form, where the definition of usefulness is intentionally left open.
Some common criteria are the size of the algebraic expression, locality of the assembled amplitude, removal of redundancies (comparability) or manifestation of a certain (symmetry) property.
The spinor helicity formalism discussed in this section is useful in the sense that it trivializes the on-shellness of massless particles.

All processes considered in this thesis include exclusively massless states.
If $p_i$ denotes the momentum of an external parton with label~$i$ the massless condition reads
\begin{equation}
  p_i^2 = p_{i,\mu}p_i^\mu = 0.
\end{equation}
Furthermore, the overall momentum is conserved:
\begin{equation}
  \sum_i p_i = 0, 
\end{equation}
where we assume that all particles have outgoing momenta.
In general, a solution to these constraints contains square rootse and leads to blown-up expressions once resubstituted into an amplitude.
The spinor helicity formalism instead introduces a set of variables that trivializes the masslessness condition for the system.
A short enlightning introduction to the four dimensional spinor helicity formalism can for example be found in~\cite{Witten:2003nn}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Four Dimensions}

Following the notation in~\cite{Elvang:2013cua}, we first note that in four spacetime dimensions the Lorentz group is isomorphic to $\text{SL}(2)_\text{L}\times\text{SL}(2)_\text{R}$.
The finite-dimensional representations of $\text{SL}(2)$ are classified by half-integers.
Objects transforming in the two-dimensional representation $(1/2,0)$ and $(0,1/2)$ are called left- and right-handed chiral spinors respectively.
We denote left-handed chiral spinors by~$\sqket{p}_a$, $a=1,2$, and right-handed chiral spinors by $\ket{p}^{\dot{a}}$, $\dot{a}=1,2$.
We will later on identify~$p$ as an on-shell momentum.
Lorentz invariant objects are formed using the antisymmetric tensor $\epsilon_{ab}$, $\epsilon_{12}=+1$, which is used to raise or lower indices.
A useful shorthand is
\begin{align}
  \sqbraket{1 2} \equiv \sqbra{p_1}^a\sqket{p_2}_a = \epsilon^{ab}\sqket{p_1}_b\sqket{p_2}_a\,,
  & & \braket{1 2} \equiv \bra{p_1}_{\dot{a}} \ket{p_2}^{\dot{a}} = \epsilon_{\dot{a}\dot{b}}\ket{p_1}^{\dot{b}}\ket{p_2}^{\dot{a}}\,.
\end{align}

Consider a four-momentum vector~$p_\mu$ transforming under the vector $(1/2,1/2)$ representation of the Lorentz group.
By the above isomorphism, it is possible to map this vector to an object with both types of chiral indices~$p_{a\dot{a}}$.
The explicit form of the map uses the Pauli matrices
\begin{equation}
  p_{a\dot{a}} = \sigma^\mu_{a\dot{a}}p_\mu\,.
\end{equation}

Coming back to the on-shellness redundancy from before we observe that for massless momenta~$p$
\begin{equation}
  p_\mu p^\mu = \det(p_{a\dot{a}}) = 0\,.
\end{equation}
This implies that $p_{a\dot{a}}$ is a $2\times 2$ matrix with non-maximal rank and can be written as
\begin{equation}
  p_{a\dot{a}} = -\sqket{p}_a\bra{p}_{\dot{a}}\,,
\end{equation}
where $\sqket{p}_a$ and $\bra{p}_{\dot{a}}$ are chiral spinors as introduced above.
These two spinors are not uniquely determined since there is a rescaling freedom
\begin{align}
  \sqket{p} \rightarrow t \sqket{p}, & & \ket{p} \rightarrow \frac{1}{t} \ket{p}\,,
\end{align}
with a non-zero complex number~$t$. This corresponds to a little group scaling, i.e. a transformation in the subgroup of the Lorentz group that leaves the momentum invariant. Some useful properties and identities for spinor products are collected in the appendix of~\cite{Elvang:2013cua}.

By a slight abuse of notation we can insert a Dirac $\gamma$-matrix between two spinors
\begin{equation}
  \bra{p}P\sqket{q} = \bra{p} \gamma^\mu \sqket{q} P_\mu
  \equiv \begin{pmatrix}0, & \bra{p}_{\dot{a}}\end{pmatrix}
  \begin{pmatrix}0 & (\sigma^\mu)_{a\dot{a}}\\ (\bar{\sigma}^mu)^{a\dot{a}} & 0\end{pmatrix}
    \begin{pmatrix} \sqket{q}_a \\ 0\end{pmatrix} P_\mu\,,
\end{equation}
where $P$ is an arbitrary (not necessarily light-like) vector.
Polarization vectors for massless spin-1 vectors can then be expressed in spinor helicity notation using an arbitrary reference spinor~$q$
\begin{equation}
  \begin{aligned}\label{eq:polVect4D}
    \varepsilon_-^\mu(p;q) &= - \frac{\bra{p}\gamma^\mu\sqket{q}}{\sqrt{2}\sqbraket{qp}}\,, &
    \varepsilon_+^\mu(p;q) &= - \frac{\bra{q}\gamma^\mu\sqket{p}}{\sqrt{2}\braket{qp}}\,.
  \end{aligned}
\end{equation}
It can easily be checked that these polarization vectors fulfill the necessary properties, i.e. they are null and orthogonal to the corresponding momentum~$p$
\begin{align}
  \varepsilon_\pm(p;q)^2 &= 0\,, & \varepsilon_\pm^\mu(p;q) p_\mu &= 0\,.
\end{align}
The non-localities introduced by polarization vectors will lead to seemingly unphysical poles in amplitudes discussed in later sections.
The unitary cut formalism developed in publication~\ref{lbl:paper3} extracts these poles and absorbs them into special state-configuration objects.

Even though we discuss amplitudes in arbitrary dimensions --- mainly for the purpose of dimensional regularization --- we use four-dimensional notation and the spinor helicity formalism for the external objects.
One can see this as an embedding in a four-dimensional subspace.
To obtain information for the extra-dimensional part of dimensionality $D-4$, a six-dimensional spinor-helicity formalism introduced by Cheung and O'Connell~\cite{Cheung:2009dc} can be used.
We will not discuss the six-dimensional computation in this thesis.
Publications~\ref{lbl:paper1} and \ref{lbl:paper3} contain an extended discussion for the use of six-dimensional spinor-helicity for unitarity cuts.
