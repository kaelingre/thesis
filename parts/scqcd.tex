\chapter{Complete SQCD Amplitude at Two-Loops}\label{ch:scqcd}

The analytic form and the transcendentality proporties of amplitudes in $\cN=4$ SYM have been studied to high loop orders and multiplicity~\cite{Anastasiou:2003kj,Bern:2005iz,DelDuca:2009au,DelDuca:2010zg,Goncharov:2010jf,Dixon:2011pw,Dixon:2011nj,Dixon:2013eka,Dixon:2014iba,Dixon:2014voa,Drummond:2014ffa,Dixon:2015iva,Dixon:2016nkn,Caron-Huot:2016owq,Drummond:2018caf,Caron-Huot:2019vjl,Golden:2014xqa,Harrington:2015bdt,Golden:2014pua,Bern:1997nh,Naculich:2008ys,Henn:2016jdu,Abreu:2018aqd,Chicherin:2018yne}.
The integrated expressions for massless amplitudes are commonly expressed in terms of multiple polylogarithms (MPL)~\cite{Goncharov:1998kja,GoncharovMixedTate}.
These functions can be classified by the number of integrations in its definition, called the \emph{transcendental weight}.
The transcendentality of four-dimensional $L$-loop amplitudes has been observed to be bounded from above by $2L$.
Heuristically, $\cN=4$ SYM amplitudes are solely built out of MPLs of maximal transcendental weight~$2L$.
A uniform weight property allows for bootstrapping methods that determine the amplitude solely from its kinematical limits~\cite{Dixon:2011pw,Dixon:2011nj,Dixon:2013eka,Dixon:2014iba,Dixon:2014voa,Drummond:2014ffa,Dixon:2015iva,Dixon:2016nkn,Caron-Huot:2016owq,Drummond:2018caf,Caron-Huot:2019vjl,Almelid:2017qju}

For non-supersymmetric QCD terms of different transcendental weight appear.
Hence, the function space for bootstrapping techniques is much bigger.
An understanding of the transcendental structure of integrated amplitudes might improve such methods significantly.
Publication~\ref{lbl:paper4} discusses the fully integrated four-gluon amplitude for $\cN=2$ SQCD --- a testing ground between $\cN=4$ SYM and QCD --- at two loops and its transcendental properties.
We consider only the case for a $\text{SU}(N_c)$ gauge group here.
An interesting part of the discussion is about the amplitude at the superconformal phase of the theory, where the number of hypermultiplets~$N_f$ equals twice the number of colors~$N_c$, i.e. $N_f=2N_c$.
This theory is called $\cN=2$ superconformal quantum chromodynamics (SCQCD).
Its integrated amplitudes has a simpler transcendental structure and its expression fits into only a few lines.

We start by briefly discussing the computational methods used to obtain the integrated answer before discussing the transcendentality structure.
The color-kinematics dual representation of this amplitude, found in papers~\ref{lbl:paper1} and \ref{lbl:paper3}, is well suited for integration due to its simplicity coming from the low number of independent building blocks.
Modern integration techniques such as tensor reduction (see chapter~\ref{ch:tensorRed}), integration by parts identities (IBP)~\cite{Chetyrkin:1981qh,Tkachov:1981wb} or the differential equations method~\cite{Kotikov:1990kg,Remiddi:1997ny,Gehrmann:1999as} are easily available in various computer implementations.
For the specific amplitude discussed here the master integrals are known~\cite{Henn:2013pwa,Tausk:1999vh,Anastasiou:2000mf,Gehrmann:2005pd}.
For conventions regarding the UV renormalization see publication~\ref{lbl:paper4} attached to the thesis.

Due to our use of a supersymmetric decomposition~\eqref{eq:susyDecomp}, it is convenient to present the result as the difference to the two-loop $\cN=4$ SYM amplitude $\cA_4^{(2)[\cN=4]}$.
Furthermore, we split the remaining amplitude into parts that either do or do not contribute at the superconformal point
\begin{equation}
  \cA_4^(2) = \cA_4^{(2)[\cN=4]} + \cR_4^{(2)} + (2N_c - N_f) \cS_4^{(2)}\,.
\end{equation}
The remainder function $\cR_4^{(2)}$ captures the full dependence (together with the $\cN=4$ part) in the conformal case.
The completion to the full amplitude is collected in $\cS_4^{(2)}$. 

The properties and relations among the numerators in the integrand representation allow us to reduce the number of independent objects that actually need to be integrated.
For the conformal contribution in $\cR_4^{(2)}$ it even turns out that the leading color part is encoded in a single double-box integral of one numerator (and permutations thereof) combined with the known $\cN=4$ SYM result.

Through the formalism by Catani~\cite{Catani:1998bh} --- which relies on the known IR pole structure --- we can further split the remainder function into a part that is reproduced by the application of the one-loop Catani operator $\mathbb{I}^{(1)}(\epsilon)$ on the corresponding one-loop remainder
\begin{equation}
  \cR_4^{(2)}=\mathbb{I}^{(1)}(\epsilon) \cR_4^{(1)} + \cR_4^{(2)\text{fin}}\,,
\end{equation}
where $\cR_4^{(2)}\text{fin}$ is IR finite.
We will discuss this finite remainder for the superconformal theory ($N_f=2N_c$) in the rest of this section.

There are two independent helicity configurations for the leading-color contributions, i.e. terms proportional to $N_c^2\tr(T^{a_1}T^{a_2}T^{a_3}T^{a_4})$.
We denote the color-independent coefficient of this object by $R^{(2)[2]}_{(1234)}$.
The two contributions, expressed in classical $\Li_n(z)$ and Nielsen generalized polylogarithms $S_{n,\rho}(z)$, are
\begin{equation}
  \begin{aligned}
    R^{(2)[2]}_{--++} = &12\zeta_3+\frac{\tau}{6}\big[48 \Li_4(\tau)-24(T+U)\Li_3(\tau)-24T\Li_3(v)\\
      &-24S_{2,2}(\tau)+24 \Li_2(\tau)(\zeta_2 + TU) +24TU\Li_2(v)+T^4-4T^3U\\
      &+18T^2U^2-12\zeta_2T^2+24\zeta_2TU+24\zeta_3U-168\zeta_4\\
      &-4i\pi\big(6\Li_3(\tau)+6\Li_3(v)-6U\Li_2(\tau)-6U\Li_2(v)-T^3\\
      &+3T^2U-6TU^2-6\zeta_2T+6\zeta_2U\big)\big] + \cO(\epsilon)\,,\\
    R^{(2)[2]}_{-+-+} = &12\zeta_3+\frac{\tau}{6v^2}T^2(T+2i\pi)^2 + \cO(\epsilon)\,,
  \end{aligned}
\end{equation}
where we have used the shorthand notations $\tau=-t/s$, $v=-u/s$, $T=\log(\tau)$, and $U=\log(v)$ such that all appearing logarithms are real.
Furthermore, we dropped an overall factor of the tree level color-ordered amplitude of the corresponding helicity configuration $A_4^{(0)}=-i/(st) \kappa_{ij}$.
Also, we did not specify the finite superscript as these contributions are IR finite and represent the full color-independent remainder function.

The only term that breaks uniform transcendentality is proportional to $\zeta_3$, confirming previous results~\cite{Dixon2008talk,Leoni:2015zxa}.
This deviation might be a hint for a simple underlying structure in the non-uniform transcendental terms.
The subleading color contributation of the remainder are of equal complexity.
One of the two independent parts for different helicity combinations is even uniform transcendental, requiring non-trivial cancellations to happen among the different contributions.

These expressions are unexpectedly short and include only a restricted set of MPLs compared to the non-conformal amplitude $\cS_4^{(2)}$.
For gauge groups different from $\text{SU}(N_c)$ there exist two cases where uniform transcendentality is completely restored:
There is only a single diagram contributing to the abelian $\text{U}(1)$ theory which contains the above $\zeta_3$ term.
Summing over permutations exactly kills this term and the full amplitude has transcendentality 4.
Even more interestingly, for $\text{SO}(3)$ the $\cN=2$ SQCD is exactly the same as for $\cN=4$ with the same gauge group, and as such it is also uniform transcendental.
%\TODO{double trace terms}
