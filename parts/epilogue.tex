\chapter{Epilogue}

The ideas and methods summarized in this thesis are small steps towards a deeper understanding of the mathematical structure of scattering amplitudes and their fundamental building blocks.
As the complexity for each additional external parton or loop grows factorially, we are challenged to improve our technology for every new calculation.
We summarize the main findings and ideas and have a first look into interesting future directions and challenges awaiting us.

A supersymmetric reduction leads to a natural organization of on-shell states in terms of the on-shell superfield formulation for the maximally supersymmetric theory --- which extends even to the non-supersymmetric theories.
This organization allowed us to find general formulae for cuts involving vector, hyper, chiral, fermionic or scalar partons on the same footing.
Combined with the color-kinematics duality and new types of constraints, the number of independent building blocks can be significantly reduced.
This in turn simplifies the computation of the complete amplitude and shrinks the algebraic size of its representation.
The amplitudes for $\cN=2$ SQCD were shown to have an especially simple structure at one and two loops, manifesting, among others, IR properties in terms of collinear and soft limits.

A further step towards pure (massless) QCD is its supersymmetric extension with $\cN=1$.
This theory contains purely chiral matter multiplets.
The chirality is expected to introduce new structures implementing the chiral nature in the scattering amplitudes.
Compared to $\cN=1$ SQCD, the non-supersymmetric theory has no fundamentally new properties.
The same methods will be applicable for a BCJ construction, just the size of the Ansatz will be increased.

Seeking a color-kinematics dual representation might seem unnecessary for a computation solely in a gauge theory.
For some examples at one and two loops we have demonstrated that the duality can also help simplifying this computation.
The reason is, once more, the significant reduction in the number of independent building blocks.
The properties of our integrand representation that relate all terms to a small set of master numerators also lead to a reduction of independent integrals.
This immensely simplifies the integration process for the full amplitude.
The integrated representation of the four-point two-loop SQCD amplitude was performed to analyze its transcendental properties and general structure.
We have identified the source of the non-maximal transcendental terms for the theory at the conformal point.

Equipped with amplitudes for $\cN=0$ QCD through $\cN=4$ SYM, the double copy immediately opens the construction of amplitudes in Einstein gravity (with arbitrary matter) up to maximal supergravity.
We have discussed how matter states on the gravity side can be added or removed via a double copy of the matter states in the corresponding gauge theories.
This BCJ construction is currently the only method that will allow us to cover new terrain at higher multiplicity or loop order in the study of (supersymmetric) gravity amplitudes and their UV properties.
Explicit results obtained in publications attached to this thesis have reproduced an enhanced UV cancellation of half-maximal supergravity in five dimensions.
Further cases of such enhanced cancellations are expected for lower degrees of supersymmetry and at higher multiplicity.
Their realization in an amplitude representation might lead to novel insights into the UV structure of gravity theories.

A different approach of presenting an amplitude was discussed in the third part.
Color decompositions factorize an amplitude into kinematic and color parts.
Both, color and kinematic parts, are in the optimal case built out of an independent set of basic building blocks.
At tree and one-loop level, minimal decompositions for (S)QCD are known at any multiplicity.
The goal of this line of research is to identify a basis of kinematic building blocks for higher loops and, most challenging, for non-planar contributions.
A first interesting project would involve the study of identities among primitive amplitudes, i.e. a generalization of Kleiss-Kuijf relations to higher loop amplitudes including fundamental matter.
%In a rather speculative and hand-waving manner we have presented ideas for such a generalization.
%Starting from a proposed definition of primitive amplitudes a basis needs to be constructed leading to a color decomposition factoring into these basis elements.


In the final part of the thesis we have presented purely computational methods that efficiently solve a specific problem.
We discussed two methods for the tensor reduction of Feynman integrals and the use of finite fields for general amplitude computations.

It is expected that the methods presented here will be instrumental for further unraveling the properties of gauge and gravity theories with reduced supersymmetry.
At two loops four points, any SQCD and supergravity amplitude should be within computational reach.
Further refinements of the methods should open up the possibility of performing detailed three-loop studies of SQCD and related supergravity theories.
