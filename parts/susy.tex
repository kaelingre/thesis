\chapter{Supersymmetric Gauge and Gravity Theories}\label{ch:susy}

This thesis discusses scattering amplitudes in supersymmetrics gauge and gravity theories in four and six dimensions.
In this chapter we introduce the theories of interest with their particle content in terms of on-shell multiplets.
The main focus will lie on supersymmetric Yang-Mills theories (SYM) coupled to massless fundamental matter.
The basic building blocks that enter the computations in the following chapters are the tree-level amplitudes of these theories.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Supersymmetric Yang Mills Coupled to Matter}

On-shell tree-level amplitudes in maximally supersymmetric Yang-Mills can be derived from symmetry arguments and recursion relations without the need of specifying a Lagrangian~\cite{Drummond:2008cr}.
Tree-level superamplitudes for theories with less supersymmetry can subsequently be projected out from the maximally symmetric amplitudes.
Everything needed is thus the supermultiplet for the maximally supersymmetric theory and an understanding of the projection mechanism for a lower amount of supersymmetry.
We start by discussing the particle content of maximally supersymmetric Yang-Mills, followed by a reduction procedure for their non-maximally supersymmetric counterparts in four dimensions.
Via an uplift to six dimensions, we can obtain amplitudes in a dimensionally regulated theory.
Hence, we continue the discussion with the corresponding six dimensional theories and their relation to their four dimensional counterpart.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Four-Dimensional SYM}\label{sec:4DSYM}

The on-shell particle content for a supersymmetric theory is conveniently described by a superspace formalism.
Using Grassmann variables~$\eta^A$, $A=1,\cdots,\cN$, introduced by Ferber~\cite{Ferber:1977qx}, the on-shell particle content of the $\cN=4$ vector supermultiplet is organized as
\begin{equation}
  \cV_{\cN=4} = A^+ + \eta^A \psi_A^+ + \frac{1}{2} \eta^A \eta^B \phi_{AB} + \frac{1}{3!} \eta^A\eta^B\eta^C \psi_{ABC}^- + \eta^1\eta^2\eta^3\eta^4 A^-\,,
\end{equation}
where $A^\pm$ denote gluons, $\psi_A^+$ and $\psi_{ABC}^-\equiv-\epsilon_{ABCD}\psi^D_-$are the two helicity states of the four gluinos, and $\phi_{AB}\equiv\epsilon_{ABCD}\phi^{CD}$ describe three complex scalars.

The tree-level color-ordered amplitude in the maximally helicity violating (MHV) sector, containing the gluonic amplitude with configuration $(--+\cdots+)$, is especially simple.
It is given by the supersymmetric generalization of the Parke-Taylor formula~\cite{Parke:1986gb,Nair:1988bq}
\begin{equation}\label{eq:PT}
  A^{(0)[\cN=4]}_n = \frac{\delta^8(Q)}{\braket{12}\braket{23}\cdots\braket{n1}}\,,
\end{equation}
where $\delta^8(Q)$ represents the supermomentum conserving delta function.
Using the on-shell superspace formalism it is given by
\begin{equation}\label{eq:superQ}
  \delta^{2\cN}(Q) = \prod_{A=1}^\cN\sum_{i<j}^n \eta^A_i\braket{ij}\eta^A_j\,.
\end{equation}

We note that there is an asymmetry in our description as we could have equivalently written
\begin{equation}
  \cV_{\cN=4} = \etab_1\etab_2\etab_3\etab_4 A^+ +\frac{1}{3!} \etab_A\etab_B\etab_C \psi^{ABC}_+ + \frac{1}{2}\etab_A\etab_B \phi^{AB} + \etab_A \psi^A_- + A^-\,,
\end{equation}
using a conjugated set of Grassmann variables~$\etab_A$.
It is the natural set of variables to describe an amplitude in the \MHVb{} sector, for example for the gluon configuration $(++-\cdots-)$.
We refer to the former as the chiral and the latter as the antichiral on-shell superspace.

For certain applications discussed further on, it will be useful to have the ability to switch between the two formulations or even mix them.
The explicit transformation between the two superfields is given by a Grassmann Fourier transform~\cite{Elvang:2011fx}
\begin{equation}
  \cV_{\cN=4}(\eta) = \int \dd\etab_1\dd\etab_2\dd\etab_3\dd\etab_4 e^{\etab_1\eta^1+\etab_2\eta^2+\etab_3\eta^3+\etab_4\eta^4} \cV_{\cN=4}(\etab)\,.
\end{equation}

Expressions for on-shell vector and matter multiplets of non-maximally supersymmetric Yang-Mills theories are obtained by projecting onto the wanted states inside~$\cV_{\cN=4}$.
We will write down the multiplets using the chiral superspace.
As for $\cN=4$, there exists an equivalent description using antichiral Grassmann variables.
For $\cN=2$, we have the vector and a pair of conjugate (half-)hyper multiplets
\begin{equation}
  \begin{aligned}
    \cV_{\cN=2} &= A^+ + \eta^A \psi_A^+ + \eta^1\eta^2 \phi_{12} + \eta^3\eta^4\phi_{34} + \eta^A\eta^3\eta^4 \psi_{A34}^- + \eta^1\eta^2\eta^3\eta^4 A^-\,, \\
    \Phi_{\cN=2} &= \eta^3\psi_3^+ + \eta^A\eta^3\phi_{A3} + \eta^1\eta^2\eta^3 \psi_{123}^-\,,\\
    \overline{\Phi}_{\cN=2} &= \eta^4\psi_4^+ + \eta^A\eta^4\phi_{A4} + \eta^1\eta^2\eta^4 \psi_{124}^-\,,
  \end{aligned}
\end{equation}
where $A=1,2$.

The three supermultiplets $\cV_{\cN=2}$, $\Phi_{\cN=2}$, and $\overline{\Phi}_{\cN=2}$ form a complete supersymmetric decomposition of $\cV_{\cN=4}$:
\begin{equation}
  \cV_{\cN=4} = \cV_{\cN=2} + \Phi_{\cN=2} + \overline{\Phi}_{\cN=2}\,.
\end{equation}
This decomposition has useful consequences for the computation of amplitudes at loop-level.
One can demand a similar decomposition on the level of numerators for the integrand.

A further decomposition of $\cV_{\cN=2}$ provides the on-shell multiplets of $\cN=1$ SYM theory with additional chiral matter:
\begin{equation}
  \begin{aligned}
    \cV_{\cN=1} &= A^+ + \eta^1\psi_1^+ + \eta^2\eta^3\eta^4\psi_{234}^- + \eta^1\eta^2\eta^3\eta^4 A^-\,,\\
    \Phi_{\cN=1} &= \eta^2\psi_2^+ + \eta^1\eta^2\phi_{12}\,,\\
    \overline{\Phi}_{\cN=1} &= \eta^3\eta^4\phi_{34} + \eta^1\eta^3\eta^4\psi_{134}^-\,.
  \end{aligned}
\end{equation}
The hypermultiplet of $\cN=2$ can also be decomposed to describe another two sets of chiral-antichiral pairs of matter multiplets
\begin{equation}
  \begin{aligned}
    \Phi_{\cN=1}' &= \eta^3 \psi_3^+ + \eta^1\eta^3\phi_{13} \,,\\
    \overline{\Phi}_{\cN=1}' &= \eta^2\eta^3\phi_{23} + \eta^1\eta^2\eta^3 \psi_{123}^-\,,\\
    \Phi_{\cN=1}'' &= \eta^4 \psi_4^+ + \eta^1\eta^4\phi_{14} \,,\\
    \overline{\Phi}_{\cN=1}'' &= \eta^2\eta^4\phi_{24} + \eta^1\eta^2\eta^4\psi_{124}^-\,.
  \end{aligned}
\end{equation}
The three sets of chiral matter $\Phi$, $\Phi'$ and $\Phi''$ and their antichiral partners may transform in a subgroup of the underlying $\cN=4$ R-symmetry group.

These supermultiplets can finally be split into their non-supersymmetric constituents, parts of which form the on-shell states of massless quantum chromodynamics (QCD).

The $\cN=1$ or $\cN=2$ theory with
\begin{itemize}
\item a vector multiplet~$\cV$, transforming under the adjoint representation of the gauge group~$\text{SU}(N_c)$, coupled to
\item hyper/chiral multiplets~$\Phi$ and $\overline{\Phi}$, transforming under the fundamental representation of the gauge group,
\end{itemize}
is called supersymmetric QCD (SQCD).
The name stems from its similarity to (massless) QCD.
$\cV$ can be seen as a supersymmetric version of a gluon and $\Phi$ and $\overline{\Phi}$ as supersymmetric generalizations of quarks. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Six-Dimensional SYM}

A six dimensional on-shell superspace has been introduced by Dennen, Huang and Siegel (DHS) in~\cite{Dennen:2009vk}.
We mostly follow the notation of~\cite{Elvang:2011fx} to define the superfields and a map to the corresponding four dimensional theory.
Supermultiplets in maximal $(\cN,\widetilde{\cN})=(1,1)$ SYM in six dimensions can be defined using two sets of Grassmann variables~$\eta^{aI}$ and $\tilde{\eta}_{\dot{a}\dot{I}}$.
The indices $a$ and $\dot{a}$ belong to the little group~$\text{SU}(2)\times\text{SU}(2)$; $I$ and $\dot{I}$ are R-symmetry indices, which is $\text{USp}(2)\times\text{USp}(2)$ in this case~\cite{Dennen:2009vk,Huang:2010rn}.

Formally, $\eta^{aI}$ and $\tilde{\eta}_{\dot{a}\dot{I}}$ form the fermionic part of supertwistors transforming under the supergroup~$\text{OSp}^\ast(8|4)$.
Due to the self-conjugate property
\begin{equation}
  \{\eta^{aI},\eta^{bJ}\}=\epsilon^{ab}\Omega^{IJ},
\end{equation}
where $\Omega^{IJ}$ is the metric of the fermionic part of the supergroup~$\text{USp}(4)$, one needs to remove half of the degrees of freedom in order to arrive at a consistent superfield formalism.
The choices are to either break manifest R-symmetry or little group covariance.
The former has been successfully applied in amplitude computations~\cite{Heydeman:2017yww,Cachazo:2018hqa}, whereas the latter is more natural from an ambitwistor string point of view~\cite{Bandos:2014lja,Geyer:2018xgb}.

We choose to break the R symmetry covariance using the two independent Grassmann variables~$\eta^a$ and $\tilde{\eta}_{\dot{a}}$.
For maximally supersymmetric YM, the vector multiplet is then written as
\begin{equation}
  \begin{aligned}
    \cV_{\cN=(1,1)}= &\eta^a\tilde{\eta}_{\dot{a}} {g_a}^{\dot{a}} + \eta^a \chi_a + \tilde{\eta}_{\dot{a}} \tilde{\chi}^{\dot{a}} + \frac{1}{2}\tilde{\eta}^{\dot{a}}\tilde{\eta}_{\dot{a}}\eta^a\bar{\chi}_a + \frac{1}{2}\eta^a\eta_a \tilde{\eta}_{\dot{a}} \bar{\tilde{\chi}}^{\dot{a}}\\
    &+ \phi + \frac{1}{2}\eta^a\eta_a \phi' + \frac{1}{2}\tilde{\eta}_{\dot{a}}\tilde{\eta}^{\dot{a}} \phi'' + \frac{1}{4}\eta^a\eta_a\tilde{\eta}_{\dot{a}}\tilde{\eta}^{\dot{a}} \phi'''\,,
  \end{aligned}
\end{equation}
where the four vector states are encoded in ${g_a}^{\dot{a}}$;
$\chi_a$ and $\tilde{\chi}^{\dot{a}}$ together with their conjugates are Weyl fermions;
and $\phi$, $\phi'$, $\phi''$, and $\phi'''$ are the four scalar states of the theory.

Projecting out half of the states leads to the supermultiplets for $\cN=(1,0)$ SQCD:
\begin{equation}
  \begin{aligned}
    \cV_{\cN=(1,0)} &= \eta^a\tilde{\eta}_{\dot{a}} {g_a}^{\dot{a}} + \tilde{\eta}_{\dot{a}} \tilde{\chi}^{\dot{a}} + \frac{1}{2}\eta^a\eta_a \tilde{\eta}_{\dot{a}}\bar{\tilde{\chi}}^{\dot{a}}\,,\\
    \Phi_{\cN=(1,0)} &= \phi + \eta^a \chi_a + \frac{1}{2}\eta^a\eta_a\phi'\,,\\
    \overline{\Phi}_{\cN=(1,0)} &= \frac{1}{2}\tilde{\eta}_{\dot{a}}\tilde{\eta}^{\dot{a}}\phi'' + \frac{1}{2}\tilde{\eta}_{\dot{a}}\tilde{\eta}^{\dot{a}} \eta^a \bar{\chi}_a + \frac{1}{4}\eta^a\eta_a\tilde{\eta}_{\dot{a}}\tilde{\eta}^{\dot{a}}\phi'''\,,
  \end{aligned}
\end{equation}
and similarly for $\cN=(0,1)$:
\begin{equation}
  \begin{aligned}
    \cV_{\cN=(0,1)} &=\eta^a\tilde{\eta}_{\dot{a}} {g_a}^{\dot{a}} + \eta^a \chi_a + \frac{1}{2}\tilde{\eta}^{\dot{a}}\tilde{\eta}_{\dot{a}}\eta^a\bar{\chi}_a\,,\\
	\Phi_{\cN=(0,1)} &= \phi + \tilde{\eta}_{\dot{a}} \tilde{\chi}^{\dot{a}} + \frac{1}{2}\tilde{\eta}_{\dot{a}}\tilde{\eta}^{\dot{a}} \phi'' \,,\\
    \overline{\Phi}_{\cN=(0,1)} &= \frac{1}{2}\eta^a\eta_a \phi' + \frac{1}{2}\eta^a\eta_a \tilde{\eta}_{\dot{a}} \bar{\tilde{\chi}}^{\dot{a}} + \frac{1}{4}\eta^a\eta_a\tilde{\eta}_{\dot{a}}\tilde{\eta}^{\dot{a}} \phi'''\,.
  \end{aligned}
\end{equation}

Since $\cN=4$ SYM in four and $\cN=(1,1)$ in six dimensions have the same on-shell degrees of freedom  one can identify states through a one-to-one map.
A Grassmann Fourier transform brings the four dimensional vector multiplet into a form resembling its six dimensional counterpart
\begin{equation}
  \begin{aligned}
    \MoveEqLeft[5] \int\dd\eta^2\dd\eta^3e^{\eta^2\bar{\eta}_2+\eta^3\bar{\eta}_3}\cV_{\cN=4} =\\
    &-\bar{\eta}_2\bar{\eta}_3 A^+ + \eta^1\eta^4 A^- + \eta^4\bar{\eta_3}\phi_{12} + \eta^4\bar{\eta}_2\phi_{34}\\
    &+\eta^1 \psi_{123}^- - \bar{\eta}_2\psi_3^+ + \bar{\eta}_3 \psi_2^+ + \eta^4 \psi_{234}^-\\
    &-\eta^1\eta^4\bar{\eta}_3\psi_{124}^- - \eta^4\bar{\eta}_2\bar{\eta}_3\psi_4^+ -\eta^1\bar{\eta}_2\bar{\eta}_3\psi_1^+ + \eta^1\eta^4\bar{\eta}_2 \psi_{134}^-\\
    &+\phi_{23} - \eta^1\bar{\eta}_2\phi_{13} - \eta^4\bar{\eta}_3\phi_{24} - \eta^1\eta_4\bar{\eta_2}\bar{\eta_3}\phi_{14}\,.
  \end{aligned}
\end{equation}
Identifying ${}^{(4)}\eta^1\lra{}^{(6)}\eta^1$, ${}^{(4)}\bar{\eta}_2 \lra {}^{(6)}\eta^2$, ${}^{(4)}\bar{\eta}_{3}\lra{}^{(6)}\tilde{\eta}_{\dot{1}}$ and ${}^{(4)}\eta^4\lra{}^{(6)}\tilde{\eta}_{\dot{2}}$ leads to the following map between the states
\begin{equation}\label{eq:map4D6D}
  \begin{aligned}
    A^+ &\lra -{g_2}^{\dot{1}}\,, & A^- &\lra {g_1}^{\dot{2}}\,, & \phi_{12} &\lra {g_1}^{\dot{1}}\,, & \phi_{34} &\lra -{g_2}^{\dot{2}}\,,\\
    \psi_{123}^- &\lra \chi_1\,, & \psi_3^+ &\lra -\chi_2\,, & \psi_2^+ &\lra \tilde{\chi}^{\dot{1}}\,, & \psi_{234}^- &\lra \tilde{\chi}^{\dot{2}}\,,\\
    \psi_{124}^- &\lra \bar{\chi}_1\,, & \psi_4^+ &\lra -\bar{\chi}_2\,, & \psi_1^+ &\lra -\bar{\tilde{\chi}}^{\dot{1}}\,, & \psi_{134}^- &\lra -\bar{\tilde{\chi}}^{\dot{2}}\,,\\
    \phi_{23} &\lra \phi\,, & \phi_{13} &\lra -\phi'\,, & \phi_{24} &\lra \phi''\,, & \phi_{14} &\lra - \phi'''\,.
  \end{aligned}
\end{equation}
This map is also valid for finding a one-to-one relationship between the states of four dimensional $\cN=2$ and six dimensional $\cN=(1,0)$ or $\cN=(0,1)$ SQCD

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Supergravity}\label{sec:sugra}

Instead of repeating the analysis as in the previous section, we directly construct the spectrum of various supergravity theories as a double copy of the spectrum of two supersymmetric gauge theories.
This construction, targeted to amplitude computations with the inclusion of matter multiplets, has first been presented in~\cite{Johansson:2014zca} for four dimensions and in paper~\ref{lbl:paper1} for six dimensions.
Here we focus on supergravity theories with $\cN=8$ and $\cN=4$ in four dimensions and the corresponding theories $\cN=(2,2)$, $\cN=(1,1)$ and $\cN=(2,0)$ in six dimensions.
The spectrum for $\cN=6,5,3,2,1,0$ and their six dimensional cousins, for the cases they exist, can be obtained through a similar treatment.
We summarize the various double-copied multiplets for $\cN=0,1,2,4,6,8$ in four dimensions and $\cN=(0,0),(1,0),(2,0),(1,1),(2,1),(2,2)$ in six dimensions respectively in appendix~\ref{app:rep}.

\paragraph{$\cN=8$ in $D=4$} We start with maximal supersymmetric gravity in four spacetime dimensions.
By supercharge counting, we can only reach it as a double copy of $(\cN=4\text{ SYM})\times(\cN=4\text{ SYM})$.
Hence, there exists a unique graviton supermultiplet
\begin{equation}
  \cV_{\cN=4}\otimes\cV_{\cN=4} = \cH_{\cN=8}\,,
\end{equation}
as a double copy of two vector multiplets of $\cN=4$ super Yang-Mills.
Upon appropriate renaming of the Grassmann variables $\eta^A$ in each of the vector multiplets, one obtains an explicit expression for the on-shell superfield of the form
\begin{equation}
  \begin{multlined}
    \cH_{\cN=8} = h^{++} + \eta^A \chi_A^+ + \dots + \eta^A\eta^B\eta^C\eta^D\eta^E\eta^F\eta^G\chi_{ABCDEFG}^- \\
    + \eta^1\eta^2\eta^3\eta^4\eta^5\eta^6\eta^7\eta^8 h^{--}\,,
  \end{multlined}
\end{equation}
in the chiral superspace formalism.
Here $h^{++}$ and $h^{--}$ denote the two graviton states and $\chi^{\pm}$ are the two states of the 16 gravitini.
It is implied that states with spin one and smaller are suppressed.

\paragraph{$\cN=4$ in $D=4$} For half-maximal supergravity, there exist two different useful constructions.
Inspecting the double copy construction of the vector multiplets of $(\cN=4\text{ SYM})\times\text{YM}$, one finds that pure $\cN=4$ supergravity is factorizable
\begin{equation}
  \cV_{\cN=4}\otimes\cV_{\cN=0} = \cH_{\cN=4}\,,
\end{equation}
where the on-shell vector multiplet of pure Yang-Mills consists simply of the two gluon states
\begin{equation}
  \cV_{\cN=0} = A^+ + \eta^1\eta^2\eta^3\eta^4 A^-\,.
\end{equation}

A construction via $(\cN=2\text{ SYM})\times(\cN=2\text{ SYM})$ leads to additional states on the supergravity side:
\begin{equation}
  \cV_{\cN=2}\otimes\cV_{\cN=2} = \cH_{\cN=4} \oplus 2\cV_{\cN=4}\,.
\end{equation}
A double copy construction of this form leads thus to amplitudes in a supergravity theory coupled to two vector multiplets.

Adding matter to the gauge theory, one can for example consider a double copy of $(\cN=2\text{ SQCD})\times(\cN=2\text{ SQCD})$.
A tensor product of the hypermultiplets leads to a vector supermultiplet on the gravity side
\begin{equation}
  \begin{aligned}
    \Phi_{\cN=2}\otimes\overline{\Phi}_{\cN=2} &= \cV_{\cN=4}\,,\\
    \overline{\Phi}_{\cN=2}\otimes\Phi_{\cN=2} &= \cV_{\cN=4}\,.\\
  \end{aligned}
\end{equation}
Of course, the same can be achieved by adding scalars to the pure YM side in the former double copy
\begin{equation}
  \cV_{\cN=4}\otimes\phi = \cV_{\cN=4}\,.
\end{equation}

More exotic massive matter content on the gravity side can be obtained by double copies of ``cross-terms'' like $\cV\otimes\Phi$, see e.g.~\cite{Chiodaroli:2017ehv}.
They are of less interest for the purpose of this thesis and we will not discuss them.
An interesting observation is that the double copy of two hypermultiplets exactly gives the additional states that render the graviton multiplet non-factorizable into two copies of $\cN=2$.
We will use this to generalize the double copy construction discussed in the previous section to also allow for a construction of pure supergravity via $(\cN=2)\times(\cN=2)$.

\paragraph{$\cN=(2,2)$ in $D=6$}
The particle content of maximal supergravity in six dimensions is equivalent to $\cN=4$ in four dimensions, stemming from the equivalence of the corresponding SYM vector multiplets.
We directly obtain the graviton on-shell multiplet via
\begin{equation}
  \cV_{\cN=(1,1)}\otimes\cV_{\cN=(1,1)} = \cH_{\cN=(2,2)}\,.
\end{equation}
A one-to-one map between four and six dimensions is found by a double copy of the gauge theory spectra, for which the correspondence was given in eq.~\eqref{eq:map4D6D}.

\paragraph{$\cN=(1,1)$ and $\cN=(2,0)$ in $D=6$}
The lift of $\cN=4$ supergravity to six dimensions is related to $\cN=(1,1)$ and $\cN=(2,0)$ supergravity.
Both of these theories can be double-copied from gauge theory building blocks.
The $\cN=(1,1)$ graviton multiplet is factorizable in two ways
\begin{equation}
  \begin{aligned}
    \cV_{\cN=(1,0)} \otimes \cV_{\cN=(0,1)} &= \cH_{\cN=(1,1)}\,,\\
    \cV_{\cN=(1,1)} \otimes \cV_{\cN=(0,0)} &= \cH_{\cN=(1,1)}\,.
  \end{aligned}
\end{equation}
In contrast, the graviton multiplet in the chiral $\cN=(2,0)$ theory is non-factorizable
\begin{equation}
  \cV_{\cN=(1,0)} \otimes \cV_{\cN=(1,0)} = \cH_{\cN=(2,0)} \oplus \cT_{\cN=(2,0)}\,,
\end{equation}
where $\cT$ denotes a tensor multiplet~\cite{Howe:1983fr}.

The double copy of the hypermultiplets leads to vector and tensor supermultiplets respectively
\begin{equation}
  \begin{aligned}
    \Phi_{\cN=(1,0)}\otimes\overline{\Phi}_{\cN=(0,1)} &= \cV_{\cN=(1,1)}\,,\\
    \overline{\Phi}_{\cN=(1,0)}\otimes\Phi_{\cN=(0,1)} &= \cV_{\cN=(1,1)}\,,\\
    \Phi_{\cN=(1,0)}\otimes\overline{\Phi}_{\cN=(1,0)} &= \cT_{\cN=(2,0)}\,.\\
  \end{aligned}
\end{equation}
We observe once more that the double copy of matter multiplets leads to gravitational matter appearing in the double copy of vector supermultiplets.
