\chapter{Color-Kinematics Duality and the Double Copy}\label{ch:bcj}

The amplitude computations in article~\ref{lbl:paper1} and~\ref{lbl:paper3} use a construction introduced by Bern, Carrasco and Johansson (BCJ).
The two main ideas --- the \emph{color-kinematics duality} for gauge theories and the \emph{double copy construction} for gravity amplitudes~\cite{Bern:2008qj,Bern:2010ue} --- are summarized as:
\begin{itemize}
\item There exists a duality between the color algebra and kinematic objects, the numerator factors, of gauge theory amplitudes.
  A representation of a gauge theory amplitude that manifestly implements this duality is called \emph{color-kinematics dual}.
\item Certain gravity amplitudes can be obtained by a procedure which acts on a color-kinematics dual form of a gauge theory amplitude.
  The prescription is to replace color objects by their kinematic counterparts.
\end{itemize}
This construction has been used to obtain gravitational amplitudes from the simpler gauge theory expressions.
Gravitational amplitudes are extremely difficult, if not impossible, to compute with a classical Feynman graph approach even with today's computing power.
Once a color-dual form of the gauge theory amplitude has been found, its double copy necessarily is invariant under linearized diffeomorphism.
Thus it is a valid candidate for a gravitational amplitude (of some gravity theory)~\cite{Chiodaroli:2017ngp}.
There is no complete criterion for the existence of a color-dual form at general loop level.
The existence has been proven for specific cases at tree level and even for some cases at higher orders~\cite{BjerrumBohr:2009rd,Stieberger:2009hq,Cachazo:2012uq,Feng:2010my,Chen:2011jxa}.
The only way to check the duality is thus by finding an explicit color-kinematics dual representation.

Another open question concerns the set of (super)gravity theories that are constructible via a double copy.
We call a gravity theory \emph{factorizable} if its spectrum can be written as a tensor product of the full spectrum of two (not necessarily equal) gauge theories, schematically
\begin{equation}
  (\text{gravity theory}) = (\text{gauge theory}) \otimes (\text{gauge theory})'\,.
\end{equation}
Factorizable theories are not the only ones that are BCJ-constructible.
In~\cite{Johansson:2014zca}, Johansson and Ochirov developed an extension of the usual double copy construction by adding matter multiplets to the gauge theory.
This allows for the construction of a larger set of gravity theories.
Publication~\ref{lbl:paper1} uses this construction at two-loop level and verifies the existence of the duality for this specific case.
In publication~\ref{lbl:paper3} we constructed gauge theory amplitudes with matter on external legs.
This allows for matter-matter or matter-graviton scattering on the double copy side.

In this section we review the basics of the color-kinematics duality and the double copy as far needed for part~\ref{part:amps}.
The last subsection will also briefly introduce BCJ-relations, which motivate the story told in part~\ref{part:color} of this thesis.
For an extensive review see for example~\cite{BCJ}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{A Duality between Color and Kinematics}\label{sec:colorKinDual}

To expose the duality, we start from a diagrammatic representation of a gauge theory amplitude.
The color factor of a four-point vertex can be expressed as a sum of products of two three-point color-factors.
Therefore, every $L$-loop amplitude admits a representation in terms of a sum over trivalent diagrams~$\Gamma_n^{(L)}$ with $n$ external legs
\begin{equation}\label{eq:trivalentRep}
  \cA_n^{(L)}=i^{L-1}g^{n+2L-2} \sum_{i\in\Gamma_n^{(L)}} \int \frac{\dd^{LD}\ell}{(2\pi)^{LD}}\frac{1}{S_i}\frac{n_ic_i}{D_i}\,.
\end{equation}
Each summand, corresponding to a graph, consists of four building blocks,
\begin{itemize}
\item the symmetry factor~$S_i$ of the graph,
\item the denominator~$D_i$ built as a product of inverse Feynman propagators,
\item the color factors~$c_i$ according to usual Yang-Mills Feynman rules,
\item and numerators~$n_i$ capturing the leftover kinematical dependence of the amplitude.
  Note that these numerators are not necessarily local as they might contain spurious poles.
\end{itemize}

For the gauge theories considered in the thesis --- massless QCD with gauge group $\text{SU}(N_c)$ or supersymmetric extensions thereof --- color factors of trivalent graphs are built out of two basic building blocks:
the structure constants $\tilde{f}^{abc}=\tr([T^a,T^b]T^c)$ and generators $T^a_{i\bar\jmath}$ of the gauge group, normalized as $\tr(T^aT^b) = \delta^{ab}$.
We use the convention that $T^a$ are hermitian and $f_{abc}$ are imaginary.
Graphically, we represent them as
\begin{align}\label{eq:colorDef}
  \tilde{f}^{abc} &= c\left(\gTreeTri[all=gluon,eLA=$a$,eLB=$b$,eLC=$c$]{}\right)\,,
  & T^a_{i\bar\jmath} &= c\left(\gTreeTri[eA=gluon,eB=aquark,eC=quark,eLA=$a$,eLB=$i$,eLC=$\bar\jmath$]{}\right)\,.
\end{align}
By defining $T^a_{\bar\imath j}\equiv -T^a_{j\bar\imath}$, we observe that both building blocks are antisymmetric under a flip of legs since also $\tilde{f}^{abc}=-\tilde{f}^{acb}=-\tilde{f}^{bac}$.
Furthermore, properties of the gauge algebra include similar identities for these building blocks:
Color factors of a triplet of four-point subgraphs fulfill the commutation relation and the Jacobi identity respectively:
\begin{equation}\label{eq:jacsComms}
  \begin{aligned}
    \tilde{f}^{abc} T^c_{i\bar\jmath} &= T^b_{i\bar{k}}T^a_{k\bar\jmath} - T^a_{i\bar{k}}T^b_{k\bar\jmath} = [T^a,T^b]_{i\bar\jmath}\\
    %\tikzset{external/force remake}
    \Leftrightarrow c\left(\gTreeS[eA=gluon,eB=gluon,eC=quark,eD=aquark,iA=gluon,eLA=$a$,eLB=$b$,eLC=$i$,eLD=$j$,iLA=$c$]{}\right)
    & = c\left(\gTreeT[eA=gluon,eB=gluon,eC=quark,eD=aquark,iA=aquark,eLA=$a$,eLB=$b$,eLC=$i$,eLD=$j$,iLA=$k$]{}\right)
    - c\left(\gTreeU[eA=gluon,eB=gluon,eC=quark,eD=aquark,iA=aquark,eLA=$a$,eLB=$b$,eLC=$i$,eLD=$j$,iLA=$k$]{}\right)\,,\\
    \tilde{f}^{abe}\tilde{f}^{cde} &= \tilde{f}^{dae}\tilde{f}^{bce} - \tilde{f}^{dbe}\tilde{f}^{ace}\\
    \Leftrightarrow c\left(\gTreeS[all=gluon,eLA=$a$,eLB=$b$,eLC=$c$,eLD=$d$,iLA=$e$]{}\right)
    &= c\left(\gTreeT[all=gluon,eLA=$a$,eLB=$b$,eLC=$c$,eLD=$d$,iLA=$e$]{}\right)
    - c\left(\gTreeU[all=gluon,eLA=$a$,eLB=$b$,eLC=$c$,eLD=$d$,iLA=$e$]{}\right)\,.
  \end{aligned}
\end{equation}
Combined with the antisymmetry property, this leads to a system of relations among the color factors in eq.~\eqref{eq:trivalentRep} of the schematic form
\begin{align}
  c_i &= c_j - c_k\,, & c_i &= -c_j\,.
\end{align}
Whereas the symmetry factors, denominators, and color factors are unique\-ly determined by the graph, the numerator factors are not unique and admit a \emph{generalized gauge freedom}.

Now, the duality between color and kinematics is exposed by a set of numerators~$n_i$, which fulfill the same identities as the corresponding color factors
\begin{equation}
  \begin{aligned}
    c_i &= c_j - c_k & \Leftrightarrow \quad n_i &= n_j - n_k\,,\\
    c_i &= -c_j & \Leftrightarrow  \quad n_i &= -n_j\,.
  \end{aligned}
\end{equation}
These constraints do still not uniquely determine~$n_i$ for most cases.
This leaves us some residual generalized gauge freedom.

Apart from the double copy prescription described in the next subsection, this representation has the advantage of manifestly reducing the number of independent building blocks compared to a standard color decomposition.
The identities between the kinematical building blocks~$n_i$ expose new relations between color-ordered amplitudes.
These identities will be discussed in the last section of this chapter.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Gravity Amplitudes as Squares}
An important application of the color-kinematics duality is the double-copy construction for gravitational amplitudes.
Assume we have found a color-kinematics dual representation~\eqref{eq:trivalentRep} of two gauge theory amplitudes at $L$ loops with numerators $n_i$ and $n_i'$ respectively.
From these gauge theory building blocks we compute the following object
\begin{equation}\label{eq:doubleCopyM}
  \cM_n^{(L)} = i^{L-1}\left(\frac{\kappa}{2}\right)^{n+2L-2} \sum_{i\in\Gamma_n^{(L)}}\int\frac{\dd{LD}\ell}{(2\pi)^{LD}} \frac{1}{S_i}\frac{n_in_i'}{D_i}\,,
\end{equation}
where we replaced the YM coupling constant $g$ by the gravitational coupling~$\kappa$ and the color factors~$c_i$ by the second set of numerators~$n_i'$.
This object is invariant under linearized diffeomorphisms and has the correct dimensions to be a valid candidate for a gravitational scattering amplitude.
An extended discussion, why this object is a gravitational scattering amplitudes can, for example, be found in~\cite{Bern:2010yg} for $\cN=8$ at tree level or in \cite{BCJ} more generally.

Two comments are in order: Firstly, the second copy of numerator factors do not need to come from the same gauge theory.
Secondly, only one of the two sets of numerators needs to be in color-kinematics dual form.
The other set can be in any (trivalent) representation.
We elaborate more on these properties in the rest of this section, following the discussion in~\cite{Chiodaroli:2017ngp,BCJ}.

A gauge invariant action implies amplitudes that are invariant under linearized gauge transformations.
Explicitly, the amplitude is invariant under the shift of the polarization vector~$\varepsilon_\mu$ of an external gluon by the corresponding momentum $\varepsilon_\mu\rightarrow \varepsilon_\mu + p_\mu$.
The polarization vector $\varepsilon_\mu$ fulfills $\varepsilon\cdot\varepsilon=0$.
In terms of the integrand we write this as $n_i\rightarrow n_i+\delta_i$, where $\delta_i=n_i|_{\varepsilon\rightarrow p}$.
Gauge invariance then implies
\begin{equation}\label{eq:gaugeInvAmp}
  \sum_i \frac{c_i\delta_i}{D_i} = 0\,.
\end{equation}

Similarly, from a diffeomorphism invariant action follows the invariance of amplitudes under linearized diffeomorphisms
\begin{equation}\label{eq:linDiff}
  \varepsilon_{\mu\nu}\rightarrow\varepsilon_{\mu\nu}+p_{(\mu}q_{\nu)}\,,
\end{equation}
for $\varepsilon_{\mu\nu}$ the polarization tensor of an external graviton and an auxiliary vector~$q$ that obeys $p\cdot q = 0$.
This transformation preserves transversality~$\varepsilon_{\mu\nu}p^\nu =0$ and tracelessness~$\varepsilon_{\mu\nu}\eta^{\mu\nu}=0$ of the polarization tensor.

In the double copy, the graviton polarization vector is expressed in terms of two gluon polarization vectors from both factors of the double copy as the symmetric traceless product
\begin{equation}
  \varepsilon_{\mu\nu} = \varepsilon_{((\mu}\tilde{\varepsilon}_{\nu))} = \varepsilon_{(\mu}\tilde{\varepsilon}_{\nu)} - \eta^{\mu\nu}\varepsilon_{\mu}\tilde{\varepsilon}_{\nu}\,.
\end{equation}
Linearized diffeomorphisms~\eqref{eq:linDiff} are then realized by a linearized gauge transformation $\varepsilon_\mu\rightarrow\varepsilon_\mu+p_\mu$ and replacing $\tilde{\varepsilon}_\mu\rightarrow q_\mu$ or vice versa.
The integrand of the gravity amplitude~\eqref{eq:doubleCopyM} finally changes by a factor proportional to
\begin{equation}
  \sum_i \frac{\delta_i\tilde{n}_i|_{\tilde{\varepsilon}\rightarrow q}}{D_i} + \sum_i \frac{n_i|_{\varepsilon\rightarrow q}\tilde{\delta}_i}{D_i}\,.
\end{equation}
Both these terms vanish by eq.~\eqref{eq:gaugeInvAmp} as the numerator factors fulfill the same algebraic identities as the color factors and we conclude that the amplitude obtained via a double copy is invariant under linearized diffeomorphisms.
However, we have assumed that both numerator factors are color-kinematics dual.

The generalized gauge freedom mentioned in the previous section allows a deformation of numerator factors $n_i\rightarrow n_i + \Delta_i$ such that the complete amplitude (integrand) is unchanged, i.e.
\begin{equation}
  \sum_i \frac{\Delta_i c_i}{D_i} = 0\,.
\end{equation}
The main point is the observation that this constraint holds due to the algebraic properties of the color factors and not their explicit values.
As color-kinematics dual numerators~$n_i$ fulfill the same algebraic properties we immediately conclude that
\begin{equation}
  \sum_i \frac{\Delta_i n_i}{D_i} = 0\,.
\end{equation}
This shows that if the second set of numerators is shifted by $n_i' \rightarrow n_i' + \Delta{i}$ the graviton amplitude~\eqref{eq:doubleCopyM} is unchanged --- as long as the first set of numerators remains in a color-kinematics dual form.
This proves that only one set of numerators needs to be color-kinematics dual.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Relations for Color-Ordered Amplitudes}\label{sec:bcjRels}

In a color-kinematics dual construction the separation of color and kinematics dependent objects is governed on a diagrammatic level.
The traditional way of separating color and kinematics is done on a higher level by identifying a set of independent color objects~$\{c_i\}$.
A gauge theory amplitude can in general be written as
\begin{equation}
  \cA = \sum_i c_i A_i\,,
\end{equation}
where $c_i$ solely depend on structure constants~$\tilde{f}^{abc}$ and generators~$T^a_{i\bar\jmath}$.
The factors $A_i$ collect all other (kinematic) dependence, including propagators.

Such a decomposition is useful as long as the kinematical objects~$A_i$ are simpler to compute than the whole amplitude.
Consider for example a \emph{trace-based} decomposition of a Yang-Mills tree level amplitude~\cite{Mangano:1988kk, Berends:1987cv, Kosower:1987ic, Mangano:1987xk, Bern:1990ux}
\begin{equation}
  \cA_n^{(0)} = g^{n-2}\sum_{\sigma\in S_{n-1}}  \tr\left(T^{a_1}T^{a_{\sigma(2)}}\cdots T^{a_{\sigma(n)}}\right) A_n^{(0)}(1,\sigma)\,,
\end{equation}
where the sum is over a permutation~$\sigma$ of the gluon labels~$2,\dots,n$.
The kinematical factors $A_n$ are called \emph{color-ordered} or \emph{partial amplitudes} and can be computed diagrammatically using color-ordered Feynman rules~\cite{Dixon:1996wi} and obtained recursively for higher points (e.g. BCFW~\cite{Britto:2004ap,Britto:2005fq}).

The color-ordered amplitudes fulfill a number of identities.
\begin{itemize}
\item From the cyclicity of the trace, cyclicity of~$A_n$ follows
  \begin{equation}
    A_n(1,2,\dots,n) = A_n(2,\dots,n,1)\,.
  \end{equation}
\item A reversal of its arguments is compensated by a sign flip for odd~$n$
  \begin{equation}
    A_n(1,2,\dots,n) = (-1)^n A_n(n,\dots,2,1)\,.
  \end{equation}
\item The Kleiss-Kuijf (KK) relations~\cite{Kleiss:1988ne} reduce the number of independent color-ordered amplitudes to $(n-2)!$
  \begin{equation}\label{eq:kk}
    A_n(1,\alpha,n,\beta) = (-1)^{\lvert\beta\rvert} \sum_{\sigma\in\alpha\shuffle\beta} A_n(1,\sigma,n)\,.
  \end{equation}
  The symbol~$\shuffle$ denotes the shuffle product which is defined as the set of permutations of the set $\alpha\cup\beta$ that leaves the relative ordering of $\alpha$ and $\beta$ intact.
\end{itemize}
The KK relations motivate a color-decomposition that removes this redundancy and reduces the sum to $(n-2)!$ terms.
This is explicitly realized through a construction by Del Duca, Dixon, and Maltoni (DDM)~\cite{DelDuca:1999iql,DelDuca:1999rs}
\begin{equation}\label{eq:ddm}
  \begin{aligned}
    \cA_n^{(0)} &= g^{n-2}\sum_{\sigma\in S_{n-2}} \tilde{f}^{a_2a_{\sigma(3)}b_1}\tilde{f}^{b_1a_{\sigma(4)}b_2}\cdots\tilde{f}^{b_{n-3}a_{\sigma(n)}a_1} A_n^{(0)}(1,2,\sigma)\\
    &= g^{n-2}\sum_{\sigma\in S_{n-2}}
    c\left(
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex]current bounding box.center)},
        font=\scriptsize]
      \draw[gluon] (0,0) node[left] {$a_2$} -- (0.4,0);
      \draw[gluon] (0.4,0) -- (0.4,0.4) node[above] {$a_{\sigma(3)}$};
      \draw[gluon] (0.4,0) -- (0.8,0);
      \draw[dotted] (0.8,0) -- (1.2,0);
      \draw[gluon] (1.2,0) -- (1.6,0);
      \draw[gluon] (1.6,0) --  (1.6,0.4) node[above] {$a_{\sigma(n)}$};
      \draw[gluon] (1.6,0) -- (2,0) node[right] {$a_1$};
    \end{tikzpicture}
    \right)A_n^{(0)}(1,2,\sigma)\,,
  \end{aligned}
\end{equation}
where the first argument of each summand is a color factor of a \emph{half-ladder} graph.
We will further discuss this problem for amplitudes including matter in a non-adjoint representation in part~\ref{part:color}.

Generically, there exist no further linear relations between color-ordered tree amplitudes over the field of rational numbers.
Allowing for kinematics-dependent coefficients, though, leads to the so-called \emph{BCJ relations}.
These are --- as the name suggests --- closely related to the color-kinematics duality.
It is instructive to obtain these identities through a simple example.

The BCJ representation of a four-point YM amplitude consists of three terms corresponding to the diagrams
\begin{align}
  \gTreeS[all=gluon,eLA=1,eLB=2,eLC=3,eLD=4]{} &= \frac{c_sn_s}{s}\,, &
  \gTreeT[all=gluon,eLA=1,eLB=2,eLC=3,eLD=4]{} &= \frac{c_tn_t}{t}\,, &
  \gTreeU[all=gluon,eLA=1,eLB=2,eLC=3,eLD=4]{} &= \frac{c_un_u}{u}\,.
\end{align}
We labeled the terms according to their pole structure expressed in the three Mandelstam invariants $s=(p_1+p_2)^2$, $t=(p_2+p_3)^2$ and $u=(p_3+p_1)^2$.
For massless particles they fulfill the identity $s+t+u=0$.
The color factors are given by
\begin{align}
  c_s &= \tilde{f}^{a_1a_2b}\tilde{f}^{ba_3a_4}\,, &
  c_t &= \tilde{f}^{a_4a_1b}\tilde{f}^{ba_2a_3}\,, &
  c_u &= \tilde{f}^{a_4a_2b}\tilde{f}^{ba_1a_3}\,.
\end{align}
The DDM color decomposition for this amplitudes is expressed as
\begin{equation}
  \begin{aligned}
    \cA_4^{(0)} &= c_t A_4^{(0)}(1,2,3,4) + c_u A_4^{(0)}(1,2,4,3)\\
    &= c_t\left(\frac{n_s}{s}+\frac{n_t}{t}\right) + c_u\left(-\frac{n_s}{s}+\frac{n_u}{u}\right)\,.
  \end{aligned}
\end{equation}
The color-kinematics duality is based on a single relation (ignoring sign flip identities)
\begin{equation}
  c_s = c_t - c_u \quad \Leftrightarrow \quad n_s = n_t - n_u\,.
\end{equation}
Choosing $n_s$ and $n_u$ as a basis of independent building blocks for the kinematic numerators we can express the two color-ordered amplitudes in the DDM decomposition as
\begin{equation}
  \begin{pmatrix} A_4^{(0)}(1,2,3,4) \\ A_4^{(0)}(1,2,3,4)\end{pmatrix} =
  \Theta \begin{pmatrix} n_s \\ n_u \end{pmatrix}
  \,\qquad \Theta = \begin{pmatrix} \frac{1}{s}+\frac{1}{t} & \frac{1}{t} \\[2pt] -\frac{1}{s} & \frac{1}{s+t} \end{pmatrix}\,.
\end{equation}
Since the matrix~$\Theta$ has rank 1, it induces a relation between the two amplitudes
\begin{equation}
  t\, A_4^{(0)}(1,2,3,4) + u\, A_4^{(0)}(1,2,4,3) = 0\,.
\end{equation}
For general multiplicity $n$, the matrix has rank $(n-3)!$ and the simplest type of relations has the form~\cite{Bern:2008qj}
\begin{equation}
  \sum_{i=3}^{n}\left(\sum_{j=3}^i s_{2j}\right)A_n^{(0)}(1,3,4,\dots,i,2,i+1,\dots,n) = 0\,.
\end{equation}
The variables $s_{ij}=(p_i+p_j)^2$ are the Mandelstam invariants for higher points.
We conclude that there are maximally $(n-3)!$ independent color-ordered amplitudes at tree level.
An extension to QCD, adding color factors for fundamental matter, is discussed in~\cite{Johansson:2014zca}.

At loop level much less is known about relations among partial amplitudes, as the kinematic structure is clouded by loop momentum labeling ambiguities.
The discussion of some features of higher loop color decompositions and relations among color-ordered objects is continued in part~\ref{part:color}.
