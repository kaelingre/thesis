\chapter{Generalized Unitarity}\label{sec:unitarity}

There exist a wide variety of analytic and numerical recursion relations for tree level amplitudes.
The computations at loop level have been dominated by the unitarity method.
To demonstrate the usefulness of this method consider the following non-exhaustive list of references related to new technologies and results obtained through unitarity~\cite{Bern:1994zx,Bern:1994cg,Bern:1995db,Bern:1996je,Bern:2000dn,Bern:1997sc,Bern:2004ky,Bern:2004cz,Britto:2004nc,Buchbinder:2005wp,Bern:2007ct,Anastasiou:2006jv,Britto:2006fc,Ossola:2006us,Britto:2007tt,Britto:2008vq,Badger:2008cm,Bern:2008pv,Bern:2010tq,Bern:2010qa,Kosower:2011ty,Davies:2011vt,Bern:2012uc,Bern:2013gka,Ochirov:2013oca,Bern:2014vza,Dunbar:2016dgg,Badger:2015lda,Dennen:2014vta,Frellesvig:2014fgs,Abreu:2017xsl,Dunbar:2017qxb,Anastasiou:2018fjr,Bern:2019nnu,Badger:2018enw,Kalin:2018thp,Abreu:2018jgq,Abreu:2018rcw,Abreu:2018gii}.
The generalized unitarity method~\cite{Bern:1994zx,Bern:1994cg} constructs loop level integrands from information solely contained in tree level amplitudes.
Tree level amplitudes contain all information necessary to compute any loop level amplitude --- they are the only building blocks needed for all the computations discussed in this thesis.
Here, we present only the basics in order to give the necessary background for the cut construction methods contained in later chapters.
For a more complete discussion and extensions of the method see e.g.~\cite{Britto:2010xq,Bern:2011qt,Carrasco_2011,Ita_2011}.

Consider an $L$-loop amplitude in arbitrary $D$ dimensions.
It can be expressed as a single integral over a sum of Feynman diagrams
\begin{equation}
  I = \int \dd^{LD}\ell \sum_i \cJ_i(\ell)\,.
\end{equation}
The integrand at a subregion of the integration domain given by constraints of the form
\begin{align}
  \ell_1^2 &= 0\,, & (\ell_1+k_1+k_2)^2 &= 0\,, & \ell_2^2 &= 0\,,& \dots\,,
\end{align}
is singular since some of the propagator denominators of some Feynman diagram expressions~$\cJ_i$ vanish --- or equivalently some momenta of internal legs of Feynman diagrams become on-shell.
These denominator factors are reduced to $i\epsilon$ coming from the Feynman prescription.
The residue at each singularity must then, in general, be given by a product of lower-loop on-shell amplitudes (at possibly higher points).
The cut propagator legs appear as external states thereof.
This procedure for all different on-shell subregions gives us information about the integrand to often completely reconstruct it.

In traditional treatment, the unitarity of the $S$-matrix, $S^\dagger S=1$, leads to an identity for its interacting part:
\begin{equation}
  -i(T-T^\dagger)=T^\dagger T\,,
\end{equation}
where $T$ is defined by $S=1+iT$.
Order by order in a perturbative expansion, this leads to equalities between the imaginary part of an $L$-loop amplitude and a sum over products of on-shell lower-loop amplitudes.
Only physical states can cross the cut.
Compared to a brute-force Feynman graph computation, a clear advantage is that all involved objects are on-shell and there is no need for Fadeev-Popov ghosts.

Graphically, these constraints can be interpreted as cuts into the diagrams building up the amplitude.
Let us do a simple one-loop example by cutting through a color-ordered four-point amplitude.
We use shaded blobs in a Feynman-like notation to represent amplitudes
\begin{equation}
  %\tikzset{external/force remake}
  A^{(1)}_4(1,2,3,4;\ell) =
  \begin{tikzpicture}
    [line width=1pt,
      baseline={([yshift=-0.5ex-0.4ex]current bounding box.center)},
      font=\scriptsize]
    \fill[blob] (0,0) circle (0.45);
    \filldraw[fill=white,draw=black] (0,0) circle (0.2);
    \draw[line] (-135:0.45) -- (-135:0.8) node[left] {$1$};
    \draw[line] (135:0.45) -- (135:0.8) node[left] {$2$};
    \draw[line] (45:0.45) -- (45:0.8) node[right] {$3$};
    \draw[line] (-45:0.45) -- (-45:0.8) node[right] {$4$};
    \node at (90:0.65) {$\uset{\rightarrow}{\ell}$};
  \end{tikzpicture}
  = \gBox[scale=0.9,eLA=$1$,eLB=$2$,eLC=$3$,eLD=$4$,iLB=$\uset{\rightarrow}{\ell}$,yshift=-0.2ex]{}
  + \gTriB[eLA=$1$,eLB=$2$,eLC=$3$,eLD=$4$]{
    \draw[line width=0.45pt,-to] (0.48,0.98) -- node[above] {$\ell$} (0.7,0.83);
  }
  + \cdots
\end{equation}
Cutting through the loop
  \begin{equation}
    \begin{aligned}
    \begin{tikzpicture}
      [line width=1pt,
        baseline={([yshift=-0.5ex-0.3ex]current bounding box.center)},
        font=\scriptsize]
      \fill[blob] (0,0) circle (0.45);
      \filldraw[fill=white,draw=black] (0,0) circle (0.2);
      \draw[line] (-135:0.45) -- (-135:0.8) node[left] {$1$};
      \draw[line] (135:0.45) -- (135:0.8) node[left] {$2$};
      \draw[line] (45:0.45) -- (45:0.8) node[right] {$3$};
      \draw[line] (-45:0.45) -- (-45:0.8) node[right] {$4$};
      \node at (108:0.67) {$\uset{\rightarrow}{\ell}$};
      \draw[dashed] (90:0.65) -- (-90:0.65);
    \end{tikzpicture}
    &=
    %\tikzset{external/force remake}
    \cBoxA[eLA=$1$,eLB=$2$,eLC=$3$,eLD=$4$,iLA=$\uset{\rightarrow}{\ell}$]{}\\
    &= \sum_{\text{states}}A_4^{(0)}(1,2,\ell,-\ell-p_{12}) \times A_4^{(0)}(3,4,\ell+p_{12},-\ell)
  \end{aligned}
\end{equation}
leads to a factorization of the residue into tree-level on-shell amplitudes.
The sum includes all physical states that are allowed to cross the cut.  

In practice, one often starts with an Ansatz for the amplitude, either in terms of a set of master integrals or as a sum over trivalent diagrams if one seeks a color-dual representation.
The residue of the Ansatz on a given cut kinematics is simple to compute and can then be compared to the cut expression to constrain the free parameters.

The method of \emph{generalized unitarity}~\cite{Bern:1994zx,Bern:1994cg} systematizes this approach by identifying the necessary cuts.
We call a minimal set of cuts required for a complete reconstruction \emph{spanning cuts}.
For massless theories, the complete integrand is reconstructible via cuts.
