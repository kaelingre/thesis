%
% Template for Doctoral Theses at Uppsala 
% University. The template is based on    
% the layout and typography used for      
% dissertations in the Acta Universitatis 
% Upsaliensis series                      
% Ver 5.2 - 2012-08-08                  
% Latest version available at:            
%   http://ub.uu.se/thesistemplate            
%                                         
% Support: Wolmar Nyberg Akerstrom        
% Thesis Production           
% Uppsala University Library              
% avhandling@ub.uu.se                          
%                                         
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass{UUThesisTemplate}

% Package to determine wether XeTeX is used
\usepackage{ifxetex}

\ifxetex
	% XeTeX specific packages and settings
	% Language, diacritics and hyphenation
	\usepackage[babelshorthands]{polyglossia}
	\setmainlanguage{english}
	\setotherlanguages{swedish}

	% Font settings
	\setmainfont{Times New Roman}
	\setromanfont{Times New Roman}
	\setsansfont{Arial}
	\setmonofont{Courier New}
\else
	% Plain LaTeX specific packages and settings
	% Language, diacritics and hyphenation
    % Use English and Swedish languages. 
	\usepackage[swedish,english]{babel} 

	% Font settings
	\usepackage{type1cm}
	\usepackage[utf8]{inputenc}
    \usepackage{lmodern}
	\usepackage[T1]{fontenc}
    \usepackage[babel=true]{microtype}
	%\usepackage{mathptmx}
	
	% Enable scaling of images on import
	\usepackage{graphicx}
\fi


% Tables
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{multirow}

% Document links and bookmarks
\usepackage{hyperref}

% Math
\usepackage{psfrag,slashed,cancel,lscape,caption,array}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{dsfont}
\usepackage{shuffle}

%\renewcommand{\appendixname}{appendix}

% Misc
\usepackage[page,title,titletoc]{appendix}
\usepackage{cite}
\usepackage{todonotes}
\newcommand{\TODO}[1]{{\tikzexternaldisable\todo{#1}\tikzexternalenable}}
\usepackage{epigraph}
\usepackage{bbding}

% Numbering of headings down to the subsection level
\numberingdepth{subsection}

% Including headings down to the subsection level in contents
\contentsdepth{section}

% gTikz
\input{gTikZ}
\tikzexternalize
\tikzsetexternalprefix{figures/}
\newcommand{\cC}[1]{\node[circle, fill=white, draw=black, inner sep=1.3] at #1 {}}


% Uncomment to use a custom abstract dummy text
\abstractdummy{
  \begin{abstract}
    Quantum field theory is a theoretical framework for the description of nature in terms of fundamental particles, fields and their interactions.
    In the quantum regime, elementary scattering processes are observables in many experiments and studied in theoretical physics.
    The theoretical understanding of scattering amplitudes is often based on a perturbative analysis in powers of the coupling strength of the fundamental forces.
    Whereas the computation of scattering amplitudes has been dominated by Feynman diagram constructions for a long time, new methods have lead to a multitude of novel results in the last 20-30 years.
    Thereafter discoveries of new representations, dualities and construction methods have enormously increased our understanding of the mathematical structure of scattering amplitudes.

    In this thesis we focus on a particular structure of gauge theory amplitudes known as the \emph{color-kinematics duality}.
    Closely tied to this duality is the \emph{double copy} construction of gravitational amplitudes, and a set of identities among basic building blocks of the gauge theory, the \emph{BCJ identities}.
    Using methods developed for the study of this duality, we obtain new results for scattering amplitudes in non-maximal supersymmetric Yang-Mills coupled to massless fundamental matter at one and two loops.
    We immediately construct amplitudes in supergravity theories via the double copy.
    Furthermore, we include methods and results for the integration of gauge theory amplitudes and the ultraviolet structure of supergravity amplitudes.

    In a second part we present ideas related to the identification of basic building blocks that underlie the construction of scattering amplitudes.
    A decomposition of gauge theory amplitudes into color- and kinematic-dependent contributions exposes a set of primitive objects.
    Relations among these objects allow us to identify a minimal set of independent kinematic building blocks.
  \end{abstract}
}

% shorthands
\newcommand{\cA}{\mathcal{A}}
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cM}{\mathcal{M}}
\newcommand{\cN}{\mathcal{N}}
\newcommand{\cV}{\mathcal{V}}
\newcommand{\cH}{\mathcal{H}}
\newcommand{\cT}{\mathcal{T}}
\newcommand{\cR}{\mathcal{R}}
\newcommand{\cS}{\mathcal{S}}
\newcommand{\cO}{\mathcal{O}}
\newcommand{\cJ}{\mathcal{J}}

\newcommand{\bra}[1]{\langle #1|}
\newcommand{\ket}[1]{|#1 \rangle}
\newcommand{\sqbra}[1]{[#1|}
\newcommand{\sqket}[1]{|#1]}
\newcommand{\braket}[1]{\langle #1 \rangle}
\newcommand{\sqbraket}[1]{[ #1 ]}
\newcommand{\braSqket}[1]{\langle #1 ]}
\newcommand{\sqbraKet}[1]{[ #1 \rangle}

\newcommand{\etab}{\bar{\eta}}

\newcommand*\dd{\mathop{}\!\mathrm{d}}

\newcommand{\lra}{\leftrightarrow}

\DeclareMathOperator{\tr}{tr}
\newcommand{\trp}{\tr_+}
\newcommand{\trm}{\tr_-}

\DeclareMathOperator{\adj}{adj}
\DeclareMathOperator{\Li}{Li}

\newcommand{\MHV}{\ensuremath{\text{MHV}}}
\newcommand{\MHVb}{\ensuremath{\overline{\text{MHV}}}}

\newcommand{\ubar}[1]{\underline{#1}}

\usepackage{stackengine}
\newcommand{\oset}[3][0.3ex]{\stackon[#1]{$#3$}{$\scriptstyle#2$}}
\newcommand{\uset}[3][0.3ex]{\stackunder[#1]{$#3$}{$\scriptstyle#2$}}

\newcommand{\nn}{\nonumber}

\newcommand{\ul}{\underline}
\newcommand{\ol}{\overline}

% cpp symbol
\usepackage{graphicx,relsize} 
\makeatletter
\DeclareRobustCommand\Cpp{{\em C\raisebox{2pt}{{\relsize{-2}++}}} }
\makeatother

\begin{document}
\frontmatter
    % Creates the front matter (title page(s), abstract, list of papers)
    % for either a Comprehensive Summary or a Monograph.
    % Authors of Comprehensive Summaries use this front matter 
    \frontmatterCS 
    % Monograph authors use this front matter 
    %\frontmatterMonograph 
 
   % Optional dedication
   %\dedication{Dedicated to all hard-working \\doctoral students at Uppsala University}
   %\dedication{To everyone that ever made me laugh}
   \dedication{To everyone who brought \\some color into my life}
 
    % Environment used to create a list of papers
    \begin{listofpapers}
    \item H.~Johansson, G.~K\"alin and G.~Mogull, {\it Two-loop supersymmetric QCD and half-maximal supergravity amplitudes}, JHEP {\bf 09} (2018) 019, arXiv:1706.09381 [hep-th]. \label{lbl:paper1}
    \item G.~K\"alin, {\it Cyclic Mario worlds -- color-decomposition for one-loop QCD}, JHEP {\bf 04} (2018) 141, arXiv:1712.03539 [hep-th]. \label{lbl:paper2}
    \item G.~K\"alin, G.~Mogull and A.~Ochirov, {\it Two-loop $\cN=2$ SQCD amplitudes with external matter from iterated cuts}, arXiv:1811.09604 [hep-th]. Submitted to JHEP. \label{lbl:paper3}
    \item C.~Duhr, H.~Johansson, G.~K\"alin, G.~Mogull and B.~Verbeek, {\it The Full-Color Two-Loop Four-Gluon Amplitude in $\cN=2$ Super-QCD}, arXiv:1904.05299 [hep-th]. Submitted to PRL. \label{lbl:paper4}
    \end{listofpapers}
    
    
    \begingroup
        % To adjust the indentation in your table of contents, uncomment and enter the widest numbers for each level
        %  E.g.  \settocnumwidth{widest chapter number}{widest section number}{widest subsection number}...{...}
       %  \settocnumwidth{5}{4}{5}{3}{3}{3}
        \tableofcontents
    \endgroup
    
% Optional tables
%\listoftables
%\listoffigures

\mainmatter
% This includes the "Instruction", "Problem and Solutions" and "Example" files. After reading it, remove it from Thesis.tex.
\input{parts/intro.tex}
\input{parts/backgroundReview.tex}
\input{parts/amps.tex}
\input{parts/colorDecomp.tex}
\input{parts/tools.tex}
\input{parts/epilogue.tex}
\input{parts/acknowledgements.tex}
\input{parts/svenskSammanfattning.tex}


\begin{appendices}
  %\appendixpage
  \input{parts/appConvNot.tex}
  \input{parts/appRep.tex}
\end{appendices}

%\input{Example/Instruction.tex}
%\input{Example/ProblemsAndSolutions}
%\input{Example/Example.tex}
    

\backmatter
    % References
    % No restriction is set to the reference styles
    % Save your references in References.bib
    % \nocite{*} % Remove this for your own citations
    \bibliographystyle{JHEP}
    \bibliography{References}

\end{document}

